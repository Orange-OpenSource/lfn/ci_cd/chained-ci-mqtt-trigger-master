.. _glossary:

Glossary
========

.. glossary::

    master controller
      the process that listen to job topic and assign them to the workers.

    worker
      the process that actually trigger the pipeline, wait for the result,
      parse it and give it back to the notification topic.
