Description
###########

Chained CI MQTT Trigger is (at names says) a way to trigger Chained CI (see
`Example Chained CI`_).

Main purpose is to listen at external voting system (in particular gerrit, but
other *should* works) and launch a Chain that will test the proposed change.

.. _Example Chained CI: https://gitlab.com/Orange-OpenSource/lfn/ci_cd/chained-ci-examples
