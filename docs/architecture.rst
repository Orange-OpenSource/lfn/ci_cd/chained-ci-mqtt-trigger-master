Architecture
############

The architecture relies deeply on MQTT:

- Workers/Controller listen to a MQTT topic (and subtopics) for new jobs to
  perform;
- Workers announce their availibility on a specific topic of the master;
- Controller assign a job to a worker on a specific topic (common to all
  workers);
- Workers publish the Chained-CI state into a "review" topic.
