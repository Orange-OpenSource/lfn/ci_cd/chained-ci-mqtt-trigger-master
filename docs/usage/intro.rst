Introduction
############

It *should* be simple to use.
You need to launch at least two processes:

- one or several workers that will listen to the reviews topics and say if
  they're available for a job.
- a master controller that will listen to the reviews topics and assign a job
  to one of the available worker or store it  in order to give it back to a
  worker that become available.

Once a worker finish, it'll publish the chained-ci result into a specific topic
