Configuration
#############

Here's an example of configuration with lines explained:

    .. include:: ../../etc/chained_ci_mqtt_trigger.conf
        :code: cfg

Complete Setup for Gerrit on Kubernetes
---------------------------------------

If you want a complete setup for Gerrit on Kubernetes, here's an example of
deployment (with fake values):
It will deploy the following:

- a container listening Gerrit and sending message on MQTT (see
  `Gerrit to MQTT`_);
- a container listening MQTT message and sending back to Gerrit (see
  `MQTT to Gerrit`_);
- a container as MQTT Broker using Mosquitto_ implementation
- a master controller
- two workers

First worker will trigger pipelines with the specific variables ``POD`` as
``64`` and ``ANOTHER_VAR`` as ``the truth`` whereas second worker will use
``42`` and ``the truth``.

``POD`` is a "well known" key for Chained CI and thus value used is used to
retrieve the right Chain in Chained CI and thus it should be always there.

Here's the example:

    .. include:: ../../etc/full_deployment.yml
        :code: yaml


.. _Gerrit to MQTT: https://gitlab.com/Orange-OpenSource/lfn/ci_cd/gerrit-to-mqtt
.. _MQTT to Gerrit: https://gitlab.com/Orange-OpenSource/lfn/ci_cd/mqtt-to-gerrit
.. _Mosquitto: https://mosquitto.org/
