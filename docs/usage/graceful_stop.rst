Graceful Stop
#############


Both workers and master controllers can be gracefully stopped. In order to do
such a thing, an MQTT message must be sent:

- on the master controller topic (i.e. ``master_topic/uuid``, with the values
  put on the configuration) with a message having ``shutdown`` as key (for
  example ``{'shutdown': 'please'}``) if you want to stop the controller;
- on the worker topic (i.e. ``worker_topic/uuid``, with the values
  put on the configuration) with a message having ``shutdown`` as key and the
  worker name as value of ``worker`` key (for
  example ``{'shutdown': 'please', 'worker': 'my_worker'}``) if you want to
  stop one of the worker;

  If such worker is in the middle of a review, the shutdown will wait the end
  of the review.

As master controller holds the jobs in memory, this queue is saved in a file at
every change (``saved_jobs_file`` in the configuration). Once restarted, the
controller will reload all the jobs from this file, allowing simpler upgrade.
