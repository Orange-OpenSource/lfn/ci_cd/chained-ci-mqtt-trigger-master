Installation
############

as an user, you can either deploy using pip or use the preconfigured docker.


Installation via PiP
--------------------

.. code:: shell

    $ git clone --depth 1 https://gitlab.com/Orange-OpenSource/lfn/ci_cd/chained-ci-mqtt-trigger-master.git
    $ pip install -r requirements.txt
    $ pip install .
    $ chained-ci-mqtt-trigger -h
    usage: chained-ci-mqtt-trigger [-h] [-m | -w] [-v] conffile
    Listen to MQTT Topics controller and trigger a Chained CI. Either starting as
    a "master" node (controls which worker will work on what) or starting as
    "worker" node (actually trigger the Chained CI). Default mode is worker.
    positional arguments:
      conffile       Configuration file
    optional arguments:
      -h, --help     show this help message and exit
      -m, --master   Start this instance as master (default: False)
      -w, --worker   Start this instance as worker (default: True)
      -v, --verbose  Increase output verbosity (default: 20)

Installation via Docker
-----------------------

.. code:: shell

    $ docker run registry.gitlab.com/orange-opensource/lfn/ci_cd/chained-ci-mqtt-trigger-master -h
