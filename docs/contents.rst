.. _contents:

Chained-CI Documentation contents
==================================

.. toctree::
   :maxdepth: 2

   description

   usage/intro
   usage/installation
   usage/configuration
   usage/graceful_stop

   architecture
   modules

.. include:: description.rst

Indices and tables
==================

.. only:: builder_html

   * :ref:`genindex`
   * :ref:`modindex`
   * :ref:`search`
   * :ref:`glossary`

.. only:: not builder_html

   * :ref:`modindex`
   * :ref:`glossary`
