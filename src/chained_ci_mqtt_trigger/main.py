# -*- coding: utf-8 -*-
# SPDX-License-Identifier: Apache-2.0
# Copyright 2019 Orange
#
# Licensed under the Apache License, Version 2.0 (the "License"); you may
# not use this file except in compliance with the License. You may obtain
# a copy of the License at
#
#      http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS, WITHOUT
# WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied. See the
# License for the specific language governing permissions and limitations
# under the License.
"""Main module."""
import argparse
import configparser
import logging
import sys

from loguru import logger
from paho.mqtt import client as mqtt

from chained_ci_mqtt_trigger.master import Master
from chained_ci_mqtt_trigger.topic import Topic
from chained_ci_mqtt_trigger.worker import Worker
from chained_ci_mqtt_trigger.xtesting import XtestingClient


def on_connect_publish(_client, _userdata, _flags, return_code):
    """Use when publish client is connected.

    Function launched when publish client is connected. Nothing is done
    except some logging.

    :param _client: the client that has been connected
    :param _userdata: the userdata defined on the client side
    :param _flags: the flags received
    :param return_code: return code of the connection

    :seealso: http://www.eclipse.org/paho/clients/python/docs/#callbacks
    """
    logger.info(
        'connected to MQTT Broker with status {} for publishing',
        return_code,
    )


def on_connect_subscribe(client, userdata, _flags, return_code):
    """Use when subscribe client is connected.

    Function launched when subscribe client is connected.
    After connection, subscribe to the parent topic is done.

    :param _client: the client that has been connected
    :param _userdata: the userdata defined on the client side
    :param _flags: the flags received
    :param rc: return code of the connection

    :type _client: paho.mqtt.client.Client
    :type userdata: a dict with at least the following entries:
        - topic: the root topic where we listen all the messages
        - master_topic: the topic used by workers to talk to the master
        - worker_topic: the topic used by master to talk to the workers
        - qos: the QoS for the messenging part
    :type flags: string -- not used --
    :type return_code: integer

    :seealso: http://www.eclipse.org/paho/clients/python/docs/#callbacks
    """
    logger.info(
        'connected to MQTT Broker with status {} for subscribing',
        str(return_code),
    )
    qos = userdata['qos']
    topics = userdata['topics']
    # Subscribing in on_connect() means that if we lose the connection and
    # reconnect then subscriptions will be renewed.
    for topic in topics['mains']:
        logger.info(
            'subscribing on topic {} with qos {}',
            topic,
            qos,
        )
        client.subscribe('{0}/#'.format(topic), qos)
    logger.info(
        'subscribing on topic {} with qos {}',
        topics['master'],
        qos,
    )
    client.subscribe(topics['master'], qos)
    logger.info(
        'subscribing on topic {} with qos {}',
        topics['worker'],
        qos,
    )
    client.subscribe(topics['worker'], qos)


def on_message(_client, userdata, msg):
    """Use when message is received.

    Function launched when a message is received on MQTT topic.
    Here we check if the topic is one of our interest.
    If yes and the topic is not a topic where we care only about message
    with "magic" word, we store the message payload on the "Master"
    instance.
    If yes and the topic is the topic where we care only about message with
    "magic" word, we check the word is present and store the message
    payload on the "Master" instance if it's the case

    :param _client: the client that has received the message
    :param userdata: the userdata defined on the client side
    :param msg: the message object

    :type _client: paho.mqtt.client.Client  -- not used --
    :type userdata: a dict with at least the following entries:
        - topic: the root topic where we listen all the messages
        - subtopics: all the subtopics we're interested in
        - master_topic: the topic used by workers to talk to the master
        - worker_topic: the topic used by master to talk to the workers
        - master: the Master instance which all the messages
    :type msg: paho.mqtt.client.MQTTMessage

    :seealso: http://www.eclipse.org/paho/clients/python/docs/#callbacks
    """
    logger.debug('received a message')
    topics = userdata['topics']['mains']
    master_topic = userdata['topics']['master']
    worker_topic = userdata['topics']['worker']
    logger.debug('parent topics: {}', topics)
    logger.debug('master topic: {}', master_topic)
    logger.debug('topic received: {}', msg.topic)
    if msg.topic == master_topic:
        logger.info(
            'Got a message with a right master topic: {}',
            msg.topic,
        )
        userdata['client'].handle_master_message(msg)
    elif msg.topic == worker_topic:
        logger.info(
            'Got a message with a right worker topic: {}',
            msg.topic,
        )
        userdata['client'].handle_worker_message(msg)
    else:
        subtopics = userdata['subtopics']
        for subtopic in subtopics:
            logger.debug('checking with subtopic {}', subtopic)
            for topic in topics:
                if msg.topic == '{0}/{1}'.format(topic, subtopic):
                    logger.info(
                        'Got a message with a right topic: {}',
                        msg.topic,
                    )
                    userdata['client'].handle_new_message(msg)


def _get_options():
    """Define and parse the command line arguments.

    :returns: Namespace of the configuration

    :seealso:
        https://docs.python.org/3/library/argparse.html#the-parse-args-method
    """
    description = """
    Listen to MQTT Topics controller and trigger a Chained CI.

    Either starting as a "master" node (controls which worker will work on
    what) or starting as "worker" node (actually trigger the Chained CI).
    Default mode is worker.
    """
    parser = argparse.ArgumentParser(
        description=description,
        formatter_class=argparse.ArgumentDefaultsHelpFormatter,
    )
    type_group = parser.add_mutually_exclusive_group()
    type_group.add_argument(
        '-m',
        '--master',
        help='Start this instance as master',
        action='store_true',
        default=False,
    )
    type_group.add_argument(
        '-w',
        '--worker',
        help='Start this instance as worker',
        action='store_true',
        default=True,
    )
    parser.add_argument(
        '-v',
        '--verbose',
        help='Increase output verbosity',
        action='store_const',
        const='DEBUG',
        default='INFO',
    )
    parser.add_argument('conffile', help='Configuration file')
    return parser.parse_args()


def _get_config(config_file):
    """Read the config file and return the configuration.

    :param config_file: the config file name

    :type config_file: string

    :returns: configparser.ConfigParser object

    :seealso: https://docs.python.org/3/library/configparser.html
    """
    logger.info('retrieving {} configuration file', config_file)
    config = configparser.RawConfigParser()
    config.optionxform = lambda option: option
    config.read(config_file)
    return config


def _generate_topic_configuation_override(config, main_topics):
    """Generate topic configuration override."""
    topic_configuration_override = {}
    for topic in main_topics:
        if config.has_section(topic):
            topic_configuration_override[topic] = {}
            if config.has_option(topic, 'summary_console_jobs'):
                topic_configuration_override[topic][
                    'summary_console_jobs'
                ] = _parse_dict_variables(
                    config.get(
                        topic,
                        'summary_console_jobs',
                    ),
                )
            if config.has_option(topic, 'case_names'):
                topic_configuration_override[topic][
                    'case_names'
                ] = _parse_dict_variables(
                    config.get(topic, 'case_names'),
                )
            if config.has_option(topic, 'strict_mode'):
                topic_configuration_override[topic][
                    'strict_mode'
                ] = config.getboolean(topic, 'strict_mode')
    return topic_configuration_override


# WPS210 Found too many local variables: 12 > 5
# C901 '_parse_config' is too complex (7)
def _parse_config(config):  # noqa: C901,WPS210
    """Parse needed configuration.

    :params config: the whole configuration

    :type config: configparser.ConfigParser object

    :returns: config

    :rtype: dict
    """
    logger.info('parsing configuration')
    auth = None
    username = config.get('mqtt', 'username', fallback=None)
    password = config.get('mqtt', 'password', fallback=None)
    if username:
        auth = {'username': username}
        if password:
            auth['password'] = password
    # retrieve the subtopics where we'll react on
    subtopics = [
        v for v in config.get('mqtt', 'subtopics').split('\n') if v != ''  # noqa: WPS111
    ]

    # retrieve the topics where we'll react on
    main_topics = [
        v for v in config.get('mqtt', 'topics').split('\n') if v != ''  # noqa: WPS111
    ]

    private_tokens = _parse_dict_variables(
        config.get('gitlab', 'private_tokens', fallback=''),
    )

    additional_variables = _parse_dict_variables(
        config.get('gitlab', 'additional_variables', fallback=''),
    )
    logger.debug('additional_variables: {}', additional_variables)

    transformations = dict(config.items('transformations'))
    logger.debug('transformations: {}', transformations)

    notification_tranformations = dict(
        config.items('notification_tranformations'),
    )
    logger.debug(
        'notification_tranformations: {}',
        notification_tranformations,
    )

    summary_console_jobs = _parse_dict_variables(
        config.get('gitlab', 'summary_console_jobs', fallback=''),
    )
    logger.debug('summary_console_jobs: {}', summary_console_jobs)

    case_names = _parse_dict_variables(
        config.get('gitlab', 'case_names', fallback=''),
    )
    logger.debug('case_names: {}', case_names)

    topic_configuration_override = _generate_topic_configuation_override(
        config, main_topics,
    )

    return {
        'mqtt': {
            'qos': config.getint('mqtt', 'qos', fallback=0),
            'port': config.getint(
                'mqtt',
                'port',
                fallback=1883,  # noqa: WPS432 Found magic number: 1883
            ),
            'keepalive': config.getint('mqtt', 'keepalive', fallback=60),
            'transport': config.get('mqtt', 'transport', fallback='tcp'),
            'hostname': config.get('mqtt', 'hostname'),
            'auth': auth,
        },
        'mqtt_path': config.get('mqtt', 'ws_path', fallback=None),
        'mqtt_headers': config.get('mqtt', 'ws_headers', fallback=None),
        'saved_jobs_file': config.get(
            'master', 'saved_jobs_file', fallback=None,
        ),
        'trigger': {
            'change_id': config.get('mqtt', 'change_id', fallback=None),
            'magic_word': config.get('mqtt', 'magic_word', fallback='redeploy'),
            'on_comment': config.get(
                'mqtt', 'trigger_on_comment', fallback='comment-added',
            ),
        },
        'topics': {
            'mains': main_topics,
            'master': '{0}/{1}'.format(
                config.get('mqtt', 'master_topic'),
                config.get('master', 'uuid'),
            ),
            'worker': '{0}/{1}'.format(
                config.get('mqtt', 'worker_topic'),
                config.get('master', 'uuid'),
            ),
            'notification': config.get('mqtt', 'notification_topic'),
        },
        'topics_overrides': topic_configuration_override,
        'subtopics': subtopics,
        'gitlab': {
            'hostname': config.get('gitlab', 'hostname'),
            'result_url': config.get('gitlab', 'result_page_base_url'),
            'result_template': config.get(
                'gitlab', 'result_page_url_specific', fallback='',
            ),
            'nickname': config.get('gitlab', 'nickname', fallback='Gitlab'),
            'chained_ci_project_id': config.get(
                'gitlab', 'chained_ci_project_id',
            ),
            'project_token': config.get('gitlab', 'project_token'),
            'private_tokens': private_tokens,
            'git_reference': config.get(
                'gitlab', 'git_reference', fallback='master',
            ),
            'additional_variables': additional_variables,
            'transformations': transformations,
            'notification_tranformations': notification_tranformations,
            'summary_console_jobs': summary_console_jobs,
            'xtesting_db': config.get('gitlab', 'xtesting_db', fallback=''),
            'build_tag': config.get('gitlab', 'build_tag', fallback=''),
            'case_names': case_names,
            'strict_mode': config.getboolean(
                'gitlab', 'strict_mode', fallback=False,
            ),
        },
    }


def _create_mqtt_client(
    config,
    name,
    on_connect_callback,
    on_message_callback=None,
    userdata=None,
):
    """Configure, create and connect an MQTT client.

    :param config: the config to apply
    :param name: the name of the client
    :param on_connect_callback: the on_connect callback method
    :param on_message_callback: the on_message callback method
    :param userdata: the userdata to add to the client

    :type config:  dict
    :type name: string
    :type on_connect_callback: function name
    :type userdata: dict, default to None
    :type on_message_callback: function name, default to None

    :returns: an MQTT client

    :rtype: paho.mqtt.client.Client
    """
    logger.info('configure and connect MQTT client {}', name)
    mqtt_config = config['mqtt']
    client = mqtt.Client(
        transport=mqtt_config['transport'],
        userdata=userdata,
    )
    client.on_connect = on_connect_callback
    if on_message_callback:
        client.on_message = on_message_callback
    if mqtt_config['transport'] == 'websockets':
        client.ws_set_options(
            path=config['mqtt_path'],
            headers=config['mqtt_headers'],
        )
    client.enable_logger(logging.getLogger(name))
    client.connect(
        mqtt_config['hostname'],
        mqtt_config['port'],
        mqtt_config['keepalive'],
    )
    return client


def _parse_dict_variables(array_variables):
    r"""
    Parse an "ini" variable and return the computed dict.

    :param array_variables: the variables given as a list

    :type array_variables: list

    :rtype: dict

    :return: the variables into a dict.


    :Example:

    >>> string_vars = 'a=b\nc=d'
    >>> _parse_dict_variables(string_vars)
    {'a': 'b', 'c': 'd'}
    """
    # WPS335 Found incorrect `for` loop iter type
    # WPS110 Found wrong variable name: item
    # WPS441 Found control variable used after block: item
    # WPS111 too short name
    # WPS352 Found multiline loop
    for item in [  # noqa: WPS335,WPS110,WPS352
        v for v in array_variables.split('\n') if v != ''  # noqa: WPS111
    ]:
        logger.debug('item: {}', item)
        logger.debug('item gauche: {}', item.split('=')[0])
        logger.debug('item droit: {}', item.split('=')[1])
    parsed_dict = {
        item.split('=')[0]: item.split('=')[1]  # noqa: WPS441
        for item in [  # noqa: WPS335,WPS110
            v for v in array_variables.split('\n') if v != ''  # noqa: WPS111
        ]
    }
    logger.debug('result: {}', parsed_dict)
    return parsed_dict


def create_topics(config):
    """Create the topics for the gating."""
    for topic in config['topics']['mains']:
        override = config['topics_overrides'].get(topic)
        override_options = generate_override_options_for_topic(override, topic)
        Topic(topic, **override_options)


def generate_override_options_for_topic(override, topic):
    """Generate Override options for topic."""
    override_options = {}
    if override is not None:
        logger.debug('Topic {}', topic)
        specific_summary = override.get('summary_console_jobs')
        if specific_summary:
            logger.debug('   * has specific summary')
            override_options['summary_console_jobs'] = specific_summary
        specific_cases_name = override.get('case_names')
        if specific_cases_name:
            logger.debug('   * has specific cases')
            override_options['case_names'] = specific_cases_name
        specific_strict_mode = override.get('strict_mode')
        if specific_strict_mode is not None:
            logger.debug('   * has specific strict mode')
            override_options['is_strict_mode'] = specific_strict_mode
    return override_options

# WPS210 Found too many local variables: 6 > 5
# WPS213 Found too many expressions: 10 > 9
# C901  'start' is too complex (7)


def start():  # noqa: WPS210,WPS213,C901
    """Enter into the main function.

    Do the following actions, according to the config file:
        - subscribe to the right MQTT topics
        - create a Master object that can interact with topics
    """
    options = _get_options()
    logger.remove()
    logger.add(sys.stdout, level=options.verbose)

    config = _parse_config(_get_config(options.conffile))
    publish_client = _create_mqtt_client(
        config,
        'mqtt.client.publisher',
        on_connect_publish,
    )

    config_gitlab = config['gitlab']
    Topic.default_summary = config_gitlab['summary_console_jobs']
    Topic.default_cases = config_gitlab['case_names']
    Topic.default_strict_mode = config_gitlab['strict_mode']
    logger.debug('Topic default summary: {}', Topic.default_summary)
    logger.debug('Topic default case names: {}', Topic.default_cases)
    logger.debug('Topic default strict mode: {}', Topic.default_strict_mode)
    create_topics(config)

    XtestingClient.url = config_gitlab['xtesting_db']
    XtestingClient.build_tag_tpl = config_gitlab['build_tag']

    if options.master:
        client = Master(
            config['trigger'],
            publish_client,
            config['topics'],
            config_gitlab,
            qos=config['mqtt']['qos'],
        )
    elif options.worker:
        client = Worker(
            config['trigger'],
            publish_client,
            config['topics'],
            config_gitlab,
            qos=config['mqtt']['qos'],
        )

    suscriber_userdata = {
        'subtopics': config['subtopics'],
        'qos': config['mqtt']['qos'],
        'client': client,
        'topics': config['topics'],
    }
    subscribe_client = _create_mqtt_client(
        config,
        'mqtt.client.subscriber',
        on_connect_subscribe,
        on_message_callback=on_message,
        userdata=suscriber_userdata,
    )
    publish_client.loop_start()
    subscribe_client.loop_start()
    if config['saved_jobs_file']:
        client.configuration_file = config['saved_jobs_file']
        client.load_jobs_from_file()
    client.loop_forever()


if __name__ == '__main__':
    start()
