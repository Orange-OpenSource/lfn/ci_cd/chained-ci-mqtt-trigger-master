# -*- coding: utf-8 -*-
# SPDX-License-Identifier: Apache-2.0
# Copyright 2019 Orange
#
# Licensed under the Apache License, Version 2.0 (the "License"); you may
# not use this file except in compliance with the License. You may obtain
# a copy of the License at
#
#      http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS, WITHOUT
# WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied. See the
# License for the specific language governing permissions and limitations
# under the License.
"""Topic module."""
from __future__ import annotations

from dataclasses import dataclass, field
from typing import ClassVar, NoReturn, Union

from loguru import logger


def generate_summary_console_jobs() -> dict[str, str]:
    """Give default summary console job.

    When no summary console job is provided, give the default summary
    console job of Topic class (`default_summary`)

    :Example:

    >>> Topic.default_summary = {'default': 'one'}
    >>> topic = Topic('topic')
    >>> topic.summary_console_jobs
    {'default': 'one'}
    >>> topic = Topic('topic', summary_console_jobs={'not': 'default'})
    >>> topic.summary_console_jobs
    {'not': 'default'}
    """
    return Topic.default_summary


def generate_case_names() -> dict[str, str]:
    """Give default case names.

    When no case name is provided, give the default summary
    console job of Topic class (`default_cases`)

    :Example:

    >>> Topic.default_cases = {'core': 'full'}
    >>> topic = Topic('topic')
    >>> topic.case_names
    {'core': 'full'}
    >>> topic = Topic('topic', case_names={'onap-k8s': 'full'})
    >>> topic.case_names
    {'onap-k8s': 'full'}
    """
    return Topic.default_cases


def generate_strict_mode() -> dict[str, str]:
    """Give default value for strict mode.

    When no strict mode is provided, give the default stric mode of Topic class
    (`default_strict_mode`)

    :Example:

    >>> Topic.default_strict_mode = False
    >>> topic = Topic('topic')
    >>> topic.is_strict_mode
    False
    >>> topic = Topic('topic', is_strict_mode=True)
    >>> topic.is_strict_mode
    True
    """
    return Topic.default_strict_mode


@dataclass
class Topic(object):
    """Topic class.

    Object representing a topic and its configuration.

    :type name: str
    :type case_names: dict, default to {}
    :type summary_console_jobs: dict, default to {}

    :note: the summary_console_jobs may be commplex to understand.
           Here's an example:
           let's say your chained-ci pipeline is the following:
           | first_step | --> | second_step | --> | third_step |

           second_step is launching the following pipeline:
           | test | --> | deploy | --> | pages |
           third_step is launching the following pipeline:
           | deploy | --> | notify |

           if summary_console_jobs is the following:
               {'second_step': 'pages', 'third_step': 'deploy'}
           that means that if second step fails, we'll try to retrieve
           (part of) the output of "pages" job of second_step launched
           pipeline.
           And if third_steps fails, we'll try to retrieve (part of) the
           output of "deploy" job of second_step launched pipeline.


           the part of the output retrieved is every lines written between
           '______________ Results _____________' and
           '____________________________________' in the job console.

    :Example:

    >>> Topic.default_summary = {}
    >>> Topic.default_cases = {}
    >>> Topic.default_strict_mode = False
    >>> topic = Topic('topic')
    >>> topic.name
    'topic'
    >>> topic.summary_console_jobs
    {}
    >>> topic.case_names
    {}
    >>> topic.is_strict_mode
    False
    >>> summary = {'job': 'inner_job'}
    >>> topic = Topic('topic', summary_console_jobs=summary)
    >>> topic.summary_console_jobs
    {'job': 'inner_job'}
    >>> case_names = {'onap-k8s': 'full', 'core': 'Basic SO Health Check'}
    >>> topic = Topic('topic', case_names=case_names)
    >>> topic.case_names
    {'onap-k8s': 'full', 'core': 'Basic SO Health Check'}
    """  # noqa: P102 docstring does contain unindexed parameters

    topics: ClassVar[list[Topic]] = []
    name: str
    default_summary: ClassVar[dict[str, str]] = {}
    default_cases: ClassVar[dict[str, str]] = {}
    default_strict_mode: ClassVar[bool] = False
    summary_console_jobs: dict[str, str] = field(
        default_factory=generate_summary_console_jobs,
    )
    case_names: dict[str, str] = field(
        default_factory=generate_case_names,
    )
    is_strict_mode: bool = field(default_factory=generate_strict_mode)

    def __post_init__(self) -> NoReturn:
        """Do post initialization.

        This is adding topic to Class topic lists.
        """
        logger.debug('Topic {0} created'.format(self.name))
        Topic.topics.append(self)

    @classmethod
    def get(cls, name: str) -> Union[Topic, None]:
        """Retrieve a topic by its name.

        :param name: the name of the topic we search

        :type name: str

        :return: the topic instance with self name if found, None otherwise

        :Example:

        >>> Topic.default_summary = {}
        >>> Topic.default_cases = {}
        >>> Topic('topic')
        Topic(name='topic', ...)
        >>> Topic('other/topic')
        Topic(name='other/topic', ...)
        >>> Topic.get('other/topic')
        Topic(name='other/topic', ...)
        >>> Topic.get('unknown')
        >>>
        """  # noqa: P102 docstring does contain unindexed parameters
        for topic in cls.topics:
            if topic.name == name:
                logger.debug('topic {0} found'.format(topic))
                return topic
        return None
