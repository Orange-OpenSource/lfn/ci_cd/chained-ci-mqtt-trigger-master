# -*- coding: utf-8 -*-
# SPDX-License-Identifier: Apache-2.0
# Copyright 2019 Orange
#
# Licensed under the Apache License, Version 2.0 (the "License"); you may
# not use this file except in compliance with the License. You may obtain
# a copy of the License at
#
#      http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS, WITHOUT
# WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied. See the
# License for the specific language governing permissions and limitations
# under the License.
"""NotificationClient module."""
import json

from chained_ci_mqtt_trigger.mqtt_client import MqttClient
from chained_ci_mqtt_trigger.utils import DictQuery
from loguru import logger


# disable too-few-public-methods as this class has one simple purpose
class NotificationClient(object):  # pylint: disable=too-few-public-methods
    """NotificationClient class.

    Object used to send  a well formatted message into a MQTT topic.

    :param client: the MQTT client to use to send the notification
    :param notification_topic: the MQTT topic to use
    :param nickname: the nickame of the sender. Use by listeners of the
                        topic to know from who the message is
    :param notification_transformations: dictionary giving how to retrieve
                                            dynamic variables that will be sent
                                            in the messages
    :param additional_variables: variables that we'll always send in the
                                    message

    :type client: :class:`paho.mqtt.client.Client`
    :type notification_topic: string
    :type nickname: string
    :type notification_transformations: dict
    :type additional_variables: dict, default to {}

    :Example:

    >>> import paho.mqtt.client as client
    >>> mqtt_client =  client.Client()
    >>> notification_transformations = {
    ...     'gerrit_review': 'change.number',
    ...     'gerrit_patchset': 'patchSet.number'}
    >>> client = NotificationClient(mqtt_client,
    ...                             "gerrit/reviews",
    ...                             "my_chained_ci_tester",
    ...                             notification_transformations)
    >>> client.additional_variables = {'the_answer': 42}
    """

    def __init__(
        self,
        client,
        notification_topic,
        nickname,
        notification_transformations,
    ):
        """Initialize the client.

        :param client: the MQTT client to use to send the notification
        :param notification_topic: the MQTT topic to use
        :param nickname: the nickame of the sender. Use by listeners of the
                            topic to know from who the message is
        :param notification_transformations: dictionary giving how to retrieve
                                             dynamic variables that will be
                                             sent in the messages
        :param additional_variables: variables that we'll always send in the
                                        message

        :type client: :class:`paho.mqtt.client.Client`
        :type notification_topic: string
        :type nickname: string
        :type notification_transformations: dict
        :type additional_variables: dict, default to {}

        :Example:

        >>> import paho.mqtt.client as client
        >>> mqtt_client =  client.Client()
        >>> notification_transformations = {
        ...     'gerrit_review': 'change.number',
        ...     'gerrit_patchset': 'patchSet.number'}
        >>> client = NotificationClient(mqtt_client,
        ...                             "gerrit/reviews",
        ...                             "my_chained_ci_tester",
        ...                             notification_transformations)
        """
        self._client = MqttClient(client, notification_topic)
        self._nickname = nickname
        self.transformations = notification_transformations
        self.additional_variables = {}
        logger.info('initialization complete')

    def notify(self, message, payload, score=0):
        """Send a notification on the MQTT topic.

        :param message: the message to send
        :param payload: JSON where we can retrieve dynamic variables that
            will be retrieved thanks to
            notification_transformations
        :param score: the score set in the message

        :type message: string
        :type payload: JSON
        :type score: int, default to 0

        :Example:

        >>> import paho.mqtt.client as client
        >>> mqtt_client =  client.Client()
        >>> notification_transformations = {
        ...     'gerrit_review': 'change.number',
        ...     'gerrit_patchset': 'patchSet.number'}
        >>> client = NotificationClient(mqtt_client,
        ...                             "gerrit/reviews",
        ...                             "my_chained_ci_tester",
        ...                             notification_transformations)
        >>> client.additional_variables = {'the_answer': 42}
        >>> payload ={'change': {'number': 42},
        ...           'patchSet': {'number': '1664'}}
        >>> client.notify("hello", payload)                    # doctest: +SKIP

        will send the following message to "gerrit/review" topic:
            {
                "message": "hello",
                "from": "my_chained_ci_tester",
                "score": 0,
                "the_answer": 42,
                "gerrit_review": 42,
                "gerrit_patchset": "1664"
            }

        >>> client.notify("hello", "yolo")

        will send nothing as "yolo" is not a dict
        """
        logger.debug('sending notification')
        logger.debug('message: {}', message)
        logger.debug('payload: {}', payload)
        logger.debug('score: {}', score)

        try:
            payload_values = json.loads(payload)
        except ValueError as exception:
            logger.exception(exception)
        else:
            logger.debug('retrieving additional_variables')
            message_payload = {}
            message_payload.update(self.additional_variables)

            for needed_variable, transformation in self.transformations.items():
                message_payload[needed_variable] = DictQuery(
                    payload_values,
                ).get(
                    transformation,
                )

            message_payload.update({
                'message': message,
                'score': score,
                'from': self._nickname,
            })

            logger.debug(
                'message to be sent: {}',
                json.dumps(message_payload),
            )
            published = self._client.publish(json.dumps(message_payload))
            logger.debug(
                'publication status: {}, published: {}',
                published.rc,
                published.is_published(),
            )
            published.wait_for_publish()
            logger.debug('notification sent')
