# -*- coding: utf-8 -*-
# SPDX-License-Identifier: Apache-2.0
# Copyright 2019 Orange
#
# Licensed under the Apache License, Version 2.0 (the "License"); you may
# not use this file except in compliance with the License. You may obtain
# a copy of the License at
#
#      http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS, WITHOUT
# WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied. See the
# License for the specific language governing permissions and limitations
# under the License.
"""Xtesting module."""
from __future__ import annotations

from dataclasses import InitVar, dataclass, field
from typing import Any, ClassVar, Dict, List, NoReturn

from jinja2 import Template
from jinja2.exceptions import TemplateError
from loguru import logger
from requests import RequestException
from urllib3.exceptions import MaxRetryError

from chained_ci_mqtt_trigger.http_adapter import Http
from chained_ci_mqtt_trigger.utils import FAILED, FULL, SUCCESS, SKIPPED

XTESTING_PASS = 'PASS'
# Xtesting value for fail: 'FAIL'
XTESTING_SKIP = 'SKIP'
XTESTING_CRITERIA = 'criteria'
XTESTING_NAME = 'name'
XTESTING_CASE_NAME = 'case_name'
XTESTING_STATUS = 'status'
XTESTING_PAGINATION = 'pagination'
XTESTING_CURRENT_PAGE = 'current_page'
XTESTING_TOTAL_PAGES = 'total_pages'
XTESTING_RESULTS = 'results'


def parse_xtesting_case_status(status: str) -> str:
    """Parse the status from xtesting to common usage here.

    :param result: the result in "XTesting" format

    :type result: str

    :return: the status in "chained ci mqtt trigger" format

    :Example:

    >>> parse_xtesting_case_status('PASS')
    'success'
    >>> parse_xtesting_case_status('SKIP')
    'skipped'
    >>> parse_xtesting_case_status('FAIL')
    'failed'
    >>> parse_xtesting_case_status('OTHER')
    'failed'
    """
    if status == XTESTING_PASS:
        return SUCCESS
    if status == XTESTING_SKIP:
        return SKIPPED
    return FAILED


def parse_xtesting_details(details: dict[str, Any]) -> List[XtestingTest]:
    """Parse details part of a specific Xtesting result.

    This will work only if the formatting is right (i.e. details is a dict and
    this dict has a 'tests' entry).
    This formatting is the one RobotFramework gives detailed results.

    :param details: details from Xtesting

    :type tests: dict

    :return: the test list (with the ones we managed to parse)
    """
    found_tests = []
    if details is not None:
        if isinstance(details, dict):
            tests = details.get('tests')
            if tests is not None:
                found_tests = parse_tests(tests)
    return found_tests


def parse_tests(tests: List[Dict[str, Any]]) -> List[XtestingTest]:
    """Parse tests from a specific Xtesting result.

    This will work only if the formatting is right (i.e. we have a 'name'
    and a 'status' for a test).
    This formatting is the one RobotFramework gives detailed results.

    :param tests: a list of tests from Xtesting

    :type tests: list of dict

    :return: the test list (with the ones we managed to parse)
    """
    xtesting_tests = []
    for test in tests:
        logger.debug(test)
        name = test.get(XTESTING_NAME)
        if name is not None:
            status = test.get(XTESTING_STATUS)
            if status is not None:
                xtesting_tests.append(
                    XtestingTest(name, parse_xtesting_case_status(status)),
                )
    return xtesting_tests


@dataclass
class XtestingTest(object):
    """XtestingCase class.

    Object used to represent a test result from Xtesting.

    :param name: the name of the test
    :param status: the status of the test

    :type name: str
    :type status: enum ('success', 'failed', 'skipped')
    """

    name: str
    status: str

    @property
    def is_successful(self) -> bool:
        """Give status in form of boolean.

        :return: True if status equals 'succes', False otherwise

        :rtype: bool

        :Example:

        >>> test = XtestingTest('name', 'success')
        >>> test.is_successful
        True
        >>> test = XtestingTest('name', 'skipped')
        >>> test.is_successful
        False
        """
        is_successful = self.status == SUCCESS
        if not is_successful:
            logger.debug('test {} is not successful', self.name)
        return is_successful


@dataclass
class XtestingCase(object):
    """XtestingCase class.

    Object used to represent a case result from Xtesting.

    :param name: the name of the case
    :param status: the status of the case
    :param tests: the test list for this case

    :type name: str
    :type status: enum ('success', 'failed', 'skipped')
    :type tests: list
    """

    name: str
    status: str
    tests: list[XtestingTest]

    @property
    def is_successful(self) -> bool:
        """Give status in form of boolean.

        :return: True if status equals 'succes', False otherwise

        :rtype: bool

        :Example:

        >>> case = XtestingCase('name', 'success', [])
        >>> case.is_successful
        True
        >>> case = XtestingCase('name', 'skipped', [])
        >>> case.is_successful
        False
        """
        is_successful = self.status == SUCCESS
        if not is_successful:
            logger.debug('test {} is not successful', self.name)
        return is_successful

    def is_test_successful(self, wanted_test: str) -> bool:
        """Find a test and return True if successful.

        :param wanted_test: the name of the test we want to check

        :type wanted_test: str

        :return: True if test is found and succesful, false otherwise

        :rtype: bool

        :Example:

        >>> from .xtesting import XtestingTest
        >>> case = XtestingCase('case', 'failed', [])
        >>> case.tests.append(XtestingTest('test1', 'success'))
        >>> case.tests.append(XtestingTest('test2', 'failed'))
        >>> case.is_test_successful('test1')
        True
        >>> case.is_test_successful('test2')
        False
        >>> case.is_test_successful('test3')
        False
        """
        for test in self.tests:
            if test.name == wanted_test:
                return test.is_successful
        return False

    def is_criteria_successful(self, criteria):
        """Parse a case to see if it mets the criteria.

        :param found_case: check if we want the full case result or a particular
            test result. ('full' for the whole case, the name of the test
            otherwise)

        :type found_case: str

        :return: True if the criteria is OK, False otherwise

        :rtype: Boolean

        :Example:

        >>> from .xtesting import XtestingTest
        >>> case = XtestingCase('case', 'failed', [])
        >>> case.tests.append(XtestingTest('test1', 'success'))
        >>> case.tests.append(XtestingTest('test2', 'failed'))
        >>> case.is_criteria_successful('full')
        False
        >>> case.is_criteria_successful('test1')
        True
        >>> case.is_criteria_successful('test2')
        False
        >>> case.status = 'success'
        >>> case.is_criteria_successful('full')
        True
        """
        if criteria == FULL:
            return self.is_successful
        return self.is_test_successful(criteria)


@dataclass
class XtestingClient(object):
    """XtestingClient class.

    Object used to interact with Xtesting Database.

    :param message: the message from which we'll be able to retrieve the right
        build tag.

    :type message: dict
    """

    url: ClassVar[str] = ''
    build_tag_tpl: ClassVar[str] = ''
    message: InitVar[dict[str, Any]]
    build_tag: str = field(init=False)
    cases: list[XtestingCase] = field(init=False)

    def __post_init__(self, message) -> NoReturn:
        """Post init the Xtesting client.

        Will retrieve and parse the cases for this specific build tag.
        """
        template = Template(self.build_tag_tpl)
        # WPS601 Found shadowed class attribute: cases
        # Post init is the way to do it AFAIK
        try:
            self.build_tag = template.render(message)  # noqa: WPS601
        except TemplateError as exc:
            logger.exception(exc)
            self.build_tag = ''  # noqa: WPS601
        self.cases = []  # noqa: WPS601
        self.get_tests()
        logger.debug('initialization finished')

    def get_tests(self, page=1) -> None:
        """Get the latest tests matching the build tag."""
        logger.debug(
            'Get tests for build_tag {} on page {}', self.build_tag, page,
        )
        url = '{0}/api/v1/results?build_tag={1}&page={2}'.format(
            self.url, self.build_tag, page,
        )
        try:
            request = Http().session.get(url)
            request.raise_for_status()
        except (MaxRetryError, RequestException) as connect_exception:
            logger.exception(connect_exception)
            return None

        try:
            response = request.json()
        except ValueError as json_exception:
            logger.exception(json_exception)
            return None

        self.parse_xtesting_response(response)
        self.handle_pagination(response)

    def handle_pagination(self, response):
        """Handle pagination part of the response.

        See if we need to grabe more tests from API.
        """
        pagination = response.get(XTESTING_PAGINATION)
        current_page = pagination.get(XTESTING_CURRENT_PAGE)
        total_pages = pagination.get(XTESTING_TOTAL_PAGES)
        if current_page < total_pages:
            self.get_tests(page=current_page + 1)

    def parse_xtesting_response(self, response: dict[str, Any]) -> None:
        """Parse the xtesting response."""
        for case_result in response.get('results'):
            # If we're finding 2 times the test, that means we're seeing an
            # on bunch of results and we can stop.
            logger.debug(
                'case found: {}',
                case_result.get(XTESTING_CASE_NAME),
            )
            if case_result.get(XTESTING_CASE_NAME) in self.case_names():
                break
            details = case_result.get('details')
            criteria = case_result.get(XTESTING_CRITERIA)
            self.cases.append(
                XtestingCase(
                    case_result.get(XTESTING_CASE_NAME),
                    parse_xtesting_case_status(criteria),
                    parse_xtesting_details(details),
                ),
            )

    def case_names(self) -> List[str]:
        """Generate the list of cases names."""
        case_names = []
        for case in self.cases:
            case_names.append(case.name)
        return case_names
