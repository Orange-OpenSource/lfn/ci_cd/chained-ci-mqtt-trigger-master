# -*- coding: utf-8 -*-
# SPDX-License-Identifier: Apache-2.0
# Copyright 2019 Orange
#
# Licensed under the Apache License, Version 2.0 (the "License"); you may
# not use this file except in compliance with the License. You may obtain
# a copy of the License at
#
#      http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS, WITHOUT
# WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied. See the
# License for the specific language governing permissions and limitations
# under the License.
"""Chained CI module."""
import re

import requests
from jinja2 import Template
from loguru import logger

from chained_ci_mqtt_trigger.utils import DictQuery


# hard to see how to disable this
# H601  class has low (22.22%) cohesion
# WPS214 Found too many methods: 9 > 7
class ChainedCi(object):  # noqa: H601,WPS214
    """ChainedCi class.

    Object use to store information from a Chained CI instance.

    :param hostname: the FQDN of gitlab instance
    :param chained_ci_id: the ID of Chained-CI project on this Gitlab instance
    :param project_token: the "trigger token" used to launch a pipeline on
                          Chained-CI project
    :param git_reference: the branch of Chained-CI we want to use
    :param additional_variables: the additional variables that we put in
                                     the trigger payload
    :param transformations: dictionary giving how to retrieve dynamic
                            variables that will be sent to Chained-CI
                            pipeline trigger

    :type hostame: string of a base url ('http://example.com)
    :type chained_ci_id: int
    :type project_token: string
    :type git_reference: string
    :type additional_variables: dict, default to empty dict
    :type transformations: dict, default to empty dict

    :Example:

    >>> cci = ChainedCi('http://example.com', 'http://example.io', 1234,
    ...                 'abc-def', 'master')
    >>> cci.api_url()
    'http://example.com/api/v4/projects/1234'
    >>> cci.trigger_url()
    'http://example.com/api/v4/projects/1234/trigger/pipeline'
    >>> cci.base_trigger_payload()
    {'token': 'abc-def', 'ref': 'master'}
    >>> result_page_template = "{{ change.number }}-{{ patchSet.number}}/"
    >>> cci.result_page_template = result_page_template
    >>> message = {'change': {'number': 1664}, 'patchSet': {'number': 3}}
    >>> cci.generate_result_page_path(message)
    'http://example.io/1664-3/index.html'
    """

    def __init__(  # noqa: WPS211 Found too many arguments: 6 > 5
        self,
        hostname,
        result_url,
        chained_ci_id,
        project_token,
        git_reference,
    ):
        """Initialize a Chained Ci object.

        :param hostname: the FQDN of gitlab instance
        :param chained_ci_id: the ID of Chained-CI project on this Gitlab
                              instance
        :param project_token: the "trigger token" used to launch a pipeline on
                              Chained-CI project
        :param git_reference: the branch of Chained-CI we want to use
        :param result_url: the url for static page result

        :type hostame: string of a base url ('http://example.com)
        :type chained_ci_id: int
        :type project_token: string
        :type git_reference: string
        :type result_url: string of the base of the result url
                          ('http://example.io)

        :Example:

        >>> cci = ChainedCi('http://example.com', 'http://example.io',
        ...                 1234, 'abc-def', 'master')
        >>> cci.api_url()
        'http://example.com/api/v4/projects/1234'
        >>> cci.trigger_url()
        'http://example.com/api/v4/projects/1234/trigger/pipeline'
        >>> cci.base_trigger_payload()
        {'token': 'abc-def', 'ref': 'master'}
        """
        self.hostname = hostname
        self.result_url = result_url
        self._chained_ci_id = chained_ci_id
        self._project_token = project_token
        self._git_reference = git_reference
        self.additional_variables = {}
        self.transformations = {}
        self.result_page_template = ''
        logger.info('initialization complete')

    def api_url(self):
        """Return the API URL.

        :rtype: string

        :return: the API URL

        :Example:

        >>> cci = ChainedCi('http://example.com', 'http://example.io', 1234,
        ...                 'abc-def', 'master')
        >>> cci.api_url()
        'http://example.com/api/v4/projects/1234'
        """
        return '{0}/api/v4/projects/{1}'.format(
            self.hostname,
            self._chained_ci_id,
        )

    def trigger_url(self):
        """Return the trigger URL.

        :rtype: string

        :return: the trigger URL

        :Example:

        >>> cci = ChainedCi('http://example.com', 'http://example.io',
        ...                 1234, 'abc-def', 'master')
        >>> cci.trigger_url()
        'http://example.com/api/v4/projects/1234/trigger/pipeline'
        """
        logger.debug(
            'trigger url: {}',
            '{0}/trigger/pipeline'.format(self.api_url()),
        )
        return '{0}/trigger/pipeline'.format(self.api_url())

    def base_trigger_payload(self):
        """Return the base trigger payload.

        :rtype: dict

        :return: the trigger payload

        :Example:

        >>> cci = ChainedCi('http://example.com', 'http://example.io',
        ...                 1234, 'abc-def', 'master')
        >>> cci.base_trigger_payload()
        {'token': 'abc-def', 'ref': 'master'}
        """
        return {'token': self._project_token, 'ref': self._git_reference}

    def pipeline_url(self, pipe_id):
        """Return the URL of a pipeline.

        :param pipe_id: the ID of the pipeline
        :type pipe_id: string
        :rtype: string
        :return: the pipeline URL

        :Example:

        >>> cci = ChainedCi('http://example.com', 'http://example.io',
        ...                 1234, 'abc-def', 'master')
        >>> cci.pipeline_url('5678')
        'http://example.com/api/v4/projects/1234/pipelines/5678'
        """
        logger.debug(
            'pipeline url: {}',
            '{0}/pipelines/{1}'.format(self.api_url(), pipe_id),
        )
        return '{0}/pipelines/{1}'.format(self.api_url(), pipe_id)

    def jobs_url(self, pipe_id):
        """Return the URL of the jobs of a given pipeline.

        :param pipe_id: the ID of the pipeline

        :type pipe_id: string

        :rtype: string

        :return: the jobs URL

        :Example:

        >>> cci = ChainedCi('http://example.com', 'http://example.io',
        ...                 1234, 'abc-def', 'master')
        >>> cci.jobs_url('5678')
        'http://example.com/api/v4/projects/1234/pipelines/5678/jobs'
        """
        logger.debug(
            'pipeline jobs url: {}',
            '{0}/jobs'.format(self.pipeline_url(pipe_id)),
        )
        return '{0}/jobs'.format(self.pipeline_url(pipe_id))

    def trace_url(self, job_id):
        """Return the URL of the trace of a given job.

        :param job_id: the ID of the job

        :type job_id: string

        :rtype: string

        :return: the trace URL

        :Example:

        >>> cci = ChainedCi('http://example.com', 'http://example.io',
        ...                 1234, 'abc-def', 'master')
        >>> cci.trace_url('5768')
        'http://example.com/api/v4/projects/1234/jobs/5768/trace'
        """
        logger.debug(
            'pipeline trace url: {}',
            '{0}/jobs/{1}/trace'.format(self.api_url(), job_id),
        )
        return '{0}/jobs/{1}/trace'.format(self.api_url(), job_id)

    # WPS210 Found too many local variables: 6 > 5
    def generate_payload(self, message):  # noqa: WPS210
        """Generate the payload to be sent to start a pipeline.

        :param message: the received message where we'll find variables found
                        thanks to transformations.

        :type message: dict

        :return: the payload for start the Chained-CI pipeline

        :rtype: dict

        :Example:

        >>> cci = ChainedCi('http://example.com', 'http://example.io',
        ...                 1234, 'abc-def', 'master')
        >>> transformations = {
        ...     'GERRIT_REVIEW': 'change.number',
        ...     'GERRIT_PATCHSET': 'patchSet.number'}
        >>> cci.transformations = transformations
        >>> cci.additional_variables = {'MY_VAR': 64}
        >>> message = {'change': {'number': 1664}, 'patchSet': {'number': 3}}
        >>> cci.generate_payload(message)
        ... # doctest: +NORMALIZE_WHITESPACE
        {'token': 'abc-def', 'ref': 'master', 'variables[MY_VAR]': 64,
         'variables[GERRIT_REVIEW]': 1664, 'variables[GERRIT_PATCHSET]': 3}
        >>> message = {'change': {'number': 1664}, 'patchSet': {'YOLO': 3}}
        >>> cci.generate_payload(message)
        ... # doctest: +NORMALIZE_WHITESPACE
        {'token': 'abc-def', 'ref': 'master', 'variables[MY_VAR]': 64,
         'variables[GERRIT_REVIEW]': 1664, 'variables[GERRIT_PATCHSET]': None}
        """
        variables = self.additional_variables

        for var_name, transformation in self.transformations.items():
            variables[var_name] = DictQuery(message).get(transformation)

        logger.debug('variables to be sent: {}', variables)
        payload = self.base_trigger_payload()

        for static_var_name, var_value in variables.items():
            payload.update(
                {'variables[{0}]'.format(static_var_name): var_value},
            )

        logger.info('pipeline payload: {}', payload)
        return payload

    def generate_result_page_path(self, message):
        """Generate URL result path.

        :param message: the received message where we'll find variables found
                        thanks to transformations.

        :type message: dict

        :return: the url

        :rtype: str

        :Example:

        >>> cci = ChainedCi('http://example.com', 'http://example.io',
        ...                 1234, 'abc-def', 'master')
        >>> message = {'change': {'number': 1664}, 'patchSet': {'number': 3}}
        >>> cci.generate_result_page_path(message)
        'http://example.io/index.html'
        >>> result_page_template = "{{ change.number }}-{{ patchSet.number}}/"
        >>> cci.result_page_template = result_page_template
        >>> cci.generate_result_page_path(message)
        'http://example.io/1664-3/index.html'
        >>> cci = ChainedCi('http://example.com', 'http://example.io',
        ...                 1234, 'abc-def', 'master')
        """
        template = Template(self.result_page_template)
        return '{0}/{1}index.html'.format(
            self.result_url,
            template.render(message),
        )

    def get_project_id(self, project_web_url):
        """Retrieve the project id from the project Web URL.

        :param project_web_url: the "root" url of the project (example:
            https://gitlab.com/Orange-OpenSource/lfn/onap/xtesting-onap/)

        :type project_web_url: str

        :return: the project id

        :rtype: str

        :Example:

        >>> cci = ChainedCi('http://example.com', 'http://example.io',
        ...                 1234, 'abc-def', 'master')
        >>> cci.get_project_id(
        ...     'https://gitlab.com/Orange-OpenSource/lfn/onap/xtesting-onap/')
        '10614465'
        >>> cci.get_project_id('https://none')
        ''
        """
        logger.debug(
            'trying to retrieve project id of {}',
            project_web_url,
        )
        pattern = re.compile(
            r'name=\"project_id\"[\s\S]*?value=\"(?P<project_id>[0-9]+)\"',
            re.MULTILINE,
        )
        try:
            response = requests.get(project_web_url)
        except requests.RequestException as request_exception:
            logger.exception(request_exception)
        else:
            if response.ok:
                search = pattern.search(response.text)
                logger.debug('search result: {}', search)
                if search:
                    return search.group('project_id')
        return ''
