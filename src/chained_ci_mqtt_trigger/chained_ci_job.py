# -*- coding: utf-8 -*-
# SPDX-License-Identifier: Apache-2.0
# Copyright 2019 Orange
#
# Licensed under the Apache License, Version 2.0 (the "License"); you may
# not use this file except in compliance with the License. You may obtain
# a copy of the License at
#
#      http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS, WITHOUT
# WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied. See the
# License for the specific language governing permissions and limitations
# under the License.
"""Chained CI Job module."""
import re

from loguru import logger
from requests import RequestException
from urllib3.exceptions import MaxRetryError

from chained_ci_mqtt_trigger.http_adapter import Http
from chained_ci_mqtt_trigger.utils import FAILED, SUCCESS

# hard to see how to disable this
# H601  class has low (28.67%) cohesion


class ChainedCiJob(object):  # noqa: H601
    """GitlabClientJob class.

    Object representing a Job of a run on Chained CI.

    :param pipeline_id: the pipeline ID of this particular run

    :param private_tokens: a dict containing the private_tokens to all
                            gitlab instances we may want to connect to
    :param payload: the payload sent to the particular run
    :param gitlab_client: the client which started this run
    :param xtesting_client: the xtesting client used to interact with xtesting
                            database where results are stored

    :type pipeline_id: str
    :type payload: dict
    :type chained_ci: :class:`
        chained_ci_mqtt_trigger.chained_ci.ChainedCi`
    :type gitlab_client: :class:`
        chained_ci_mqtt_trigger.chained_ci.GitlabClient`
    :type available: boolean
    :type xtesting_client: :class:`
        chained_ci_mqtt_trigger.xtesting_client.XtestingClient`, default
        to None
    """

    def __init__(self, run, job_id, name):
        """Initialize a Chained CI Job."""
        logger.info('client initialization')
        self.run = run
        self.job_id = job_id
        self.name = name
        self.inner_jobs = []
        self.inner_pipe = None
        self._status = None
        self.output = ''
        self.summary = None

    @property
    def status(self):
        """Return and lazy load the status."""
        if not self._status:
            self.get_information()
        return self._status

    @status.setter
    def status(self, status):
        """Set value for status."""
        self._status = status

    def get_information(self):
        """Parse the job console to get error information.

        Will deep parse (ask for summary of one of the inner jobs) if
        its name is summary_console_jobs keys.
        """
        url = self.run.chained_ci.trace_url(self.job_id)
        logger.debug('url to use: {0}'.format(url))
        try:
            request = Http().session.get(
                url,
                headers={
                    'PRIVATE-TOKEN': self.run.gitlab_client.private_token(url),
                },
            )
        except MaxRetryError as retry_exc:
            logger.exception(retry_exc)
            self.status = FAILED
            self.output = self.generate_message_header_failure(
                url,
                "can't connect to Gitlab CI API",
            )
            return

        logger.debug(
            'trace request status code: {}', request.status_code,
        )
        if request.ok:
            # I've either to choose between w504 and W503
            # https://www.python.org/dev/peps/pep-0008/#should-a-line-break-before-or-after-a-binary-operator
            # says it's W504 that should be chosen
            pattern = re.compile(
                r'\* Pipeline triggered for step[\s\S]*?'
                + r'(?P<inner_pipe>https?:\/\/[\w\/.-]+)\\?\"',  # noqa: W503
                re.MULTILINE,
            )
            search = pattern.search(request.text)
            logger.debug('search result: {}', search)
            if search:
                self.find_and_parse_inner_pipe(search)
            else:
                self.status = FAILED
                self.output = self.generate_message_header_failure(
                    url,
                    "can't find Inner Pipeline",
                )
        else:
            self.status = FAILED
            self.output = self.generate_message_header_failure(
                url,
                "can't connect to Gitlab CI API",
            )

    def find_and_parse_inner_pipe(self, search):
        """Find and Parse informations from Inner pipe."""
        if search.group('inner_pipe'):
            self.inner_pipe = re.sub(
                r':\/',
                '://',
                re.sub(r'\/\/', '/', search.group('inner_pipe')),
            )
            # other string does contain unindexed parameters
            logger.debug(
                'inner_pipe_url: {}', self.inner_pipe,
            )
            self.root_url = re.search(
                r'(?P<root_url>https?:\/\/[^\/]*)',
                self.inner_pipe,
            ).group('root_url')
            logger.debug('root_url: {}', self.root_url)
            self.get_inner_pipe_information()
            if self.status != SUCCESS:
                self.output = self.generate_message_header(
                    self.inner_pipe, 'Failed jobs:',
                )
            for job in self.inner_jobs:
                if job['status'] != SUCCESS:
                    self.output += self.generate_job_summary(job)

        self.summary = self.get_summary()

    def get_inner_pipe_information(self):
        """Parse the inner pipe status and retrieve its jobs information."""
        logger.debug(
            'get inner pipe information for {}', self.inner_pipe,
        )
        headers = {
            'PRIVATE-TOKEN': self.run.gitlab_client.private_token(
                self.inner_pipe,
            ),
            'Accept': 'application/json',
        }

        try:
            request = Http().session.get(self.inner_pipe, headers=headers)
        except (MaxRetryError, RequestException) as connect_exception:
            return self.handle_gitlab_exception(
                connect_exception,
                "can't connect to Inner Pipeline Gitlab",
            )

        try:
            request.raise_for_status()
        except RequestException as status_exception:
            return self.handle_gitlab_exception(
                status_exception,
                'issue when connecting to Inner Pipeline Gitlab',
            )

        logger.debug(
            'request status code: {}', request.status_code,
        )

        response = {}
        try:
            response = request.json()
        except ValueError as json_exception:
            return self.handle_gitlab_exception(
                json_exception,
                "can't parse Inner Pipeline Gitlab JSON response",
            )

        self.parse_inner_pipe_information(response)

    def parse_inner_pipe_information(self, response):
        """Parse the JSON of Gitlab in order to grap informations."""
        self.status = response['details']['status']['group']
        for stage in response['details']['stages']:
            for group in stage['groups']:
                for job in group['jobs']:
                    self.inner_jobs.append({
                        'name': job['name'],
                        'status': job['status']['group'],
                        'path': job['build_path'],
                    })

    def get_summary(self):
        """Deep retrieve the summary of the job.

        Which will be in the console of one of the triggered jobs.
        """
        logger.debug('retrieving summary for job {}', self.name)
        summary_console_jobs = self.run.topic.summary_console_jobs

        inner_job_name = summary_console_jobs.get(self.name)
        if inner_job_name is not None:
            for job in self.inner_jobs:
                if inner_job_name == job['name']:
                    return self.parse_output(job['path'])
            return ''

        logger.debug(
            'name {} is not in summary_console_jobs {}',
            self.name,
            summary_console_jobs,
        )
        return ''

    def parse_output(self, path):
        """Retrieve inner jobs output and parse its output.

        In the output of the job, text between '______________ Results
        _____________' and '____________________________________' (if
        found) is returned.
        """
        url = self._generate_inner_trace_url(path)
        headers = {
            'PRIVATE-TOKEN': self.run.gitlab_client.private_token(
                self.root_url,
            ),
        }

        logger.debug('url to use: {}', url)
        try:
            request = Http().session.get(url, headers=headers)
        except (MaxRetryError, RequestException) as connect_exception:
            logger.exception(connect_exception)
            return ''

        try:
            request.raise_for_status()
        except RequestException as exception:
            logger.exception(exception)
            return ''

        # I've either to choose between w504 and W503
        # https://www.python.org/dev/peps/pep-0008/#should-a-line-break-before-or-after-a-binary-operator
        # says it's W504 that should be chosen
        pattern = re.compile(
            r'______________ Results _____________[\s\S]*'
            + r'____________________________________',  # noqa: W503
            re.MULTILINE,
        )
        if pattern.search(request.text):
            return pattern.search(request.text).group(0)

        return ''

    def generate_message_header(self, pipeline, message):
        """Generate the message header."""
        header = '\n * Step: {0}\n   Status: {1}\n   Pipeline: {2}\n'.format(
            self.name,
            self.status,
            pipeline,
        )
        return '{0}   {1}'.format(header, message)

    def generate_message_header_failure(self, pipeline, message):
        """Generate the message header when failure."""
        return self.generate_message_header(
            pipeline, 'Failure Reason: {0}'.format(message),
        )

    def generate_job_summary(self, job):
        """Generate the failed job summary."""
        return '\n     - name: {0}\n       link: {1}'.format(
            job['name'], self.root_url + job['path'],
        )

    def handle_gitlab_exception(self, exception, message):
        """Handle request exceptions when connecting / retrieving JSON."""
        logger.exception(exception)
        self.status = FAILED
        self.output = self.generate_message_header_failure(
            self.inner_pipe,
            message,
        )
        return True

    def __repr__(self):
        """Represent GitlabClientJob as string."""
        base_repr = '{0} ({1})'.format(self.name, self.job_id)
        if self._status:
            return '{0}: {1}'.format(base_repr, self.status)
        return base_repr

    def _generate_inner_trace_url(self, path):
        """Generate the trace URL for the inner job."""
        project_root = path.split('/-/')[0]
        job_trace_path = path.split('/-/')[1]
        project_id = self.run.chained_ci.get_project_id('{0}{1}'.format(
            self.root_url,
            project_root,
        ))
        return '{0}/api/v4/projects/{1}/{2}/trace'.format(
            self.root_url, project_id, job_trace_path,
        )
