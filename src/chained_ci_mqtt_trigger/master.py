# -*- coding: utf-8 -*-
# SPDX-License-Identifier: Apache-2.0
# Copyright 2019 Orange
# Licensed under the Apache License, Version 2.0 (the "License"); you may
# not use this file except in compliance with the License. You may obtain
# a copy of the License at
#
#      http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS, WITHOUT
# WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied. See the
# License for the specific language governing permissions and limitations
# under the License.
"""Master module."""
import json
from collections import deque

from loguru import logger

from chained_ci_mqtt_trigger.mqtt_client import MqttClient
from chained_ci_mqtt_trigger.runner import Runner
from chained_ci_mqtt_trigger.utils import DictQuery


class Master(Runner):
    """Master controller for Chained CI Trigger."""

    # hard to see how to disable this
    # pylint: disable=too-many-arguments
    def __init__(
        self,
        trigger_config,
        mqtt_client,
        topics_config,
        gitlab_config,
        qos=0,
    ):
        """Init function.

        :param trigger_config: trigger configuration
        :param mqtt_client: the client used for publishing messages
        :param gitlab_config: config parameter for gitlab client
        :param topics_config: the configuration of all topics
        :param qos: the QoS to use to send messages (default to 0)
        :type mqtt_client: :class:paho.mqtt.client.Client
        :type topics_config: dict[str]
        :type gitlab_config: dict[str]
        :type trigger_config: dict[str]
        :type qos: int
        :Example:
        >>> import paho.mqtt.client as client
        >>> mqtt_client =  client.Client()
        >>> master = Master({'magic_word': 'magic', 'on_comment': 'trigger',
        ...                  'change_id': None},
        ...                 mqtt_client,
        ...                 {"worker": "topic/workers",
        ...                  "notification": "notification"},
        ...                 {"notification_tranformations":[]}, qos=1)
        >>> master.magic_word
        'magic'
        >>> master.trigger_on_comment
        'trigger'
        >>> len(master.jobs)
        0
        """
        super().__init__(
            trigger_config,
            mqtt_client,
            topics_config['notification'],
            'Gitlab controller',
            gitlab_config['notification_tranformations'],
        )
        self.jobs = deque([])
        self._mqtt_client = MqttClient(
            mqtt_client,
            topics_config['worker'],
            qos,
        )
        self.change_id = trigger_config['change_id']
        self.configuration_file = '/tmp/chained_ci_master.conf'
        logger.info('initialization complete')

    def put_job(self, job):
        """Add an job into the jobs queue.

        :param job: the job to insert
        :type job: an job

        :Example:
        >>> import paho.mqtt.client as client
        >>> mqtt_client =  client.Client()
        >>> master = Master({'magic_word': 'magic', 'on_comment': 'trigger',
        ...                  'change_id': None},
        ...                 mqtt_client,
        ...                 {"worker": "topic/workers",
        ...                  "notification": "notification"},
        ...                 {"notification_tranformations":[]}, qos=1)
        >>> master.jobs
        deque([])
        >>> master.put_job("job")
        >>> master.jobs
        deque(['job'])
        >>> master.put_job("job2")
        >>> master.jobs.popleft()
        'job'
        >>> master.jobs.popleft()
        'job2'
        """
        self.jobs.append(job)
        self.save_jobs_to_file()
        logger.info(
            'job put in the queue, queue size is {} now',
            len(self.jobs),
        )

    def get_job(self):
        """Get the next job to process.

        :return: the next job
        """
        job = self.jobs.popleft()
        self.save_jobs_to_file()
        logger.info(
            'job retrieved in the queue, queue size is {} now',
            len(self.jobs),
        )
        return job

    def handle_new_message(self, message):
        """Handle message for "new" jobs.

        these new jobs can be new code to be tested but also code to be
        retested.
        We need to check that "magic" word is inside the payload of the
        message.

        :param message: the message to look at
        :type message: :class:`paho.mqtt.client.MQTTMessage`

        :Example:

        >>> import paho.mqtt.client as mqtt
        >>> import json
        >>> mqtt_client =  mqtt.Client()
        >>> master = Master({'magic_word': 'magic', 'on_comment': 'trigger',
        ...                  'change_id': None},
        ...                 mqtt_client,
        ...                 {"worker": "topic/workers",
        ...                  "notification": "notification"},
        ...                 {"notification_tranformations":[]}, qos=1)
        >>> len(master.jobs)
        0
        >>> topic = "gerrit/trigger".encode('utf-8')
        >>> msg = mqtt.MQTTMessage(topic=topic)
        >>> payload = json.dumps(
        ...     {"comment": "it's a kind of magic"}).encode('utf-8')
        >>> msg.payload =  payload
        >>> master.handle_new_message(msg)                     # doctest: +SKIP
        >>> # payload has been put in jobs
        >>> len(master.jobs)                                   # doctest: +SKIP
        1
        >>> payload = json.dumps(
        ...     {"comment": "ce n'est pas magique"}).encode('utf-8')
        >>> msg.payload =  payload
        >>> master.handle_new_message(msg)                     # doctest: +SKIP
        >>> # payload has _NOT_ been put in jobs
        >>> len(master.jobs)                                   # doctest: +SKIP
        1
        """
        if self.need_handle_new_message(message):
            logger.info('Adding job to the queue')
            logger.debug(
                'Message payload: {}',
                str(message.payload.decode('UTF-8')),
            )
            logger.debug('topic: {}', message.topic)
            self.remove_duplicate_jobs(message)
            self.put_job({
                'topic': message.topic,
                'payload': str(message.payload.decode('UTF-8')),
            })
            self._notification_client.notify(
                'Deployment put on queue, number {0} on the queue.'.format(
                    len(self.jobs),
                ),
                message.payload.decode('UTF-8'),
            )

    def remove_duplicate_jobs(self, message):
        """Remove duplicate jobs.

        Looks into the queue to see if jobs with similar jobs id exists. If
        thus, will remove them from the queue as this job supersedes them.

        :param message: the message to look at
        :type message: :class:`paho.mqtt.client.MQTTMessage`
        """
        if self.change_id:
            logger.debug('starting remove duplicate process')
            try:
                payload = json.loads(message.payload.decode('UTF-8'))
            except json.decoder.JSONDecodeError as exc:
                logger.exception(exc)
            else:
                message_id = DictQuery(payload).get(self.change_id)
                for job in list(self.jobs):
                    try:
                        job_payload = json.loads(job['payload'])
                    except json.decoder.JSONDecodeError as exc:
                        logger.exception(exc)
                    else:
                        job_change_id = DictQuery(job_payload).get(
                            self.change_id,
                        )
                        if message_id == job_change_id:
                            logger.info(
                                'removing duplicate job for id {}',
                                message_id,
                            )
                            self.jobs.remove(job)

    def handle_master_message(self, message):
        """Handle message for master jobs.

        Can be a death trigger or an available message.
        Available messages can be there for two reasons:
            - a worker has finished a job and says he's available
            - a worker has seen a new job and says he's available
        If at least a job exists in the job queue, master will assign it to
        the worker.

        :param message: the message to look at
        :type message: :class:`paho.mqtt.client.MQTTMessage`


        :Example:

        >>> import json
        >>> import paho.mqtt.client as client
        >>> mqtt_client =  client.Client()
        >>> master = Master({'magic_word': 'magic', 'on_comment': 'trigger',
        ...                  'change_id': None},
        ...                 mqtt_client,
        ...                 {"worker": "topic/workers",
        ...                  "notification": "notification"},
        ...                 {"notification_tranformations":[]}, qos=1)
        >>> topic = "gerrit/availabme".encode('utf-8')
        >>> msg = client.MQTTMessage(topic=topic)
        >>> payload = json.dumps(
        ...     {"worker": "123-456"}).encode('utf-8')
        >>> msg.payload =  payload
        >>> master.handle_master_message(msg)
        True
        >>> # nothing will be done has no job available
        >>> master.put_job("job")
        >>> len(master.jobs)
        1
        >>> master.handle_master_message(msg)               # doctest: +SKIP
        >>> # first job will be affected to worker "123-456" and job will
        >>> # removed from queue
        >>> # message will be sent on 'mqtt-broker' MQTT broker with QoS of
        >>> # 2 on the topic "topic/worker" for the job affection.
        >>> len(master.jobs)                                # doctest: +SKIP
        0
        """
        logger.debug(
            'handling master message {}',
            message.payload,
        )
        try:
            payload = json.loads(message.payload)
        except json.decoder.JSONDecodeError as exc:
            logger.exception(exc)
        else:
            worker = payload.get('worker')
            if worker is not None:
                if self.jobs:
                    message = {
                        'worker': worker,
                        'job': self.get_job(),
                    }
                    logger.debug(
                        'assigning job {} to worker {}',
                        message['job'],
                        worker,
                    )
                    logger.debug('topic: {}', message['job']['topic'])
                    self._mqtt_client.publish(json.dumps(message))
                return True
            shutdown = payload.get('shutdown')
            if shutdown is not None:
                logger.info('received a graceful death trigger')
                self.loop = False
            else:
                logger.debug(
                    'no worker in payload and/or no jobs available ({})',
                    len(self.jobs),
                )

    def save_jobs_to_file(self):
        """Save jobs to file.

        save the job queue into a file for retrieval
        """
        logger.debug('save jobs from file')
        with open(self.configuration_file, 'w') as conf_file:
            jobs_array = []
            for job in self.jobs:
                jobs_array.append(job)
            conf_file.write(json.dumps(jobs_array, indent=4))

    def load_jobs_from_file(self):
        """Load jobs from file.

        retrieve information from the file and load the into the job
        queue.
        """
        logger.info('load jobs from file')
        old_conf = []
        try:
            with open(self.configuration_file, 'r') as old_config_file:
                old_conf = json.load(old_config_file)
        except (json.JSONDecodeError, FileNotFoundError) as exc:
            logger.exception(exc)
        for job in old_conf:
            self.jobs.append(job)
        logger.info(
            'saved jobs puts in the queue, queue size is {} now',
            len(self.jobs),
        )
