# -*- coding: utf-8 -*-
# SPDX-License-Identifier: Apache-2.0
# Copyright 2019 Orange
#
# Licensed under the Apache License, Version 2.0 (the "License"); you may
# not use this file except in compliance with the License. You may obtain
# a copy of the License at
#
#      http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS, WITHOUT
# WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied. See the
# License for the specific language governing permissions and limitations
# under the License.
"""Utilities for chained CI MQTT Trigger.

some simple functions used in the different classes
"""

SUCCESS = 'success'
FAILED = 'failed'
SKIPPED = 'skipped'

FULL = 'full'

# H601  class has low (0.00%) cohesion
# WPS600 Found subclassing a builtin: dict


class DictQuery(dict):  # noqa: WPS600,H601,WPS232
    """Dictionnary query helper."""

    # WPS231 Found too high function cognitive complexity: 14 > 12
    def get(self, path, default=None):  # noqa: WPS231
        """[Deep retrieve a value in nested dictionnaries.

        :param self: this class, used for self call
        :param path: the path of the value to get with dots ('.') between
                        elements
        :param default: what to give back if not found
        :return: The value of the element in path

        :Example:

        >>> dictionnary = {'a': {'b': {'c':['d', 'e'],
        ...                'f':[{'g': 'h'}]}, 'i': 'j'}}
        >>> DictQuery(dictionnary).get('a.i')
        'j'
        >>> DictQuery(dictionnary).get('a.b.c')
        ['d', 'e']
        >>> DictQuery(dictionnary).get('a.b.f.g')
        ['h']
        >>> DictQuery(dictionnary).get('a.b.d', default='NotFound')
        'NotFound'
        >>> DictQuery(dictionnary).get('a.b.d')
        """
        clefs = path.split('.')
        valeur = None

        for clef in clefs:
            if valeur:
                if isinstance(valeur, list):
                    valeur = [
                        v.get(
                            clef,
                            default,
                        ) if v else None for v in valeur  # noqa: WPS111
                    ]
                else:
                    valeur = valeur.get(clef, default)
            else:
                valeur = dict.get(self, clef, default)
            if not valeur:
                break

        return valeur
