# -*- coding: utf-8 -*-
# SPDX-License-Identifier: Apache-2.0
# Copyright 2019 Orange
#
# Licensed under the Apache License, Version 2.0 (the "License"); you may
# not use this file except in compliance with the License. You may obtain
# a copy of the License at
#
#      http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS, WITHOUT
# WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied. See the
# License for the specific language governing permissions and limitations
# under the License.
"""Runner module."""
import json
from threading import Event

from loguru import logger

from chained_ci_mqtt_trigger.notification_client import NotificationClient


# hard to see how to disable this
# WPS214 Found too many methods: 8 > 7
# H601  class has low (22.86%) cohesion
class Runner(object):  # noqa: H601,WPS214
    """Runner meta class for Chained CI Trigger."""

    # hard to see how to disable this
    # WPS211 Found too many arguments: 6 > 5
    def __init__(  # noqa: WPS211
        self,
        trigger_config,
        mqtt_client,
        notification_topic,
        nickname,  # noqa: RST301 (false positive)
        notification_tranformations,  # noqa: RST201 (false positive)
    ):
        """Init function.

        :param trigger_config: the config with following entries:
                                 - magic_word: the "magic" word that will
                                   trigger a new deployment
                                 - on_comment: which subtopic name will use
                                   "magic" word
        :param mqtt_client: the client used for publishing messages
        :type trigger_config: dict[str]
        :type mqtt_client: :class:paho.mqtt.client.Client

        :Example:

        >>> import paho.mqtt.client as mqtt
        >>> mqtt_client = mqtt.Client()
        >>> runner = Runner({'magic_word': 'magic', 'on_comment': 'trigger'},
        ...                 mqtt_client, "notification", "nickname", [])
        >>> runner.magic_word
        'magic'
        """
        self.magic_word = trigger_config['magic_word']
        self.trigger_on_comment = trigger_config['on_comment']
        self._loop = True
        self._notification_client = NotificationClient(
            mqtt_client,
            notification_topic,
            nickname,
            notification_tranformations,
        )

    @property
    def loop(self):
        """Show loop mode.

        :return: the loop mode
        :rtype: bool
        """
        return self._loop

    @loop.setter
    def loop(self, loop):
        self._loop = loop

    def handle_master_message(self, _message):
        """Handle message to master.

        As it's not a master, nothing is done

        :param _message: the message (not used)
        """
        pass  # noqa: WPS420: Found wrong keyword: pass

    def handle_worker_message(self, _message):
        """Handle worker messages.

        As it's not a worker, nothing is done

        :param _message: the message (not used)
        """
        pass  # noqa: WPS420: Found wrong keyword: pass

    def need_handle_new_message(self, message):
        """Do we need to handle message for "new" jobs.

        these new jobs can be new code to be tested but also code to be
        retested.
        We need to check that "magic" word is inside the payload of the
        message.

        :param message: the message to look at
        :type message: paho.mqtt.client.MQTTMessage to look at
        :return: True if need to handle / False either
        :rtype: bool

        :Example:

        >>> import paho.mqtt.client as mqtt
        >>> mqtt_client = mqtt.Client()
        >>> runner = Runner({'magic_word': 'magic', 'on_comment': 'trigger'},
        ...                 mqtt_client, "notification", "nickname", [])
        >>> topic = "gerrit/trigger".encode('utf-8')
        >>> msg = mqtt.MQTTMessage(topic=topic)
        >>> payload = json.dumps(
        ...     {"comment": "it's a kind of magic"}).encode('utf-8')
        >>> msg.payload =  payload
        >>> runner.need_handle_new_message(msg)
        True
        >>> payload = json.dumps(
        ...     {"comment": "ce n'est pas magique"}).encode('utf-8')
        >>> msg.payload =  payload
        >>> runner.need_handle_new_message(msg)
        False
        >>> payload = "payload".encode('utf-8')
        >>> runner.need_handle_new_message(msg)
        False
        """
        need_handle = True
        logger.debug(
            'check if trigger_on_comment {} is in topic {}',
            self.trigger_on_comment,
            message.topic,
        )  # other string does contain unindexed parameters
        logger.debug(
            'check also if magic_word {} is in payload',
            self.magic_word,
        )  # other string does contain unindexed parameters
        if self.trigger_on_comment in message.topic:
            comment = ''
            try:
                comment = json.loads(message.payload)['comment']
            except (KeyError, json.decoder.JSONDecodeError) as exc:
                logger.exception(exc)
                need_handle = False
            if self.magic_word not in comment:
                # do not execute
                # standard comment shall not trigger chained-ci
                need_handle = False

        return need_handle

    def loop_forever(self):
        """Start a waiting loop."""
        logger.info('starting waiting loop')
        while self.loop:
            Event().wait(timeout=10)
        logger.info('finishing waiting loop')

    def load_jobs_from_file(self):
        """Load jobs from file.

        doing nothing
        """
        logger.debug('called load_jobs_from_files (NOOP version)')
