# -*- coding: utf-8 -*-
# SPDX-License-Identifier: Apache-2.0
# Copyright 2019 Orange
#
# Licensed under the Apache License, Version 2.0 (the "License"); you may
# not use this file except in compliance with the License. You may obtain
# a copy of the License at
#
#      http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS, WITHOUT
# WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied. See the
# License for the specific language governing permissions and limitations
# under the License.
"""GitlabClient module."""
import json
import re
import time

import requests
from loguru import logger

from chained_ci_mqtt_trigger.chained_ci_run import ChainedCiRun
from chained_ci_mqtt_trigger.topic import Topic

# from chained_ci_mqtt_trigger.xtesting_client import XtestingClient

ERROR400 = 400
ERROR404 = 404
WAIT_TIME = 'wait_time'
MAX_RETRIES = 'max_retries'
MAX_TIMEOUT_RETRIES = 'max_timeout_retries'


class GitlabClient(object):
    """GitlabClient class.

    Object used to interact with Gitlab and Chained-CI.

    :param chained_ci: the Chained CI instance
    :param private_tokens: a dict containing the private_tokens to all
                            gitlab instances we may want to connect to
    :param notification_client: the client used for notification.
    :param available: to know if we're running a chain or not
    :param boundaries: the different boundaries for waiting:
                       - wait_time: how many seconds we wait between two
                                    check of the status of the pipeline
                       - max_retries: how many checks do we do before
                                      deciding the pipeline is too long.
                       - max_timeout_retries: how many check do we accept
                                              with timeout
                                              (i.e. we cannot connect to
                                              gitlab instance)
    :param xtesting_client: the xtesting client used to interact with xtesting
                            database where results are stored

    :type chained_ci: :class:`
        chained_ci_mqtt_trigger.chained_ci.ChainedCi`
    :type private_tokens: dict of string with FQDN as key and private token
                          for this FQDN as value
    :type notification_client: :class:
        `chained_ci_mqtt_trigger.notification_client.NotificationClient`
        or any object with same functions
    :type boundaries: dict, default to
        {'wait_time': 10, 'max_retries': 1440, 'max_timeout_retries': 5}
    :type available: boolean
    :type xtesting_client: :class:
        `chained_ci_mqtt_trigger.xtesting_client.XtestingClient`, default
        to None

    :Example:

    >>> from chained_ci_mqtt_trigger.chained_ci import ChainedCi
    >>> from chained_ci_mqtt_trigger.notification_client import NotificationClient
    >>> import paho.mqtt.client as client
    >>> cci = ChainedCi('http://example.com', 'http://example.io', 1234,
    ...                 'abc-def', 'master')
    >>> mqtt_client =  client.Client()
    >>> notification_transformations = {
    ...     'gerrit_review': 'change.number',
    ...     'gerrit_patchset': 'patchSet.number'}
    >>> notification_client = NotificationClient(mqtt_client,
    ...                                          "gerrit/reviews",
    ...                                          "my_chained_ci_tester",
    ...                                          notification_transformations)
    >>> private_tokens = {'gitlab.com': 'mysecret_token',
    ...                   'another.fqdn': 'another_secret'}
    >>> client = GitlabClient(cci, private_tokens, notification_client)

    :seealso: https://docs.gitlab.com/ce/ci/triggers/README.html
    :seealso: https://docs.gitlab.com/ce/user/profile/personal_access_tokens.html
    :seealso: NotificationClient

    """  # noqa: E501

    def __init__(self, chained_ci, private_tokens, notification_client):  # ,
        # xtesting_db=None):
        """Initialize the client.

        :param chained_ci: the Chained CI instance
        :param private_tokens: a dict containing the private_tokens to all
                               gitlab instances we may want to connect to
        :param notification_client: the client used for notification.
        :param xtesting_db: the url of Xtesting DB (where we'll retrieve test
                            results)

        :type chained_ci: :class:`chained_ci_mqtt_trigger.chained_ci.ChainedCi`
        :type private_tokens: dict of string with FQDN as key and private token
                              for this FQDN as value
        :type notification_client: :class:
            `chained_ci_mqtt_trigger.notification_client.NotificationClient`
            or any object with same functions
        :type xtesting_db: str, default to None

        """
        logger.info('client initialization')
        self._chained_ci = chained_ci
        self.private_tokens = private_tokens
        self._notification_client = notification_client
        self.boundaries = {
            WAIT_TIME: 10,
            MAX_RETRIES: 2440,
            MAX_TIMEOUT_RETRIES: 5,
        }
        self.available = True
        # if xtesting_db:
        #     self.xtesting_client = XtestingClient(xtesting_db)
        # else:
        #     self.xtesting_client = None
        logger.info('initialization complete')

    def launch(self, payload, topic_name):
        """Launch a pipeline and wait for the end.

        Launch a pipeline and wait for the end.

        :param payload: JSON where we can retrieve dynamic variables that
                        will be retrieved thanks to transformations
        :param topic_name: give the topic name to see if we need to overrides
                           some variables

        :return: None

        :Example:

        >>> from chained_ci_mqtt_trigger.chained_ci import ChainedCi
        >>> from chained_ci_mqtt_trigger.notification_client import NotificationClient
        >>> import paho.mqtt.client as client
        >>> mqtt_client =  client.Client()
        >>> notification_transformations = {
        ...     'gerrit_review': 'change.number',
        ...     'gerrit_patchset': 'patchSet.number'}
        >>> transformations = {
        ... 'GERRIT_REVIEW': 'change.number',
        ... 'GERRIT_PATCHSET': 'patchSet.number'}
        >>> private_tokens = {'gitlab.com': 'mysecret_token',
        ...                   'another.fqdn': 'another_secret'}
        >>> cci = ChainedCi("https://gitlab.com", 'http://example.io', 1234,
        ...                 "secret_token", "master")
        >>> cci.additional_variables = {'MYVAR': 42}
        >>> cci.transformations = transformations
        >>> notification_client = NotificationClient(
        ...     mqtt_client,
        ...     "gerrit/reviews",
        ...     "my_chained_ci_tester",
        ...     notification_transformations)
        >>> client = GitlabClient(cci, private_tokens, notification_client)
        >>> payload ={'change': {'number': 42},
        ...           'patchSet': {'number': '1664'}}
        >>> client.launch(payload)                              # doctest: +SKIP

        will trigger a pipeline on project 1234 from gitlab.com with the
        following variables:

            GERRIT_REVIEW = 42
            GERRIT_PATCHSET = '1664'
            MYVAR = 42

        and will wait up to the end or up to 1440 times 10 seconds.
        """  # noqa: E501
        logger.debug('received a launch request')
        self.available = False
        request = None
        launch_values = self.launch_values(payload)

        if launch_values is None:
            return False

        try:
            request = requests.post(
                self._chained_ci.trigger_url(),
                data=self._chained_ci.generate_payload(launch_values),
            )
        except requests.RequestException as connect_exception:
            logger.exception(connect_exception)
            self.parse_request_error(
                request, payload, 'Chained-CI build starting error.',
            )
            self.available = True
            return True

        logger.debug(
            'request status code: {}', request.status_code,
        )

        try:
            request.raise_for_status()
        except requests.RequestException as request_exception:
            logger.exception(request_exception)
            self.parse_request_error(
                request, payload, 'Chained-CI build starting error.',
            )
            self.available = True
            return True

        self.process_launch_informations(
            request, payload, launch_values, topic_name,
        )

    def launch_values(self, payload):
        """Retrieve the launch values."""
        try:
            launch_values = json.loads(payload)
        except ValueError as load_exception:
            logger.exception(load_exception)
            self._notification_client.notify(
                'Chained-CI failure, payload is not JSON', payload,
            )
            self.available = True
            return None
        else:
            return launch_values

    def process_launch_informations(
        self, request, payload, launch_values, topic_name,
    ):
        """Process Gitlan Answer and create a Run instance."""
        try:
            response = request.json()
        except ValueError as json_exception:
            logger.exception(json_exception)
            self.parse_request_error(
                request, payload, 'Chained-CI build starting error.',
            )
            self.available = True
            return True

        pipeline_id = response['id']
        message = 'Chained-CI build started (pipeline id: {0})'.format(
            pipeline_id,
        )
        message = '{0}, max time: {1} seconds'.format(
            message,
            self.boundaries[MAX_RETRIES] * self.boundaries[WAIT_TIME],
        )
        score = 0
        if Topic.get(topic_name).is_strict_mode:
            message = '{0}\n Putting a `-1`'.format(message)
            message = '{0} while waiting for the results.'.format(message)
            score = -1
        self._notification_client.notify(message, payload, score=score)
        chained_ci_run = ChainedCiRun(
            self,
            self._chained_ci,
            pipeline_id,
            launch_values,
            Topic.get(topic_name),
        )
        self.wait_for_pipeline_end(chained_ci_run)
        self.available = True

    def wait_for_pipeline_end(self, chained_ci_run):
        """Wait for the end of the pipeline.

        :param chained_ci_run: the actual run to monitor
        :param pipe_id: the ID of the pipeline to monitor
        :param payload: JSON where we can retrieve dynamic variables that
                        will be retrieved thanks to transformations
        """
        logger.debug(
            'wait for end of pipeline {}',
            chained_ci_run.pipeline_id,
        )
        retry = 0
        while retry < self.boundaries[MAX_RETRIES]:
            logger.debug('in the loop, retry {}', retry)
            if chained_ci_run.finished:
                logger.debug('Pipeline is finished')
                self._notification_client.notify(
                    chained_ci_run.overall_message,
                    json.dumps(chained_ci_run.payload),
                )
                self._notification_client.notify(
                    chained_ci_run.summary_message,
                    json.dumps(chained_ci_run.payload),
                    score=chained_ci_run.score,
                )
                break
            retry += 1
            time.sleep(self.boundaries[WAIT_TIME])
        if retry >= self.boundaries[MAX_RETRIES]:
            message = 'Chained-CI build too long. Aborting.'
            self._notification_client.notify(
                message,
                json.dumps(chained_ci_run.payload),
            )

    def parse_request_error(self, request, payload, message):
        """Parse the error of an HTTP request.

        :param request: the failed request
        :param payload: JSON where we can retrieve dynamic variables that
                        will be retrieved thanks to transformations
        :param message: the message to send with the request error
        """
        logger.debug('request is NOK')
        logger.debug(
            'request status code: {}', request.status_code,
        )
        logger.debug('request response: {}', request.text)
        failure_message = ''
        try:
            response = request.json()
        except ValueError as exception:
            logger.exception(exception)
            failure_message = request.text
        else:
            if request.status_code == ERROR404:
                failure_message = response['error']
            elif request.status_code == ERROR400:
                failure_message = response['message']['base']
            else:
                failure_message = response['message']

        message = '{0} Status code: {1}, failure message: {2}'.format(
            message,
            request.status_code,
            failure_message,
        )
        self._notification_client.notify(message, payload)

    def private_token(self, url):
        """Return the private token of an url if provided.

        :param url: the url to search for

        :type url: string (http)

        :return: the private token or None

        :rtype: string

        :Example:

        >>> private_tokens = {'fq.dn': 'private1', 'other.fqdn': 'private'}
        >>> client = GitlabClient(None, private_tokens, None)
        >>> client.private_token('https://fq.dn/api/v4')
        'private1'
        >>> client.private_token('http://other.fqdn')
        'private'
        >>> client.private_token('https://fq.dan/api/v4')
        >>> client.private_token('fq.dn')
        """
        pattern = re.compile(r'http[s]*://(?P<hostname>[^/]*)')
        hostname_search = pattern.search(url)
        if not hostname_search:
            return None
        hostname = hostname_search.group('hostname')
        if hostname not in self.private_tokens:
            logger.debug('token for {} NOT found', hostname)
            return None
        logger.debug('token for {} found', hostname)
        return self.private_tokens[hostname]
