# -*- coding: utf-8 -*-
# SPDX-License-Identifier: Apache-2.0
# Copyright 2019 Orange
#
# Licensed under the Apache License, Version 2.0 (the "License"); you may
# not use this file except in compliance with the License. You may obtain
# a copy of the License at
#
#      http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS, WITHOUT
# WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied. See the
# License for the specific language governing permissions and limitations
# under the License.
"""Worker module."""
import json
import uuid
from queue import Empty, Queue

from loguru import logger

from chained_ci_mqtt_trigger.chained_ci import ChainedCi
from chained_ci_mqtt_trigger.gitlab_client import GitlabClient
from chained_ci_mqtt_trigger.mqtt_client import MqttClient
from chained_ci_mqtt_trigger.runner import Runner

WORKER = 'worker'

# hard to see how to disable this


class Worker(Runner):  # noqa: H601 H601  class has low (47.62%) cohesion
    """Worker for Chained CI Trigger."""

    # hard to see how to disable this
    def __init__(  # noqa: WPS211 Found too many arguments: 7 > 5
        self,
        trigger_config,
        mqtt_client,
        topics_config,
        gitlab_config,
        qos=0,
    ):
        """Init function.

        :param trigger_config: trigger configuration
        :param mqtt_client: the client used for publishing messages
        :param gitlab_config: config parameter for gitlab client
        :param topics_config: the configuration of all topics
        :param qos: the QoS to use to send messages (default to 0)

        :type mqtt_client: `class`:paho.mqtt.client.Client
        :type topics_config: dict[str]
        :type gitlab_config: dict[str]
        :type trigger_config: dict[str]
        :type qos: int

        :Example:

        >>> import paho.mqtt.client as client
        >>> mqtt_client =  client.Client()
        >>> gitlab_configuration = {
        ...    'hostname': 'https://gitlab.com',
        ...    'result_url': 'https://pages.gitlab.io',
        ...    'result_template': '',
        ...    'nickname': 'Gitlab Public',
        ...    'chained_ci_project_id': '1234',
        ...    'project_token': '_trigger_token_',
        ...    'private_tokens': {
        ...        'gitlab.forge.orange-labs.fr': '_private_user_token_',
        ...        'gitlab.com':'_private_user_token_gitlab_com_'
        ...    },
        ...    'git_reference': 'master',
        ...    'additional_variables': {
        ...        'POD': 'onap_oom_gating_pod4',
        ...        'WORKAROUND': 'False'
        ...    },
        ...    'transformations': {
        ...        'GERRIT_REVIEW': 'change.number',
        ...        'GERRIT_PATCHSET': 'patchSet.number'
        ...    },
        ...    'notification_tranformations': {
        ...        'gerrit_review': 'change.number',
        ...        'gerrit_patchset': 'patchSet.number'
        ...    },
        ...    'summary_console_jobs': {
        ...        'apps_test:onap_oom_gating_pod4': 'pages',
        ...        'apps_deploy:onap_oom_gating_pod4': 'postconfigure'
        ...    }
        ... }
        >>> topics_config = {
        ...     'topics': ['topic'],
        ...     'master': 'topic/master',
        ...     'worker': 'topic/worker',
        ...     'notification': 'topic/review'
        ... }
        >>> worker = Worker({'magic_word': 'magic', 'on_comment': 'trigger'},
        ...                 mqtt_client, topics_config, gitlab_configuration,
        ...                 qos=1)
        >>> worker.magic_word
        'magic'
        >>> worker.trigger_on_comment
        'trigger'
        >>> worker.qos
        1
        """
        super().__init__(
            trigger_config,
            mqtt_client,
            topics_config['notification'],
            gitlab_config['nickname'],
            gitlab_config['notification_tranformations'],
        )

        chained_ci = ChainedCi(
            gitlab_config['hostname'],
            gitlab_config['result_url'],
            gitlab_config['chained_ci_project_id'],
            gitlab_config['project_token'],
            gitlab_config['git_reference'],
        )
        chained_ci.additional_variables = gitlab_config['additional_variables']
        chained_ci.transformations = gitlab_config['transformations']
        chained_ci.result_page_template = gitlab_config['result_template']

        self._gitlab_client = GitlabClient(
            chained_ci,
            gitlab_config['private_tokens'],
            self._notification_client,
        )
        self.events = Queue()
        self._mqtt_client = MqttClient(
            mqtt_client, topics_config['master'], qos,
        )
        self.name = '{0}/{1}'.format(
            gitlab_config['nickname'],
            str(uuid.uuid4()),
        )
        self.qos = qos
        # other string does contain unindexed parameters
        logger.info('initialization complete for {}', self.name)

    def handle_new_message(self, message):
        """Handle message for "new" jobs.

        these new jobs can be new code to be tested but also code to be
        retested.
        We need to check that "magic" word is inside the payload of the
        message.
        if Gitlab client is available, will launch an "available" message

        :param message: the message to look at

        :type message: paho.mqtt.client.MQTTMessage to look at
        """
        if self.need_handle_new_message(message):
            if self._gitlab_client.available:
                self._mqtt_client.publish(json.dumps({WORKER: self.name}))

    def handle_worker_message(self, message):
        """Handle worker messages.

        Can be a death trigger or a "go" message.
        If Gitlab client is available and message is destinated to us, will
        put the payload into the queue which will eventually launch a pipeline
        into gitlab.
        If Gitlab client is _not_ available and message is destinated to us,
        will resend the message as it was so master can rehandle it.

        :param message: the message to parse

        :type message: dict
        """
        payload = {}
        try:
            payload = json.loads(message.payload)
        except (json.decoder.JSONDecodeError) as exc:
            logger.exception(exc)
            return False

        worker = payload.get(WORKER)
        if worker is None:
            logger.warning(
                "there's no worker in the message {}",
                payload,
            )  # other string does contain unindexed parameters
            return True

        if worker == self.name:
            shutdown = payload.get('shutdown')
            if shutdown is not None:
                logger.info(
                    'received a kill message, gracefuly exiting',
                )
                self.loop = False
                return True

            job_payload = payload.get('job')
            if job_payload is None:
                logger.debug(
                    "there's no job in the message {}",
                    payload,
                )  # other string does contain unindexed parameters
                return True

            actual_payload = job_payload.get('payload')
            if actual_payload is None:
                logger.debug(
                    "there's no payload in the message {}",
                    job_payload,
                )  # other string does contain unindexed parameters
                return True

            topic = job_payload.get('topic')
            logger.debug('topic: {}', topic)
            if self._gitlab_client.available:
                self.events.put({'payload': actual_payload, 'topic': topic})
            else:
                # that means gitlab_client is not available
                logger.warning(
                    'Received a go for a job but not available, resending it',
                )
                if topic is not None:
                    self._mqtt_client.publish(actual_payload, topic)
        else:
            logger.debug(
                'message for worker {} is not for me {}',
                payload[WORKER],
                self.name,
            )  # other string does contain unindexed parameters

    def get_event(self, block=True, timeout=None):
        """Get the next event from the queue.

        :param block: Set to True to block if no event is available.
        :param timeout: Timeout to wait if no event is available.

        :type block: bool
        :type timeout: int

        :return: The next event as a JSON Data instance, or nothing

        if:
            - `block` is False and there is no event available in the queue, or
            - `block` is True and no event is available within the time
              specified by `timeout`.

        :Example:

        >>> import paho.mqtt.client as client
        >>> mqtt_client =  client.Client()
        >>> gitlab_configuration = {
        ...    'hostname': 'https://gitlab.com',
        ...    'result_url': 'https://pages.gitlab.io',
        ...    'result_template': '',
        ...    'nickname': 'Gitlab Public',
        ...    'chained_ci_project_id': '1234',
        ...    'project_token': '_trigger_token_',
        ...    'private_tokens': {
        ...        'gitlab.forge.orange-labs.fr': '_private_user_token_',
        ...        'gitlab.com':'_private_user_token_gitlab_com_'
        ...    },
        ...    'git_reference': 'master',
        ...    'additional_variables': {
        ...        'POD': 'onap_oom_gating_pod4',
        ...        'WORKAROUND': 'False'
        ...    },
        ...    'transformations': {
        ...        'GERRIT_REVIEW': 'change.number',
        ...        'GERRIT_PATCHSET': 'patchSet.number'
        ...    },
        ...    'notification_tranformations': {
        ...        'gerrit_review': 'change.number',
        ...        'gerrit_patchset': 'patchSet.number'
        ...    },
        ...    'summary_console_jobs': {
        ...        'apps_test:onap_oom_gating_pod4': 'pages',
        ...        'apps_deploy:onap_oom_gating_pod4': 'postconfigure'
        ...    }
        ... }
        >>> topics_config = {
        ...     'topics': ['topic'],
        ...     'master': 'topic/master',
        ...     'worker': 'topic/worker',
        ...     'notification': 'topic/review'
        ... }
        >>> worker = Worker({'magic_word': 'magic', 'on_comment': 'trigger'},
        ...                 mqtt_client, topics_config, gitlab_configuration,
        ...                 qos=1)
        >>> worker.events.put({'a': 1, 'b': 2})
        >>> worker.get_event()
        {'a': 1, 'b': 2}
        >>> worker.get_event(timeout=1)
        >>> worker.get_event(block=False)
        >>> worker.events.put({'a': 2, 'b': 3})
        >>> worker.get_event(block=False)
        {'a': 2, 'b': 3}
        """
        try:
            return self.events.get(block, timeout)
        except Empty:
            return None

    def loop_forever(self):
        """Start a waiting loop.

        Wait for a new event. When received,launch gitlab with the
        message.
        """
        while self.loop:
            try:
                self._inner_loop()
            except KeyboardInterrupt:
                self.loop = False
                logger.debug('shutdown asked')
                break

    def _inner_loop(self):
        """Do the Inner loop for loop forever."""
        logger.debug('ready to receive a job')
        self._mqtt_client.publish(json.dumps({WORKER: self.name}))
        event_info = None
        while self.loop and not event_info:
            logger.info('trying to get a new event')
            event_info = self.get_event(timeout=10)
            if event_info:
                logger.info('new event found, launching gitlab')
                actual_payload = event_info.get('payload')
                topic = event_info.get('topic')
                logger.info('topic given: {}', topic)
                # remove the last part of topic as it's the type of message and
                # we don't want it here.
                real_topic = '/'.join(topic.split('/')[:-1])
                self._gitlab_client.launch(actual_payload, real_topic)
