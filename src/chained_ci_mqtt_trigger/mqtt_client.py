# -*- coding: utf-8 -*-
# SPDX-License-Identifier: Apache-2.0
# Copyright 2019 Orange
#
# Licensed under the Apache License, Version 2.0 (the "License"); you may
# not use this file except in compliance with the License. You may obtain
# a copy of the License at
#
#      http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS, WITHOUT
# WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied. See the
# License for the specific language governing permissions and limitations
# under the License.
"""mqtt client module."""
from loguru import logger


class MqttClient(object):  # pylint: disable=too-few-public-methods
    """MqttClient class."""

    def __init__(self, client, topic, qos=0):
        """Object used to send  a message into a MQTT topic.

        :param client: the MQTT client to use to send the notification
        :param topic: the MQTT topic to use
        :param qos: the QoS to use to send messages (default to 0)

        :type client: :class:`paho.mqtt.client.Client`
        :type topic: string
        :type qos: integer
        """
        self._topic = topic
        self._client = client
        self._qos = qos
        logger.info('initialization complete for {}', self._topic)

    def publish(self, message, topic=None):
        """Publish a message into the topic with the QoS.

        :param message: the message to send
        :param topic: a specific topic to send if needed
        :type message: str
        :type topic: str
        """
        if not topic:
            topic = self._topic
        return self._client.publish(topic, message, qos=self._qos)
