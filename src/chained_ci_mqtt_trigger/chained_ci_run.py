# -*- coding: utf-8 -*-
# SPDX-License-Identifier: Apache-2.0
# Copyright 2019 Orange
#
# Licensed under the Apache License, Version 2.0 (the "License"); you may
# not use this file except in compliance with the License. You may obtain
# a copy of the License at
#
#      http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS, WITHOUT
# WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied. See the
# License for the specific language governing permissions and limitations
# under the License.
"""Chained CI Run module."""

from loguru import logger
from requests import RequestException
from urllib3.exceptions import MaxRetryError

from chained_ci_mqtt_trigger.chained_ci_job import ChainedCiJob
from chained_ci_mqtt_trigger.http_adapter import Http
from chained_ci_mqtt_trigger.utils import FAILED, SUCCESS
from chained_ci_mqtt_trigger.xtesting import XtestingClient


class ChainedCiRun(object):
    """GitlabClientRun class.

    Object representing a Run on Chained CI.

    :param pipeline_id: the pipeline ID of this particular run

    :param private_tokens: a dict containing the private_tokens to all
                            gitlab instances we may want to connect to
    :param payload: the payload sent to the particular run
    :param gitlab_client: the client which started this run
    :param topic: the topic that has created this run
    :param boundaries: the different boundaries for waiting:
                         - wait_time: how many seconds we wait between two
                                      check of the status of the pipeline
                         - max_retries: how many checks do we do before
                                        deciding the pipeline is too long.
                         - max_timeout_retries: how many check do we accept
                                                with timeout
                                                (i.e. we cannot connect to
                                                gitlab instance)
    :param xtesting_client: the xtesting client used to interact with xtesting
                            database where results are stored

    :type pipeline_id: str
    :type payload: dict

    :type chained_ci: :class:`
        chained_ci_mqtt_trigger.chained_ci.ChainedCi`
    :type gitlab_client: :class:`
        chained_ci_mqtt_trigger.chained_ci.GitlabClient`
    :type boundaries: dict, default to {
                            'wait_time': 10, 'max_retries': 1440,
                            'max_timeout_retries': 5}
    :type available: boolean
    :type xtesting_client: :class:
        `chained_ci_mqtt_trigger.xtesting_client.XtestingClient`, default
        to None
    """

    def __init__(self, gitlab_client, chained_ci, pipeline_id, payload, topic):
        """Initialize a Chained CI Run."""
        logger.info('client initialization')
        self.gitlab_client = gitlab_client
        self.chained_ci = chained_ci
        self.pipeline_id = pipeline_id
        self.payload = payload
        self.topic = topic
        self._finished = False
        self.overall_message = ''
        self.summary_message = ''
        self.score = 0
        self.jobs = []

    @property
    def finished(self):
        """Return and lazy load the unique_identifier."""
        if not self._finished:
            self.check_pipeline_status()
        return self._finished

    @finished.setter
    def finished(self, finished):
        """Set value for unique_identifier."""
        self._finished = finished

    def check_pipeline_status(self):
        """Check the status of a pipeline."""
        headers = {
            'PRIVATE-TOKEN': self.gitlab_client.private_token(
                self.chained_ci.hostname,
            ),
        }

        request = None
        try:
            request = Http().session.get(
                self.chained_ci.pipeline_url(self.pipeline_id),
                headers=headers,
            )
        except (MaxRetryError, RequestException) as connect_exception:
            return self.handle_check_pipeline_exception(
                connect_exception, request,
            )

        try:
            request.raise_for_status()
        except RequestException as status_exception:
            return self.handle_check_pipeline_exception(
                status_exception, request,
            )

        try:
            response = request.json()
        except ValueError as json_exception:
            return self.handle_check_pipeline_exception(
                json_exception, request,
            )

        logger.debug('request is OK')

        status = response.get('status')
        logger.debug(
            'pipeline {} status: {}', self.pipeline_id, status,
        )

        if status == SUCCESS:
            self.parse_pipeline(error_only=False)
            self.finished = True
            return True
        if status == FAILED:
            self.parse_pipeline()
            self.finished = True
            return True
        return False

    def parse_pipeline(self, error_only=True):
        """Parse a pipeline.

        :param error_only: do we parse only jobs in error or all
        """
        logger.debug('parsing pipeline {}', self.pipeline_id)

        headers = {
            'PRIVATE-TOKEN': self.gitlab_client.private_token(
                self.chained_ci.hostname,
            ),
        }
        self.summary_message = self._generate_result_url_msg()

        request = None
        try:
            request = Http().session.get(
                self.chained_ci.jobs_url(self.pipeline_id),
                headers=headers,
            )
        except (MaxRetryError, RequestException) as connect_exception:
            return self.handle_parse_pipeline_exception(
                connect_exception, request,
            )

        logger.debug(
            'request status code: {}', request.status_code,
        )

        try:
            request.raise_for_status()
        except RequestException as status_exception:
            return self.handle_parse_pipeline_exception(
                status_exception, request,
            )

        try:
            response = request.json()
        except ValueError as json_exception:
            return self.handle_parse_pipeline_exception(
                json_exception, request,
            )

        self.timeout_retry = 0

        self.find_jobs(response, error_only)

        self.overall_message = 'Chained CI build failure on some jobs.'
        self.overall_message = '{0}\nJobs with issue:'.format(
            self.overall_message,
        )

        logger.debug('jobs: {}', self.jobs)
        for job in self.jobs:
            job.get_information()
            if job.output:
                self.overall_message = '{0}\n{1}'.format(
                    self.overall_message, job.output,
                )
            elif job.status != SUCCESS:
                self.overall_message = '{0}\n  * name: {1}, no url'.format(
                    self.overall_message,
                    job.name,
                )
            if job.summary:
                self.summary_message = '{0}\n{1}'.format(
                    self.summary_message, job.summary,
                )

        if self.topic.case_names:
            self.retrieve_status()

    def find_jobs(self, response, error_only):
        """Find failed jobs of a pipeline.

        :param response: the ID of the pipeline to monitor
        :param error_only: do we gather only jobs in error
        :return: the failed jobs
        :rtype: dict

        :Example:

        >>> jobs = [
        ...     {'id': 1, 'name': 'first', 'status': 'success'},
        ...     {'id': 2, 'name': 'second', 'status': 'success'},
        ...     {'id': 3, 'name': 'third', 'status': 'failed'},
        ...     {'id': 4, 'name': 'fourth', 'status': 'cancelled'},
        ...     {'id': 5, 'name': 'fifth', 'status': 'failed'}]
        >>> run = ChainedCiRun(None, None, 12, {'sth':'a'}, None)
        >>> run.find_jobs(jobs, True)
        >>> len(run.jobs)
        2
        >>> run.jobs[0]
        third (3)
        >>> run.jobs[1]
        fifth (5)
        >>> run = ChainedCiRun(None, None, 12, {'sth':'a'}, None)
        >>> run.find_jobs(jobs, False)
        >>> len(run.jobs)
        5
        """
        for job in response:
            logger.debug(
                'job {} with name {} has status {}',
                job['id'],
                job['name'],
                job['status'],
            )
            if (not error_only) or job['status'] == FAILED:
                self.jobs.append(ChainedCiJob(self, job['id'], job['name']))
        logger.debug('jobs: {}', self.jobs)

    def handle_check_pipeline_exception(self, exception, request):
        """Handle request exceptions when checking pipeline."""
        logger.exception(exception)
        self.finished = True
        self.overall_message = self.parse_request_error(
            request, 'Chained-CI status retrieving error.',
        )
        return True

    def handle_parse_pipeline_exception(self, exception, request):
        """Handle request exceptions when parsing pipeline jobs."""
        logger.exception(exception)
        self.finished = True
        self.overall_message = self.parse_request_error(
            request,
            'Chained-CI status retrieving error.',
        )
        return True

    def retrieve_status(self):
        """Retrieve the status from Xtesting DB."""
        client = XtestingClient(self.payload)
        found_cases_number = 0
        logger.debug('cases number: {}', len(client.cases))
        for case in client.cases:
            logger.debug('case name: {}', case.name)
            criteria = self.topic.case_names.get(case.name)
            if criteria is not None:
                found_cases_number += 1
                if not case.is_criteria_successful(criteria):
                    logger.debug(
                        'case {} for test {} is not successful',
                        case.name,
                        criteria,
                    )
                    self.score = -1
                    return False

        if found_cases_number != len(self.topic.case_names):
            logger.debug(
                'we wanted {} tests results, we have only {}',
                len(self.topic.case_names),
                found_cases_number,
            )
            self.score = -1
            return False
        self.score = 1
        return True

    def _generate_result_url_msg(self):
        """Generate the message with full URL.

        :return: the message with the URL if set or empty string.
        :rtype: string
        """
        if self.chained_ci.result_page_template:
            result_url = self.chained_ci.generate_result_page_path(
                self.payload,
            )
            return '\nFull results can be seen here: {0}'.format(result_url)
        return ''
