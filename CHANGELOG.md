# Chained CI MQTT Trigger changelog

## 3.0.0

- send a message when put in queue
- delete duplicate changes in queue

## 2.0.0

- save the master controller queue state in a file and reload it at start
- ability to send a "graceful close" message to workers and master controllers
- ability to monitor several topics (useful for submodule gating)

## 1.0.1

- use configured QoS for worker and master notifications

## 1.0.0

- initial versions with following feature:

  - a master that stores the requests
  - several workers that performs the requests
