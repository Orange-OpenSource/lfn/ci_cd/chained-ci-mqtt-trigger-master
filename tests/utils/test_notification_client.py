#!/usr/bin/env python3
# -*- coding: utf-8 -*-
# SPDX-License-Identifier: Apache-2.0
import mock

import json

import paho.mqtt.client as mqtt

from chained_ci_mqtt_trigger.notification_client import NotificationClient


def test_notify_simple(mocker):
    mocker.patch.object(mqtt.Client, 'publish')
    mqtt_client = mqtt.Client()
    notification_transformations = {
        'gerrit_review': 'change.number',
        'gerrit_patchset': 'patchSet.number'
    }
    client = NotificationClient(mqtt_client, "gerrit/reviews",
                                "my_chained_ci_tester",
                                notification_transformations)
    client.additional_variables = {'the_answer': 64}
    payload = json.dumps({
        'change': {
            'number': 42
        },
        'patchSet': {
            'number': '1664'
        }
    })
    client.notify("hello", payload)
    expected_payload = {
        "the_answer": 64,
        "gerrit_review": 42,
        "gerrit_patchset": "1664",
        "message": "hello",
        "score": 0,
        "from": "my_chained_ci_tester"
    }
    mqtt.Client.publish.assert_called_once_with("gerrit/reviews",
                                                json.dumps(expected_payload),
                                                qos=0)
