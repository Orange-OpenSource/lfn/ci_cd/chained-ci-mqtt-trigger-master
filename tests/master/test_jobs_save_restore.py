#!/usr/bin/env python3
# -*- coding: utf-8 -*-
# SPDX-License-Identifier: Apache-2.0
import mock
import pytest

import json
import paho.mqtt.client as mqtt

from chained_ci_mqtt_trigger import master

EXPECTED_CONF = """[
    {
        "a": "b",
        "c": {
            "d": 1
        }
    },
    {
        "e": "f",
        "g": {
            "h": 1
        }
    }
]"""


class TestFileRestoreJobs():
    trigger_config = {
        'magic_word': 'magic',
        'on_comment': 'trigger',
        'change_id': None
    }
    m = master.Master(trigger_config, mqtt.Client(), {
        'notification': "notif",
        'worker': "topic_workers"
    }, {"notification_tranformations": []})

    def test_jobs_to_file(self):
        conf_file = open("/tmp/test.json", 'w')
        conf_file.write("YOLO")
        conf_file.close()
        self.m.configuration_file = "/tmp/test.json"
        self.m.put_job({'a': 'b', 'c': {'d': 1}})
        self.m.put_job({'e': 'f', 'g': {'h': 1}})
        self.m.save_jobs_to_file()
        result = open('/tmp/test.json', 'r')
        assert result.read() == EXPECTED_CONF

    def test_loads_jobs_from_file_bad_JSON(self):
        self.m.jobs.clear()
        conf_file = open("/tmp/test.json", 'w')
        conf_file.write("YOLO")
        conf_file.close()
        self.m.configuration_file = "/tmp/test.json"
        self.m.load_jobs_from_file()
        assert len(self.m.jobs) == 0

    def test_loads_jobs_from_file_OK(self):
        self.m.jobs.clear()
        conf_file = open("/tmp/test.json", 'w')
        conf_file.write(EXPECTED_CONF)
        conf_file.close()
        self.m.configuration_file = "/tmp/test.json"
        self.m.load_jobs_from_file()
        assert len(self.m.jobs) == 2
        assert self.m.jobs.popleft() == {'a': 'b', 'c': {'d': 1}}
        assert self.m.jobs.popleft() == {'e': 'f', 'g': {'h': 1}}
