#!/usr/bin/env python3
# -*- coding: utf-8 -*-
# SPDX-License-Identifier: Apache-2.0
import mock
import pytest

import json
import paho.mqtt.client as mqtt

from chained_ci_mqtt_trigger import master

EXPECTED_CONF = """[
    {
        "a": "b",
        "c": {
            "d": 1
        }
    },
    {
        "e": "f",
        "g": {
            "h": 1
        }
    }
]"""


class TestHandleMasqterMessage():
    trigger_config = {
        'magic_word': 'magic',
        'on_comment': 'trigger',
        'change_id': None
    }
    m = master.Master(trigger_config, mqtt.Client(), {
        'notification': "notif",
        'worker': "topic_workers"
    }, {"notification_tranformations": []})

    def clear_jobs(self):
        self.m.jobs.clear()
        conf_file = open("/tmp/test.json", 'w')
        conf_file.write("[]")
        conf_file.close()
        self.m.configuration_file = "/tmp/test.json"

    def test_handle_master_message_bad_json(self, mocker):
        self.clear_jobs()
        topic = "topic".encode('utf-8')
        mocker.patch.object(mqtt.Client, 'publish')
        msg = mqtt.MQTTMessage(topic=topic)
        msg.payload = "payload".encode('utf-8')
        self.m.put_job("job")
        self.m.handle_master_message(msg)
        result = open('/tmp/test.json', 'r')
        assert json.load(result) == ['job']
        assert self.m.jobs.popleft() == "job"
        mqtt.Client.publish.assert_not_called()

    def test_handle_master_message_no_worker_id(self, mocker):
        self.clear_jobs()
        topic = "topic".encode('utf-8')
        mocker.patch.object(mqtt.Client, 'publish')
        msg = mqtt.MQTTMessage(topic=topic)
        msg.payload = json.dumps({
            "comment": "it's a kind of magic"
        }).encode('utf-8')
        self.m.put_job("job")
        self.m.handle_master_message(msg)
        result = open('/tmp/test.json', 'r')
        assert json.load(result) == ['job']
        assert self.m.jobs.popleft() == "job"
        mqtt.Client.publish.assert_not_called()

    def test_handle_master_message(self, mocker):
        self.clear_jobs()
        topic = "topic".encode('utf-8')
        mocker.patch.object(mqtt.Client, 'publish')
        msg = mqtt.MQTTMessage(topic=topic)
        msg.payload = json.dumps({"worker": "my_worker"}).encode('utf-8')
        msg.topic = "other_topic"
        self.m.put_job({'payload': 'the_job', 'topic': 'topic'})
        self.m.handle_master_message(msg)
        mqtt.Client.publish.assert_called_once_with('topic_workers',
                                                    json.dumps({
                                                        "worker": "my_worker",
                                                        "job": {"payload": "the_job", "topic": "topic"}
                                                    }),
                                                    qos=0)
        assert len(self.m.jobs) == 0
        conf_file = open("/tmp/test.json", 'r')
        assert conf_file.read() == "[]"

    def test_handle_master_message_no_new_jobs(self, mocker):
        self.clear_jobs()
        topic = "topic".encode('utf-8')
        mocker.patch.object(mqtt.Client, 'publish')
        msg = mqtt.MQTTMessage(topic=topic)
        msg.payload = json.dumps({"worker": "myself"}).encode('utf-8')
        self.m.handle_master_message(msg)
        mqtt.Client.publish.assert_not_called()

    def test_handle_master_death_trigger(self, mocker):
        self.clear_jobs()
        topic = "topic".encode('utf-8')
        mocker.patch.object(mqtt.Client, 'publish')
        msg = mqtt.MQTTMessage(topic=topic)
        msg.payload = json.dumps({"shutdown": "please"}).encode('utf-8')
        self.m.handle_master_message(msg)
        mqtt.Client.publish.assert_not_called()
        assert self.m.loop == False
