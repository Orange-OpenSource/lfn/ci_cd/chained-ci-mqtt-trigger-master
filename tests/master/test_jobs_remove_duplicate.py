#!/usr/bin/env python3
# -*- coding: utf-8 -*-
# SPDX-License-Identifier: Apache-2.0
import pytest

import json
import paho.mqtt.client as mqtt

from chained_ci_mqtt_trigger import master

EXPECTED_CONF = """[
    {
        "a": "b",
        "c": {
            "d": 1
        }
    },
    {
        "e": "f",
        "g": {
            "h": 1
        }
    }
]"""


class TestFileRestoreJobs():
    trigger_config = {
        'magic_word': 'magic',
        'on_comment': 'trigger',
        'change_id': 'change.number'
    }
    m = master.Master(trigger_config, mqtt.Client(), {
        'notification': "notif",
        'worker': "topic_workers"
    }, {"notification_tranformations": []})

    def clear_jobs(self):
        self.m.jobs.clear()
        conf_file = open("/tmp/test.json", 'w')
        conf_file.write("[]")
        conf_file.close()
        self.m.configuration_file = "/tmp/test.json"

    def test_remove_duplicate_jobs_bad_JSON(self):
        self.clear_jobs()
        self.m.put_job("job")
        topic = "topic".encode('utf-8')
        msg = mqtt.MQTTMessage(topic=topic)
        msg.payload = "payload".encode('utf-8')
        self.m.remove_duplicate_jobs(msg)
        assert len(self.m.jobs) == 1

    def test_remove_duplicate_jobs_bad_JSON_in_jobs(self):
        self.clear_jobs()
        self.m.put_job({'payload': "job"})
        topic = "topic".encode('utf-8')
        msg = mqtt.MQTTMessage(topic=topic)
        msg.payload = json.dumps({
            "comment": "it's a kind of magic"
        }).encode('utf-8')
        self.m.remove_duplicate_jobs(msg)
        assert len(self.m.jobs) == 1

    def test_remove_duplicate_jobs_no_change_id_in_message(self):
        self.clear_jobs()
        self.m.put_job({'payload': json.dumps({"change": {"number": 12}})})
        topic = "topic".encode('utf-8')
        msg = mqtt.MQTTMessage(topic=topic)
        msg.payload = json.dumps({
            "comment": "it's a kind of magic"
        }).encode('utf-8')
        self.m.remove_duplicate_jobs(msg)
        assert len(self.m.jobs) == 1

    def test_remove_duplicate_jobs_no_duplicate(self):
        self.clear_jobs()
        self.m.put_job({'payload': json.dumps({"change": {"number": 12}})})
        topic = "topic".encode('utf-8')
        msg = mqtt.MQTTMessage(topic=topic)
        msg.payload = json.dumps({'change': {'number': 13}}).encode('utf-8')
        self.m.remove_duplicate_jobs(msg)
        assert len(self.m.jobs) == 1

    def test_remove_duplicate_jobs_one_duplicate(self):
        self.clear_jobs()
        self.m.put_job({
            'payload':
            json.dumps({
                "change": {
                    "number": 12
                },
                "value": "first"
            })
        })
        topic = "topic".encode('utf-8')
        msg = mqtt.MQTTMessage(topic=topic)
        msg.payload = json.dumps({
            'change': {
                'number': 12
            },
            'value': "second"
        }).encode('utf-8')
        self.m.remove_duplicate_jobs(msg)
        assert len(self.m.jobs) == 0

    def test_remove_duplicate_jobs_two_duplicate(self):
        self.clear_jobs()
        self.m.put_job({
            "payload":
            json.dumps({
                "change": {
                    "number": 12
                },
                "other_value": "first"
            })
        })
        self.m.put_job({
            "payload":
            json.dumps({
                "change": {
                    "number": 12
                },
                "other_value": "second"
            })
        })
        self.m.put_job({"payload": json.dumps({"change": {"number": 13}})})
        topic = "topic".encode('utf-8')
        msg = mqtt.MQTTMessage(topic=topic)
        msg.payload = json.dumps({
            "change": {
                "number": 12
            },
            "other_value": "third"
        }).encode('utf-8')
        self.m.remove_duplicate_jobs(msg)
        assert len(self.m.jobs) == 1
