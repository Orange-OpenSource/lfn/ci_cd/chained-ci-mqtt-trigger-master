#!/usr/bin/env python3
# -*- coding: utf-8 -*-
# SPDX-License-Identifier: Apache-2.0
import mock

import json
import paho.mqtt.client as mqtt

from chained_ci_mqtt_trigger import master


class TestHandleNewMessage():
    trigger_config = {
        'magic_word': 'magic',
        'on_comment': 'second_one',
        'change_id': None
    }
    m = master.Master(trigger_config, None, {
        'notification': "notif",
        'worker': "topic_workers"
    }, {"notification_tranformations": []})

    def clear_jobs(self):
        self.m.jobs.clear()
        conf_file = open("/tmp/test.json", 'w')
        conf_file.write("[]")
        conf_file.close()
        self.m.configuration_file = "/tmp/test.json"

    def test_handle_new_message_no_magic_bad_json(self):
        self.clear_jobs()
        assert len(self.m.jobs) == 0
        topic = "gerrit/myproject/second_one".encode('utf-8')
        msg = mqtt.MQTTMessage(topic=topic)
        msg.payload = "payload".encode('utf-8')
        self.m.handle_new_message(msg)
        assert len(self.m.jobs) == 0
        result = open('/tmp/test.json', 'r')
        assert result.read() == "[]"

    def test_handle_new_message_no_magic(self):
        self.clear_jobs()
        assert len(self.m.jobs) == 0
        topic = "gerrit/myproject/second_one".encode('utf-8')
        msg = mqtt.MQTTMessage(topic=topic)
        msg.payload = json.dumps({
            "comment": "Ce n'est pas magique"
        }).encode('utf-8')
        self.m.handle_new_message(msg)
        assert len(self.m.jobs) == 0
        result = open('/tmp/test.json', 'r')
        assert result.read() == "[]"

    @mock.patch(
        'chained_ci_mqtt_trigger.notification_client.NotificationClient.notify'
    )
    def test_handle_new_message_magic(self, mock_method):
        self.clear_jobs()
        assert len(self.m.jobs) == 0
        topic = "gerrit/myproject/second_one".encode('utf-8')
        msg = mqtt.MQTTMessage(topic=topic)
        payload = json.dumps({
            "comment": "it's a kind of magic"
        }).encode('utf-8')
        msg.payload = payload
        self.m.handle_new_message(msg)
        assert self.m.jobs.popleft() == {
            'topic': topic.decode('utf-8'),
            'payload': payload.decode('utf-8')
        }
        result = open('/tmp/test.json', 'r')
        assert json.load(result) == [{
            'topic': topic.decode('utf-8'),
            'payload': payload.decode('utf-8')
        }]
        mock_method.assert_called_once_with(
            "Deployment put on queue, number 1 on the queue.",
            payload.decode('UTF-8'))

    @mock.patch(
        'chained_ci_mqtt_trigger.notification_client.NotificationClient.notify'
    )
    def test_handle_new_message_not_trigger_topic(self, mock_method):
        self.clear_jobs()
        assert len(self.m.jobs) == 0
        topic = "gerrit/myproject/third_one".encode('utf-8')
        msg = mqtt.MQTTMessage(topic=topic)
        payload = json.dumps({"we": "shouldn't care"}).encode('utf-8')
        msg.payload = payload
        self.m.handle_new_message(msg)
        assert self.m.jobs.popleft() == {
            'topic': topic.decode('utf-8'),
            'payload': payload.decode('utf-8')
        }
        result = open('/tmp/test.json', 'r')
        assert json.load(result) == [{
            'topic': topic.decode('utf-8'),
            'payload': payload.decode('utf-8')
        }]
        mock_method.assert_called_once_with(
            "Deployment put on queue, number 1 on the queue.",
            payload.decode('UTF-8'))
