#!/usr/bin/env python3
# -*- coding: utf-8 -*-
# SPDX-License-Identifier: Apache-2.0
import sys
import mock
import pytest

from loguru import logger
import paho.mqtt.client
from chained_ci_mqtt_trigger import master
from chained_ci_mqtt_trigger import worker

from chained_ci_mqtt_trigger import main
from chained_ci_mqtt_trigger.topic import Topic
from chained_ci_mqtt_trigger.xtesting import XtestingClient


@mock.patch('chained_ci_mqtt_trigger.main._create_mqtt_client')
def test_main_master(mock_method, mocker, monkeypatch, valid_config_parsed,
                     valid_config_topics_parsed, valid_config_gitlab_parsed,
                     valid_config_trigger_parsed):
    Topic.topics = []
    mock_class = mocker.patch('chained_ci_mqtt_trigger.main.Master')
    with monkeypatch.context() as m:
        m.setattr(sys, 'argv',
                  ['', '--master', 'etc/chained_ci_mqtt_trigger.conf'])
        main.start()
    expected_userdata = {
        'topics': valid_config_topics_parsed,
        'subtopics': ['patchset-created', 'comment-added'],
        'qos': 2,
        'client': mock.ANY,
    }
    calls = [
        mocker.call(valid_config_parsed, "mqtt.client.publisher",
                    main.on_connect_publish),
        mocker.call(valid_config_parsed,
                    "mqtt.client.subscriber",
                    main.on_connect_subscribe,
                    on_message_callback=main.on_message,
                    userdata=expected_userdata)
    ]
    assert main._create_mqtt_client.call_count == 2

    logger.info(Topic.topics)
    assert len(Topic.topics) == 4
    assert Topic.get('gerrit/myproject').summary_console_jobs == {
        'apps_test:onap_oom_gating_pod4': 'pages',
        'apps_deploy:onap_oom_gating_pod4': 'postconfigure',
    }
    assert Topic.get('gerrit/myproject').is_strict_mode == False
    assert Topic.get('gerrit/submodule/myproject').summary_console_jobs == {
        'apps_test:onap_oom_gating_pod4': 'pages',
        'apps_deploy:onap_oom_gating_pod4': 'postconfigure',
    }
    assert Topic.get('gerrit/submodule/myproject').is_strict_mode == False
    assert Topic.get('gerrit/another/project').summary_console_jobs == {
        'apps_test:onap_oom_gating_pod4': 'pages',
        'apps_deploy:onap_oom_gating_pod4': 'postconfigure',
        'build_so:onap_oom_gating_pod4': 'build',
    }
    assert Topic.get('gerrit/another/project').is_strict_mode == True
    assert Topic.get('gerrit/yet/another/project').summary_console_jobs == {
        'apps_test:onap_oom_gating_pod4': 'pages',
        'apps_deploy:onap_oom_gating_pod4': 'postconfigure',
        'build_so:onap_oom_gating_pod4': 'build',
    }
    assert Topic.get('gerrit/yet/another/project').is_strict_mode == False

    main._create_mqtt_client.assert_has_calls(calls, any_order=True)
    mock_class.assert_called_once_with(valid_config_trigger_parsed,
                                       mock.ANY,
                                       valid_config_topics_parsed,
                                       valid_config_gitlab_parsed,
                                       qos=2)


@mock.patch('chained_ci_mqtt_trigger.main._create_mqtt_client')
def test_main_worker(mock_method, mocker, monkeypatch, valid_config_parsed,
                     valid_config_topics_parsed, valid_config_gitlab_parsed,
                     valid_config_trigger_parsed,
                     valid_config_topics_overrides_parsed):
    mock_class = mocker.patch('chained_ci_mqtt_trigger.main.Worker')
    with monkeypatch.context() as m:
        m.setattr(sys, 'argv',
                  ['', '--worker', 'etc/chained_ci_mqtt_trigger.conf'])
        main.start()
    expected_userdata = {
        'topics': valid_config_topics_parsed,
        'subtopics': ['patchset-created', 'comment-added'],
        'qos': 2,
        'client': mock.ANY,
    }
    Topic.topics = []
    calls = [
        mocker.call(valid_config_parsed, "mqtt.client.publisher",
                    main.on_connect_publish),
        mocker.call(valid_config_parsed,
                    "mqtt.client.subscriber",
                    main.on_connect_subscribe,
                    on_message_callback=main.on_message,
                    userdata=expected_userdata)
    ]
    assert main._create_mqtt_client.call_count == 2
    print(main._create_mqtt_client.call_args_list)
    main._create_mqtt_client.assert_has_calls(calls, any_order=True)
    mock_class.assert_called_once_with(valid_config_trigger_parsed,
                                       mock.ANY,
                                       valid_config_topics_parsed,
                                       valid_config_gitlab_parsed,
                                       qos=2)
    assert XtestingClient.url == 'http://testresults.opnfv.org/onap/'
    assert XtestingClient.build_tag_tpl == 'gitlab_ci-functest-aks-baremetal-daily-master-{{ GERRIT_REVIEW }}-{{ GERRIT_PATCHSET }}'
