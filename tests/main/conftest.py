#!/usr/bin/env python3
# -*- coding: utf-8 -*-
# SPDX-License-Identifier: Apache-2.0
import pytest


@pytest.fixture()
def valid_config_mqtt_parsed():
    return {
        'qos': 2,
        'port': 1883,
        'keepalive': 60,
        'transport': "websockets",
        'hostname': "localhost",
        'auth': {
            'username': 'username',
            'password': 'password'
        }
    }


@pytest.fixture()
def valid_config_trigger_parsed():
    return {
        'magic_word': "gating_recheck",
        'on_comment': 'comment-added',
        'change_id': 'change.number'
    }


@pytest.fixture()
def valid_config_topics_parsed():
    return {
        'mains': [
            'gerrit/myproject', 'gerrit/submodule/myproject',
            'gerrit/another/project', 'gerrit/yet/another/project'
        ],
        'master':
        'gerrit/master/0d083da8-dde6-4a3d-bf4b-5e11f00dbd2b',
        'worker':
        'gerrit/worker/0d083da8-dde6-4a3d-bf4b-5e11f00dbd2b',
        'notification':
        'gerrit/review'
    }


@pytest.fixture()
def valid_config_topics_overrides_parsed():
    return {
        'gerrit/another/project': {
            'case_names': {
                'core': 'Basic SO Health Check',
                'hv-ves': 'full',
                'onap-k8s': 'full'
            },
            'summary_console_jobs': {
                'apps_deploy:onap_oom_gating_pod4': 'postconfigure',
                'apps_test:onap_oom_gating_pod4': 'pages',
                'build_so:onap_oom_gating_pod4': 'build'
            },
            'strict_mode': True,
        },
        'gerrit/submodule/myproject': {
            'case_names': {
                'core': 'Basic SO Health Check',
                'hv-ves': 'full',
                'onap-k8s': 'full'
            },
        },
        'gerrit/yet/another/project': {
            'summary_console_jobs': {
                'apps_deploy:onap_oom_gating_pod4': 'postconfigure',
                'apps_test:onap_oom_gating_pod4': 'pages',
                'build_so:onap_oom_gating_pod4': 'build'
            },
        }
    }


@pytest.fixture()
def valid_config_gitlab_parsed():
    return {
        'hostname':
        'https://gitlab.com',
        'result_url':
        'https://pages.gitlab.io',
        'result_template':
        '{{ change.number}}-{{ patchSet.number }}/',
        'nickname':
        'Gitlab Public',
        'chained_ci_project_id':
        '1234',
        'project_token':
        '_trigger_token_',
        'private_tokens': {
            'gitlab.fqdn': '_private_user_token_',
            'gitlab.com': '_private_user_token_gitlab_com_'
        },
        'git_reference':
        'master',
        'additional_variables': {
            'POD': 'onap_oom_gating_pod4',
            'WORKAROUND': 'False'
        },
        'transformations': {
            'GERRIT_REVIEW': 'change.number',
            'GERRIT_PATCHSET': 'patchSet.number',
            'PROJECT': 'change.project'
        },
        'notification_tranformations': {
            'gerrit_review': 'change.number',
            'gerrit_patchset': 'patchSet.number'
        },
        'summary_console_jobs': {
            'apps_test:onap_oom_gating_pod4': 'pages',
            'apps_deploy:onap_oom_gating_pod4': 'postconfigure'
        },
        'xtesting_db':
        'http://testresults.opnfv.org/onap/',
        'build_tag':
        'gitlab_ci-functest-aks-baremetal-daily-master-{{ GERRIT_REVIEW }}-{{ GERRIT_PATCHSET }}',
        'case_names': {},
        'strict_mode': False,
    }


@pytest.fixture()
def valid_config_parsed(valid_config_mqtt_parsed, valid_config_trigger_parsed,
                        valid_config_topics_parsed, valid_config_gitlab_parsed,
                        valid_config_topics_overrides_parsed):
    return {
        'mqtt': valid_config_mqtt_parsed,
        'mqtt_path': "/path",
        'mqtt_headers': None,
        'saved_jobs_file': 'etc/saved_jobs.json',
        'trigger': valid_config_trigger_parsed,
        'topics': valid_config_topics_parsed,
        'topics_overrides': valid_config_topics_overrides_parsed,
        'subtopics': ['patchset-created', 'comment-added'],
        'gitlab': valid_config_gitlab_parsed
    }
