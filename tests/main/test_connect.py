#!/usr/bin/env python3
# -*- coding: utf-8 -*-
# SPDX-License-Identifier: Apache-2.0
import mock
import pytest

import paho.mqtt.client as mqtt

from chained_ci_mqtt_trigger import main as c


def test_connect_publish():
    c.on_connect_publish(None, None, None, 0)
    assert True == True


BAD_USERDATAS = [
    {
        'yolo': 'topic'
    },
    {
        'topics': {
            'mains': ['topic']
        }
    },
    {
        'topics': {
            'master': 'topic'
        }
    },
    {
        'topics': {
            'worker': 'topic'
        }
    },
    {
        'yolo': 'topic',
        'topics': {
            'mains': ['topic']
        }
    },
    {
        'topics': {
            'master': 'topic',
            'mains': ['topic']
        }
    },
    {
        'topics': {
            'worker': 'topic',
            'main': ['topic']
        }
    },
    {
        'yolo': 'topic',
        'topics': {
            'master': 'topic'
        }
    },
    {
        'topics': {
            'worker': 'topic',
            'master': 'topic'
        }
    },
]


def test_connect_subscribe(mocker):
    userdata = {
        'topics': {
            'mains': ['topic', 'topic2'],
            'master': 'master_topic',
            'worker': 'worker_topic'
        },
        'qos': 2
    }
    mqtt_client = mqtt.Client()
    mocker.patch.object(mqtt.Client, 'subscribe')
    c.on_connect_subscribe(mqtt_client, userdata, None, 0)
    assert mqtt.Client.subscribe.call_count == 4
    mqtt.Client.subscribe.assert_has_calls([
        mocker.call('topic/#', 2),
        mocker.call('topic2/#', 2),
        mocker.call('master_topic', 2),
        mocker.call('worker_topic', 2)
    ],
                                           any_order=True)


@pytest.mark.parametrize("userdata", BAD_USERDATAS)
def test_connect_subscribe_bad_userdata(userdata):
    mqtt_client = mqtt.Client()
    with pytest.raises(KeyError) as excinfo:
        c.on_connect_subscribe(mqtt_client, userdata, None, 0)
    assert excinfo.typename == "KeyError"
