#!/usr/bin/env python3
# -*- coding: utf-8 -*-
# SPDX-License-Identifier: Apache-2.0
import sys
import mock
import pytest
import configparser

from chained_ci_mqtt_trigger import main as c


def test_get_options_no_args(capsys):
    with pytest.raises(SystemExit):
        c._get_options()
    _out, err = capsys.readouterr()
    assert "the following arguments are required: conffile" in err


def test_get_options_help(monkeypatch, capsys):
    with pytest.raises(SystemExit):
        with monkeypatch.context() as m:
            m.setattr(sys, 'argv', ['', "--help"])
            c._get_options()
    out, _err = capsys.readouterr()
    assert "Listen to MQTT Topics controller" in out
    assert "usage: [-h] [-m | -w] [-v] conffile" in out


def test_get_options_conffile(monkeypatch):
    with monkeypatch.context() as m:
        m.setattr(sys, 'argv', ['', 'myfile'])
        args = c._get_options()
        assert args.conffile == "myfile"


def test_get_config_no_file():
    config = c._get_config("yolo")
    assert config.sections() == []


def test_get_config_file():
    config = c._get_config("etc/chained_ci_mqtt_trigger.conf")
    assert config.get('mqtt', 'hostname') == "localhost"


def test_parse_config_ok(valid_config_parsed):
    raw_config = c._get_config("etc/chained_ci_mqtt_trigger.conf")
    config = c._parse_config(raw_config)
    assert config == valid_config_parsed


def test_parse_config_nok():
    with pytest.raises(configparser.NoSectionError) as excinfo:
        raw_config = c._get_config("yolo")
        c._parse_config(raw_config)
    assert "No section:" in excinfo.value.message
