#!/usr/bin/env python3
# -*- coding: utf-8 -*-
# SPDX-License-Identifier: Apache-2.0
import mock
import pytest

import paho.mqtt.client

from chained_ci_mqtt_trigger import main as c


def callback_connect(_client, _userdata, _flags, _return_code):
    return None


def callback_message(_client, _userdata, _flags, _return_code):
    return None


def test_create_mqtt_client_simple(mocker):
    mocker.patch.object(paho.mqtt.client.Client, 'connect')
    config = {
        'mqtt': {
            'qos': 2,
            'port': 1883,
            'keepalive': 60,
            'transport': "websockets",
            'hostname': "localhost",
            'auth': {
                'username': 'username',
                'password': 'password'
            }
        },
        'mqtt_path': "/path",
        'mqtt_headers': None,
        'magic_word': "gating_recheck",
        'trigger_on_comment': 'comment-added',
        'topic': 'gerrit/myproject',
        'master_topic': 'gerrit/master',
        'worker_topic': 'gerrit/worker',
        'master_uuid': '0d083da8-dde6-4a3d-bf4b-5e11f00dbd2b',
        'subtopics': ['patchset-created', 'comment-added']
    }

    client = c._create_mqtt_client(config, "test", callback_connect)
    assert client._transport == "websockets"
    assert client._on_message == None
    assert client._on_connect == callback_connect
    assert client._userdata == None
    assert client._websocket_path == "/path"
    assert client._websocket_extra_headers == None
    paho.mqtt.client.Client.connect.assert_called_once_with(
        "localhost", 1883, 60)


def test_create_mqtt_client_userdata(mocker):
    mocker.patch.object(paho.mqtt.client.Client, 'connect')
    config = {
        'mqtt': {
            'qos': 2,
            'port': 1883,
            'keepalive': 60,
            'transport': "websockets",
            'hostname': "localhost",
            'auth': {
                'username': 'username',
                'password': 'password'
            }
        },
        'mqtt_path': "/path",
        'mqtt_headers': None,
        'magic_word': "gating_recheck",
        'trigger_on_comment': 'comment-added',
        'topic': 'gerrit/myproject',
        'master_topic': 'gerrit/master',
        'worker_topic': 'gerrit/worker',
        'master_uuid': '0d083da8-dde6-4a3d-bf4b-5e11f00dbd2b',
        'subtopics': ['patchset-created', 'comment-added']
    }
    userdata = {"user": "data"}

    client = c._create_mqtt_client(config,
                                   "test",
                                   callback_connect,
                                   userdata=userdata)
    assert client._transport == "websockets"
    assert client._on_message == None
    assert client._on_connect == callback_connect
    assert client._userdata == userdata
    assert client._websocket_path == "/path"
    assert client._websocket_extra_headers == None
    paho.mqtt.client.Client.connect.assert_called_once_with(
        "localhost", 1883, 60)


def test_create_mqtt_client_message_callback(mocker):
    mocker.patch.object(paho.mqtt.client.Client, 'connect')
    config = {
        'mqtt': {
            'qos': 2,
            'port': 1883,
            'keepalive': 60,
            'transport': "websockets",
            'hostname': "localhost",
            'auth': {
                'username': 'username',
                'password': 'password'
            }
        },
        'mqtt_path': "/path",
        'mqtt_headers': None,
        'magic_word': "gating_recheck",
        'trigger_on_comment': 'comment-added',
        'topic': 'gerrit/myproject',
        'master_topic': 'gerrit/master',
        'worker_topic': 'gerrit/worker',
        'master_uuid': '0d083da8-dde6-4a3d-bf4b-5e11f00dbd2b',
        'subtopics': ['patchset-created', 'comment-added']
    }

    client = c._create_mqtt_client(config,
                                   "test",
                                   callback_connect,
                                   on_message_callback=callback_message)
    assert client._transport == "websockets"
    assert client._on_message == callback_message
    assert client._on_connect == callback_connect
    assert client._userdata == None
    assert client._websocket_path == "/path"
    assert client._websocket_extra_headers == None
    paho.mqtt.client.Client.connect.assert_called_once_with(
        "localhost", 1883, 60)
