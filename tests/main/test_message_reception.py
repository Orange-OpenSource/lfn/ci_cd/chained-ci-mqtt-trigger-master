#!/usr/bin/env python3
# -*- coding: utf-8 -*-
# SPDX-License-Identifier: Apache-2.0
import pytest
import mock

import paho.mqtt.client as mqtt

from chained_ci_mqtt_trigger import main as c
from chained_ci_mqtt_trigger import master


@pytest.fixture()
def mock_init(mocker):
    def _mock_init(topic):
        mocker.patch.object(master.Master, 'handle_new_message')
        mocker.patch.object(master.Master, 'handle_master_message')
        mocker.patch.object(master.Master, 'handle_worker_message')
        msg = mqtt.MQTTMessage(topic=topic)
        msg.payload = "payload".encode('utf-8')
        return msg

    return _mock_init


class TestMessages():
    trigger_config = {
        'magic_word': 'magic',
        'on_comment': 'trigger_on_comment',
        'change_id': None
    }
    m = master.Master(trigger_config, None, {
        'notification': "notif",
        'worker': "worker"
    }, {"notification_tranformations": []})
    userdata = {
        "topics": {
            'mains': [
                'gerrit/myproject', 'gerrit/submodule/myproject',
                'gerrit/another/project'
            ],
            "master":
            "gerrit/master/123-456",
            "worker":
            "gerrit/worker/123-456"
        },
        "subtopics": ["first_one", "second_one"],
        "submodules": ["submodule1", "submodule2"],
        "magic_word": "magic",
        "trigger_on_comment": "second_one",
        "client": m
    }
    BAD_MASTER_TOPICS = [
        "gerrit/master/123-456/new".encode('utf-8'),
        "gerrit/master".encode('utf-8')
    ]
    BAD_WORKER_TOPICS = [
        "gerrit/worker/123-456/new".encode('utf-8'),
        "gerrit/worker".encode('utf-8')
    ]

    def test_topic_not_in_subtopics(self, mocker, mock_init):
        msg = mock_init("gerrit/myproject/third_one".encode('utf-8'))
        c.on_message(None, self.userdata, msg)
        master.Master.handle_new_message.assert_not_called()
        master.Master.handle_master_message.assert_not_called()
        master.Master.handle_worker_message.assert_not_called()

    def test_topic_in_subtopics(self, mocker, mock_init):
        msg = mock_init("gerrit/myproject/first_one".encode('utf-8'))
        c.on_message(None, self.userdata, msg)
        master.Master.handle_new_message.assert_called_once_with(msg)
        master.Master.handle_master_message.assert_not_called()
        master.Master.handle_worker_message.assert_not_called()

    def test_topic_in_subtopics_and_submodule(self, mocker, mock_init):
        msg = mock_init("gerrit/submodule/myproject/first_one".encode('utf-8'))
        c.on_message(None, self.userdata, msg)
        master.Master.handle_new_message.assert_called_once_with(msg)
        master.Master.handle_master_message.assert_not_called()
        master.Master.handle_worker_message.assert_not_called()

    def test_topic_in_subtopics_but_bad_submodule(self, mocker, mock_init):
        msg = mock_init(
            "gerrit/submodule3/myproject/first_one".encode('utf-8'))
        c.on_message(None, self.userdata, msg)
        master.Master.handle_new_message.assert_not_called()
        master.Master.handle_master_message.assert_not_called()
        master.Master.handle_worker_message.assert_not_called()

    def test_topic_is_master_topic(self, mocker, mock_init):
        msg = mock_init("gerrit/master/123-456".encode('utf-8'))
        c.on_message(None, self.userdata, msg)
        master.Master.handle_new_message.assert_not_called()
        master.Master.handle_master_message.assert_called_once_with(msg)
        master.Master.handle_worker_message.assert_not_called()

    def test_topic_is_worker_topic(self, mocker, mock_init):
        msg = mock_init("gerrit/worker/123-456".encode('utf-8'))
        c.on_message(None, self.userdata, msg)
        master.Master.handle_new_message.assert_not_called()
        master.Master.handle_master_message.assert_not_called()
        master.Master.handle_worker_message.assert_called_once_with(msg)

    @pytest.mark.parametrize("topic", BAD_MASTER_TOPICS)
    def test_topic_not_master_topic(self, mocker, mock_init, topic):
        msg = mock_init(topic)
        c.on_message(None, self.userdata, msg)
        master.Master.handle_new_message.assert_not_called()
        master.Master.handle_master_message.assert_not_called()
        master.Master.handle_worker_message.assert_not_called()

    @pytest.mark.parametrize("topic", BAD_WORKER_TOPICS)
    def test_topic_not_worker_topic(self, mocker, mock_init, topic):
        msg = mock_init(topic)
        c.on_message(None, self.userdata, msg)
        master.Master.handle_new_message.assert_not_called()
        master.Master.handle_master_message.assert_not_called()
        master.Master.handle_worker_message.assert_not_called()
