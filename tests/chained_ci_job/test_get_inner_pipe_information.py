#!/usr/bin/env python3
# -*- coding: utf-8 -*-
# SPDX-License-Identifier: Apache-2.0import mock
import os
from pathlib import Path

import mock
import pytest
import responses

from chained_ci_mqtt_trigger.chained_ci import ChainedCi
from chained_ci_mqtt_trigger.chained_ci_job import ChainedCiJob
from chained_ci_mqtt_trigger.chained_ci_run import ChainedCiRun
from chained_ci_mqtt_trigger.gitlab_client import GitlabClient
from chained_ci_mqtt_trigger.http_adapter import Http

dir_path = os.path.dirname(os.path.realpath(__file__))

SUCCESS_INNER_PIPELINE = Path(
    dir_path + "/success_inner_pipeline.json").read_text().replace('\n', '')
FAILED_INNER_PIPELINE = Path(
    dir_path + "/failed_inner_pipeline.json").read_text().replace('\n', '')

EXPECTED_INNER_PIPE_ACCESS_FAILURE_OUTPUT = """
 * Step: test_job
   Status: failed
   Pipeline: http://no-output.url/api/v4/projects/1234/pipelines/12
   Failure Reason: issue when connecting to Inner Pipeline Gitlab"""

EXPECTED_INNER_PIPE_ACCESS_FAILURE_JSON_OUTPUT = """
 * Step: test_job
   Status: failed
   Pipeline: http://no-json.url/api/v4/projects/1234/pipelines/12
   Failure Reason: can't parse Inner Pipeline Gitlab JSON response"""

EXPECTED_INNER_PIPE_CONNECT_FAILURE_OUTPUT = """
 * Step: test_job
   Status: failed
   Pipeline: http://exception.url/api/v4/projects/1234/pipelines/12
   Failure Reason: can't connect to Inner Pipeline Gitlab"""


def request_callback_gitlab(request):
    if request.headers['PRIVATE-TOKEN'] != 'gitlab_token' or request.headers[
        'Accept'
    ] != 'application/json':
        return (404, {}, None)
    if request.url == 'https://gitlab.com/Orange-OpenSource/lfn/onap/xtesting-onap/pipelines/213503661':
        return (200, {}, SUCCESS_INNER_PIPELINE)
    if request.url == 'https://gitlab.com/Orange-OpenSource/lfn/onap/xtesting-onap/pipelines/213503660':
        return (200, {}, FAILED_INNER_PIPELINE)
    return (404, {}, None)


def generate_requests_mocks():
    responses.add_callback(
        responses.GET,
        'https://gitlab.com/Orange-OpenSource/lfn/onap/xtesting-onap/pipelines/213503661',
        callback=request_callback_gitlab,

        content_type='application/json',
    )
    responses.add_callback(
        responses.GET,
        'https://gitlab.com/Orange-OpenSource/lfn/onap/xtesting-onap/pipelines/213503660',
        callback=request_callback_gitlab,
        content_type='application/json',
    )
    responses.add(
        method='GET',
        url='http://no-json.url/api/v4/projects/1234/pipelines/12',
        body='no-json',
    )
    responses.add(
        method='GET',
        url='http://no-output.url/api/v4/projects/1234/pipelines/12',
        status=404,
    )


def init_objects(url, use_bad_token=False, topic='topic/summary'):
    chained_ci = ChainedCi("http://{}.url".format(url), 'https://pages.io',
                           1234, '', '')
    token = "my_private_token"
    if use_bad_token:
        token = "bad_one"
    client = GitlabClient(chained_ci, {
        "{}.url".format(url): token,
        "gitlab.com": "gitlab_token"
    }, None)
    generate_requests_mocks()
    payload = {"payload": {"value": 12}}
    run = ChainedCiRun(client, chained_ci, 12, payload, topic)
    job = ChainedCiJob({}, 64, 'test_job')
    job.run = run
    job.inner_jobs = []
    job.inner_pipe = None
    job._status = None
    job.output = ""
    job.summary = None
    return job


@responses.activate
def test_get_inner_pipe_information_failed_output():
    job = init_objects("failed-output")
    inner_pipe = "https://gitlab.com/Orange-OpenSource/lfn/onap/xtesting-onap/pipelines/213503660"
    job.inner_pipe = inner_pipe
    job.get_inner_pipe_information()
    assert job.status == "failed"
    assert len(job.inner_jobs) == 20
    assert job.inner_jobs[0]['name'] == 'prepare'
    assert job.inner_jobs[0]['status'] == 'success'
    assert job.inner_jobs[0][
        'path'] == '/Orange-OpenSource/lfn/onap/xtesting-onap/-/jobs/840306905'
    assert job.inner_jobs[8]['name'] == 'pnf_registrate'
    assert job.inner_jobs[8]['status'] == 'failed-with-warnings'
    assert job.inner_jobs[8][
        'path'] == '/Orange-OpenSource/lfn/onap/xtesting-onap/-/jobs/840306919'
    assert job.inner_jobs[18]['name'] == 'pages'
    assert job.inner_jobs[18]['status'] == 'success'
    assert job.inner_jobs[18][
        'path'] == '/Orange-OpenSource/lfn/onap/xtesting-onap/-/jobs/840306953'


@responses.activate
def test_get_inner_pipe_information_success_output():
    job = init_objects("failed-output")
    inner_pipe = "https://gitlab.com/Orange-OpenSource/lfn/onap/xtesting-onap/pipelines/213503661"
    job.inner_pipe = inner_pipe
    job.get_inner_pipe_information()
    assert job.status == "success"
    assert len(job.inner_jobs) == 7
    assert job.inner_jobs[3]['name'] == 'deploy_full'
    assert job.inner_jobs[3]['status'] == 'success'
    assert job.inner_jobs[3][
        'path'] == '/Orange-OpenSource/lfn/onap/onap_oom_automatic_installation/-/jobs/837468330'


@responses.activate
def test_get_inner_pipe_information_bad_json():
    job = init_objects("failed-output")
    Http.default_max_retries = 2
    Http.default_backoff = 0
    inner_pipe = "http://no-json.url/api/v4/projects/1234/pipelines/12"
    job.inner_pipe = inner_pipe
    job.get_inner_pipe_information()
    assert job.status == "failed"
    assert job.output == EXPECTED_INNER_PIPE_ACCESS_FAILURE_JSON_OUTPUT
    assert job.summary is None


@responses.activate
def test_get_inner_pipe_information_bad_request():
    job = init_objects("failed-output")
    Http.default_max_retries = 2
    Http.default_backoff = 0
    inner_pipe = "http://no-output.url/api/v4/projects/1234/pipelines/12"
    job.inner_pipe = inner_pipe
    job.get_inner_pipe_information()
    assert job.status == "failed"
    assert job.output == EXPECTED_INNER_PIPE_ACCESS_FAILURE_OUTPUT
    assert job.summary is None


@responses.activate
def test_get_inner_pipe_information_bad_connection():
    job = init_objects("exception")
    Http.default_max_retries = 2
    Http.default_backoff = 0
    inner_pipe = "http://exception.url/api/v4/projects/1234/pipelines/12"
    job.inner_pipe = inner_pipe
    job.get_inner_pipe_information()
    assert job.status == "failed"
    assert job.output == EXPECTED_INNER_PIPE_CONNECT_FAILURE_OUTPUT
    assert job.summary is None
