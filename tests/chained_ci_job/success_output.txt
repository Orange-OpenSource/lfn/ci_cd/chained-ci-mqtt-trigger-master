TASK [run-ci : set pipeline url] ***********************************************
            ok: [onap_oom_gating_azure_1]
            TASK [run-ci : Echo running pipeline link] *************************************
            ok: [onap_oom_gating_azure_1] => {
                "msg": [
                    "******************************************************************",
                    "* Pipeline triggered for step 'onap_test'",
                    "* https://gitlab.com/Orange-OpenSource/lfn/onap/xtesting-onap//pipelines/213503661",
                    "******************************************************************",
                    ""
                ]
            }
            TASK [run-ci : set grafana start point] ****************************************
            skipping: [onap_oom_gating_azure_1]
            TASK [run-ci : Wait for pipeline result onap_test] *****************************
            FAILED - RETRYING: Wait for pipeline result onap_test (600 retries left).
            FAILED - RETRYING: Wait for pipeline result onap_test (599 retries left).
            FAILED - RETRYING: Wait for pipeline result onap_test (598 retries left).
            FAILED - RETRYING: Wait for pipeline result onap_test (597 retries left).
            FAILED - RETRYING: Wait for pipeline result onap_test (596 retries left).
            FAILED - RETRYING: Wait for pipeline result onap_test (595 retries left).
            }