#!/usr/bin/env python3
# -*- coding: utf-8 -*-
# SPDX-License-Identifier: Apache-2.0import mock
import pytest
import mock

from chained_ci_mqtt_trigger.chained_ci_job import ChainedCiJob

JOB = ChainedCiJob(None, 64, 'test_job')


def update_status():
    JOB.status = True


@mock.patch.object(ChainedCiJob, 'get_information')
def test_not_finished(get_information):
    JOB.status = False
    assert JOB.status == False
    get_information.assert_called_once()


@mock.patch.object(ChainedCiJob, 'get_information')
def test_already_finished(get_information):
    JOB.status = True
    assert JOB.status == True
    get_information.assert_not_called()


@mock.patch(
    'chained_ci_mqtt_trigger.chained_ci_run.ChainedCiJob.get_information')
def test_finished(get_information):
    JOB.status = False
    get_information.side_effect = update_status
    assert JOB.status == True
    get_information.assert_called_once()