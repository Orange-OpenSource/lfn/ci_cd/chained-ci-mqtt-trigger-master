#!/usr/bin/env python3
# -*- coding: utf-8 -*-
# SPDX-License-Identifier: Apache-2.0import mock
import mock
import pytest

from chained_ci_mqtt_trigger.chained_ci_job import ChainedCiJob
from chained_ci_mqtt_trigger.chained_ci_run import ChainedCiRun
from chained_ci_mqtt_trigger.gitlab_client import GitlabClient
from chained_ci_mqtt_trigger.topic import Topic


def mocked_parse_output(path):
    if path == "summary":
        return "nice_summary"
    if path == "other/summary":
        return "other_summary"
    return ''


def init_objects(topic_name):
    Topic.topics = []
    Topic('topic/nosummary', summary_console_jobs={})
    Topic('topic/summary', summary_console_jobs={'test_job': 'oui'})
    Topic('topic', summary_console_jobs={'test_job': 'yolo'})
    client = GitlabClient(None, {}, None)

    payload = {"payload": {"value": 12}}
    run = ChainedCiRun(client, None, 12, payload, Topic.get(topic_name))
    job = ChainedCiJob(run, 64, 'test_job')
    job.parse_output = mock.Mock()
    job.inner_jobs = [
        {
            'name': 'yolo',
            'path': 'summary'
        },
        {
            'name': 'oui',
            'path': 'other/summary'
        },
        {
            'name': 'nope',
            'path': 'nosummary'
        },
    ]
    job.parse_output.side_effect = mocked_parse_output
    return job


def test_get_summary_override_summary():
    job = init_objects('topic/summary')
    assert job.get_summary() == "other_summary"


def test_get_summary_override_no_summary():
    job = init_objects('topic/summary')
    job.inner_jobs = [
        {
            'name': 'yolo',
            'path': 'other/summary'
        },
        {
            'name': 'nope',
            'path': 'nosummary'
        },
    ]
    assert job.get_summary() == ""


def test_get_summary_other_override_summary():
    job = init_objects('topic')
    assert job.get_summary() == "nice_summary"


def test_get_summary_no_summary():
    job = init_objects('topic/nosummary')
    assert job.get_summary() == ""
