#!/usr/bin/env python3
# -*- coding: utf-8 -*-
# SPDX-License-Identifier: Apache-2.0import mock
import mock
import pytest
import responses

from chained_ci_mqtt_trigger.chained_ci import ChainedCi
from chained_ci_mqtt_trigger.chained_ci_job import ChainedCiJob
from chained_ci_mqtt_trigger.chained_ci_run import ChainedCiRun
from chained_ci_mqtt_trigger.gitlab_client import GitlabClient

EXPECTED_OUTPUT = """______________ Results _____________
******** Kubernetes Results ********
* Nb Pods: 200
* Nb Failed Pods: 26
* List of Failed Pods: [
    - onap-aai-aai-78b95bdcfb-gcm97
    - onap-aai-aai-champ-7f97f98976-rqvd5
    - onap-aai-aai-graphadmin-c79fb7fcf-q6wcl
    - onap-aai-aai-graphadmin-create-db-schema-wv7mn
    - onap-aai-aai-resources-557c6f9d78-qzf8d
    - onap-aai-aai-sparky-be-5ff8689c54-lgsmk
    - onap-aai-aai-spike-77ccc5bf78-7l9hk
    - onap-aai-aai-traversal-7fb5558d98-hjp2s
    - onap-aai-aai-traversal-update-query-data-qggnx
    - onap-contrib-netbox-app-695fd89fd-x68gn
    - onap-contrib-netbox-app-provisioning-cht7l
    - onap-dcaegen2-dcae-bootstrap-d8bbc99f7-2vgfh
    - onap-dcaegen2-dcae-cloudify-manager-d7b597474-75zsp
    - onap-dcaegen2-dcae-deployment-handler-5dcd748c6-w6pmw
    - onap-dcaegen2-dcae-healthcheck-84775fddd8-5f4lh
    - onap-dcaegen2-dcae-policy-handler-85c5b8fcb9-p77zb
    - onap-dmaap-dmaap-bc-68f4565db7-hc5gb
    - onap-sdnc-blueprints-processor-6d8c669cb-hxcwc
    - onap-sdnc-controller-blueprints-64b8cf4c7c-882cl
    - onap-so-so-bpmn-infra-7b4cf7dbf4-26v49
    - onap-so-so-catalog-db-adapter-684f7f7f57-6xtz6
    - onap-so-so-f57fb6c78-q69cf
    - onap-so-so-openstack-adapter-6c75fcdd89-4rhcv
    - onap-so-so-request-db-adapter-6797d64c57-vtwk8
    - onap-so-so-sdc-controller-849949db55-dhwvl
    - onap-so-so-vfc-adapter-6df5476d68-4hcst]
* Deployment Duration Estimation (s): 3600
*********** Helm Results ***********
* Nb Helm Charts: 27
* Nb Failed Helm Charts: 2
* List of Failed Helm Charts: [
    - onap-dmaap
    - onap-vnfsdk]
******** Healthcheck Results *******
2019-03-26 11:23:48,406 - xtesting.core.robotframework - INFO -
==============================================================================
Health-Check :: Testing ecomp components are available via calls.
==============================================================================
Basic A&AI Health Check                                               | FAIL |
Test timeout 10 seconds exceeded.
------------------------------------------------------------------------------
Basic OOF-Homing Health Check                                         | FAIL |
ConnectionError: HTTPConnectionPool(host='oof-has-api.onap', port=8091): Max retries exceeded with url: /v1/plans/healthcheck (Caused by NewConnectionError('<urllib3.connection.HTTPConnection object at 0x7f39aea6f290>: Failed to establish a new connection: [Errno -2] Name does not resolve',))
------------------------------------------------------------------------------
Basic OOF-SNIRO Health Check                                          | FAIL |
ConnectionError: HTTPConnectionPool(host='oof-osdf.onap', port=8698): Max retries exceeded with url: /api/oof/v1/healthcheck (Caused by NewConnectionError('<urllib3.connection.HTTPConnection object at 0x7f39aea196d0>: Failed to establish a new connection: [Errno -2] Name does not resolve',))
------------------------------------------------------------------------------
Basic OOF-CMSO Health Check                                           | FAIL |
ConnectionError: HTTPConnectionPool(host='oof-cmso.onap', port=8080): Max retries exceeded with url: /cmso/v1/health?checkInterfaces=false (Caused by NewConnectionError('<urllib3.connection.HTTPConnection object at 0x7f39aea49e90>: Failed to establish a new connection: [Errno -2] Name does not resolve',))
------------------------------------------------------------------------------
Basic Policy Health Check                                             | PASS |
------------------------------------------------------------------------------
Basic VNFSDK Health Check                                             | FAIL |
ConnectionError: HTTPConnectionPool(host='refrepo.onap', port=8702): Max retries exceeded with url: /onapapi/vnfsdk-marketplace/v1/PackageResource/healthcheck (Caused by NewConnectionError('<urllib3.connection.HTTPConnection object at 0x7f39ae94bd10>: Failed to establish a new connection: [Errno -2] Name does not resolve',))
------------------------------------------------------------------------------
Basic Holmes Rule Management API Health Check                         | FAIL |
502 != 200
------------------------------------------------------------------------------
Basic Holmes Engine Management API Health Check                       | FAIL |
502 != 200
------------------------------------------------------------------------------
Health-Check :: Testing ecomp components are available via calls.     | FAIL |
57 critical tests, 38 passed, 19 failed
57 tests total, 38 passed, 19 failed
==============================================================================
Output:  /var/lib/xtesting/results/full/output.xml
____________________________________"""

OUTPUT = """
sent 934.80K bytes  received 615 bytes  623.61K bytes/sec
total size is 3.93M  speedup is 4.20
$ scripts/output_summary.sh
______________ Results _____________
******** Kubernetes Results ********
* Nb Pods: 200
* Nb Failed Pods: 26
* List of Failed Pods: [
    - onap-aai-aai-78b95bdcfb-gcm97
    - onap-aai-aai-champ-7f97f98976-rqvd5
    - onap-aai-aai-graphadmin-c79fb7fcf-q6wcl
    - onap-aai-aai-graphadmin-create-db-schema-wv7mn
    - onap-aai-aai-resources-557c6f9d78-qzf8d
    - onap-aai-aai-sparky-be-5ff8689c54-lgsmk
    - onap-aai-aai-spike-77ccc5bf78-7l9hk
    - onap-aai-aai-traversal-7fb5558d98-hjp2s
    - onap-aai-aai-traversal-update-query-data-qggnx
    - onap-contrib-netbox-app-695fd89fd-x68gn
    - onap-contrib-netbox-app-provisioning-cht7l
    - onap-dcaegen2-dcae-bootstrap-d8bbc99f7-2vgfh
    - onap-dcaegen2-dcae-cloudify-manager-d7b597474-75zsp
    - onap-dcaegen2-dcae-deployment-handler-5dcd748c6-w6pmw
    - onap-dcaegen2-dcae-healthcheck-84775fddd8-5f4lh
    - onap-dcaegen2-dcae-policy-handler-85c5b8fcb9-p77zb
    - onap-dmaap-dmaap-bc-68f4565db7-hc5gb
    - onap-sdnc-blueprints-processor-6d8c669cb-hxcwc
    - onap-sdnc-controller-blueprints-64b8cf4c7c-882cl
    - onap-so-so-bpmn-infra-7b4cf7dbf4-26v49
    - onap-so-so-catalog-db-adapter-684f7f7f57-6xtz6
    - onap-so-so-f57fb6c78-q69cf
    - onap-so-so-openstack-adapter-6c75fcdd89-4rhcv
    - onap-so-so-request-db-adapter-6797d64c57-vtwk8
    - onap-so-so-sdc-controller-849949db55-dhwvl
    - onap-so-so-vfc-adapter-6df5476d68-4hcst]
* Deployment Duration Estimation (s): 3600
*********** Helm Results ***********
* Nb Helm Charts: 27
* Nb Failed Helm Charts: 2
* List of Failed Helm Charts: [
    - onap-dmaap
    - onap-vnfsdk]
******** Healthcheck Results *******
2019-03-26 11:23:48,406 - xtesting.core.robotframework - INFO -
==============================================================================
Health-Check :: Testing ecomp components are available via calls.
==============================================================================
Basic A&AI Health Check                                               | FAIL |
Test timeout 10 seconds exceeded.
------------------------------------------------------------------------------
Basic OOF-Homing Health Check                                         | FAIL |
ConnectionError: HTTPConnectionPool(host='oof-has-api.onap', port=8091): Max retries exceeded with url: /v1/plans/healthcheck (Caused by NewConnectionError('<urllib3.connection.HTTPConnection object at 0x7f39aea6f290>: Failed to establish a new connection: [Errno -2] Name does not resolve',))
------------------------------------------------------------------------------
Basic OOF-SNIRO Health Check                                          | FAIL |
ConnectionError: HTTPConnectionPool(host='oof-osdf.onap', port=8698): Max retries exceeded with url: /api/oof/v1/healthcheck (Caused by NewConnectionError('<urllib3.connection.HTTPConnection object at 0x7f39aea196d0>: Failed to establish a new connection: [Errno -2] Name does not resolve',))
------------------------------------------------------------------------------
Basic OOF-CMSO Health Check                                           | FAIL |
ConnectionError: HTTPConnectionPool(host='oof-cmso.onap', port=8080): Max retries exceeded with url: /cmso/v1/health?checkInterfaces=false (Caused by NewConnectionError('<urllib3.connection.HTTPConnection object at 0x7f39aea49e90>: Failed to establish a new connection: [Errno -2] Name does not resolve',))
------------------------------------------------------------------------------
Basic Policy Health Check                                             | PASS |
------------------------------------------------------------------------------
Basic VNFSDK Health Check                                             | FAIL |
ConnectionError: HTTPConnectionPool(host='refrepo.onap', port=8702): Max retries exceeded with url: /onapapi/vnfsdk-marketplace/v1/PackageResource/healthcheck (Caused by NewConnectionError('<urllib3.connection.HTTPConnection object at 0x7f39ae94bd10>: Failed to establish a new connection: [Errno -2] Name does not resolve',))
------------------------------------------------------------------------------
Basic Holmes Rule Management API Health Check                         | FAIL |
502 != 200
------------------------------------------------------------------------------
Basic Holmes Engine Management API Health Check                       | FAIL |
502 != 200
------------------------------------------------------------------------------
Health-Check :: Testing ecomp components are available via calls.     | FAIL |
57 critical tests, 38 passed, 19 failed
57 tests total, 38 passed, 19 failed
==============================================================================
Output:  /var/lib/xtesting/results/full/output.xml
____________________________________
Running after script...
$ ./scripts/chained-ci-tools/clean.sh

=====================================================================
"""


def request_callback(request):
    if request.headers['PRIVATE-TOKEN'] != 'my_private_token':
        return (404, {}, None)
    if request.url == 'http://output.url/api/v4/projects/1234/jobs/3/trace':
        return (200, {}, 'output')
    if request.url == 'http://output.url/api/v4/projects/1234/jobs/64/trace':
        return (200, {}, OUTPUT)
    return (404, {}, None)


def generate_requests_mocks():
    responses.add_callback(
        responses.GET,
        'http://output.url/api/v4/projects/1234/jobs/3/trace',
        callback=request_callback,
    )
    responses.add_callback(
        responses.GET,
        'http://output.url/api/v4/projects/1234/jobs/64/trace',
        callback=request_callback,
    )


def init_objects(use_bad_token=False, topic='topic/summary'):
    chained_ci = ChainedCi("http://output.url", 'https://pages.io', 1234, '',
                           '')
    chained_ci.get_project_id = mock.Mock()
    chained_ci.get_project_id.return_value = '1234'
    token = "my_private_token"
    if use_bad_token:
        token = "bad_one"
    client = GitlabClient(chained_ci, {
        "output.url": token,
        "gitlab.com": "gitlab_token"
    }, None)
    generate_requests_mocks()
    payload = {"payload": {"value": 12}}
    run = ChainedCiRun(client, chained_ci, 12, payload, topic)
    job = ChainedCiJob(run, 64, 'test_job')
    job.root_url = "http://output.url"
    return job


@responses.activate
def test_parse_output_OK():
    job = init_objects()
    assert job.parse_output(
        "/Orange-OpenSource/lfn/onap/xtesting-onap/-/jobs/64") == EXPECTED_OUTPUT


@responses.activate
def test_parse_output_no_results():
    job = init_objects()
    assert job.parse_output(
        "/Orange-OpenSource/lfn/onap/xtesting-onap/-/jobs/3") == ""


@responses.activate
def test_parse_output_bad_token():
    job = init_objects(use_bad_token=True)
    assert job.parse_output(
        "/Orange-OpenSource/lfn/onap/xtesting-onap/-/jobs/64") == ""


@responses.activate
def test_parse_output_bad_url_pipeline_jobs():
    job = init_objects()
    assert job.parse_output(
        "/Orange-OpenSource/lfn/onap/xtesting-onap/-/jobs/30") == ""
