#!/usr/bin/env python3
# -*- coding: utf-8 -*-
# SPDX-License-Identifier: Apache-2.0
import os
from pathlib import Path

import mock
import pytest
import responses
from urllib3.exceptions import MaxRetryError

from chained_ci_mqtt_trigger import gitlab_client
from chained_ci_mqtt_trigger.chained_ci import ChainedCi
from chained_ci_mqtt_trigger.chained_ci_job import ChainedCiJob
from chained_ci_mqtt_trigger.chained_ci_run import ChainedCiRun
from chained_ci_mqtt_trigger.http_adapter import Http

dir_path = os.path.dirname(os.path.realpath(__file__))

SUCCESS_INNER_PIPELINE = Path(
    dir_path + "/success_inner_pipeline.json").read_text().replace('\n', '')
FAILED_INNER_PIPELINE = Path(
    dir_path + "/failed_inner_pipeline.json").read_text().replace('\n', '')
SUCCESS_TRACE = Path(dir_path + "/success_output.txt").read_text()
FAILED_TRACE = Path(dir_path + "/failed_output.txt").read_text()
CHAINED_CI_ERROR_TRACE = Path(dir_path +
                              "/chained_ci_error_output.txt").read_text()

EXPECTED_FAILED_OUTPUT = """
 * Step: test_job
   Status: failed
   Pipeline: https://gitlab.com/Orange-OpenSource/lfn/onap/xtesting-onap/pipelines/213503660
   Failed jobs:
     - name: inner_job_failed
       link: https://gitlab.com/my_url
     - name: inner_job_skipped
       link: https://gitlab.com/my_url
     - name: other_inner_job_failed
       link: https://gitlab.com/my_other_url"""

EXPECTED_CHAINED_CI_FAILURE_OUTPUT = """
 * Step: test_job
   Status: failed
   Pipeline: http://chained-ci-error-output.url/api/v4/projects/1234/jobs/64/trace
   Failure Reason: can't find Inner Pipeline"""

EXPECTED_CHAINED_CI_ACCESS_FAILURE_OUTPUT = """
 * Step: test_job
   Status: failed
   Pipeline: http://chained-ci-error-output.url/api/v4/projects/1234/jobs/64/trace
   Failure Reason: can't connect to Gitlab CI API"""

EXPECTED_CHAINED_CI_CONNECT_FAILURE_OUTPUT = """
 * Step: test_job
   Status: failed
   Pipeline: http://chained-ci-error-connect.url/api/v4/projects/1234/jobs/64/trace
   Failure Reason: can't connect to Gitlab CI API"""

JOB = ChainedCiJob({}, 64, 'test_job')


def mocked_get_summary():
    return 'nice_summary'


def mocked_inner_info():
    if JOB.inner_pipe == "https://gitlab.com/Orange-OpenSource/lfn/onap/xtesting-onap/pipelines/213503660":
        JOB.status = "failed"
        JOB.inner_jobs = [
            {
                "name": "inner_job_success",
                "status": "success",
                "path": "/my_url"
            },
            {
                "name": "inner_job_failed",
                "status": "failed",
                "path": "/my_url"
            },
            {
                "name": "inner_job_skipped",
                "status": "skipped",
                "path": "/my_url"
            },
            {
                "name": "other_inner_job_failed",
                "status": "skipped",
                "path": "/my_other_url"
            },
        ]
    if JOB.inner_pipe == "https://gitlab.com/Orange-OpenSource/lfn/onap/xtesting-onap/pipelines/213503661":
        JOB.status = "success"
        JOB.inner_jobs = [
            {
                "name": "inner_job_success",
                "status": "success",
                "path": "/my_url"
            },
            {
                "name": "inner_job_other",
                "status": "success",
                "path": "/my_url"
            },
            {
                "name": "inner_job_another",
                "status": "success",
                "path": "/my_url"
            },
            {
                "name": "other_inner_a_last",
                "status": "success",
                "path": "/my_other_url"
            },
        ]


def request_callback_normal(request):
    if request.headers['PRIVATE-TOKEN'] != 'my_private_token':
        return (404, {}, None)
    if request.url == 'http://failed-output.url/api/v4/projects/1234/jobs/64/trace':
        return (200, {}, FAILED_TRACE)
    if request.url == 'http://success-output.url/api/v4/projects/1234/jobs/64/trace':
        return (200, {}, SUCCESS_TRACE)
    if request.url == 'http://chained-ci-error-output.url/api/v4/projects/1234/jobs/64/trace':
        return (200, {}, CHAINED_CI_ERROR_TRACE)
    if request.url == 'http://no-output.url/api/v4/projects/1234/pipelines/12':
        return (200, {}, 'output')


def request_callback_gitlab(request):
    if request.headers['PRIVATE-TOKEN'] != 'gitlab_token' or request.headers[
        'Accept'
    ] == 'content/json':
        return (404, {}, None)
    if request.url == 'https://gitlab.com/Orange-OpenSource/lfn/onap/xtesting-onap/pipelines/213503661':
        return (200, {}, SUCCESS_INNER_PIPELINE)
    if request.url == 'https://gitlab.com/Orange-OpenSource/lfn/onap/xtesting-onap/pipelines/213503660':
        return (200, {}, FAILED_INNER_PIPELINE)


def generate_requests_mocks():
    responses.add_callback(
        responses.GET,
        'http://failed-output.url/api/v4/projects/1234/jobs/64/trace',
        callback=request_callback_normal,
        content_type='application/json',
    )
    responses.add_callback(
        responses.GET,
        'http://success-output.url/api/v4/projects/1234/jobs/64/trace',
        callback=request_callback_normal,
        content_type='application/json',
    )
    responses.add_callback(
        responses.GET,
        'http://chained-ci-error-output.url/api/v4/projects/1234/jobs/64/trace',
        callback=request_callback_normal,
        content_type='application/json',
    )
    responses.add_callback(
        responses.GET,
        'http://no-output.url/api/v4/projects/1234/pipelines/12',
        callback=request_callback_normal,
        content_type='application/json',
    )
    responses.add_callback(
        responses.GET,
        'https://gitlab.com/Orange-OpenSource/lfn/onap/xtesting-onap/pipelines/213503661',
        callback=request_callback_gitlab,
        content_type='application/json',
    )
    responses.add_callback(
        responses.GET,
        'https://gitlab.com/Orange-OpenSource/lfn/onap/xtesting-onap/pipelines/213503660',
        callback=request_callback_gitlab,
        content_type='application/json',
    )
    responses.add(
        method='GET',
        url='http://chained-ci-error-connect.url/api/v4/projects/1234/jobs/64/trace',
        body=MaxRetryError(
            None,
            'http://chained-ci-error-connect.url/api/v4/projects/1234/jobs/64/trace',
        ),
    )


def init_objects(url, use_bad_token=False, topic='topic/summary'):
    chained_ci = ChainedCi("http://{}.url".format(url), 'https://pages.io',
                           1234, '', '')
    token = "my_private_token"
    if use_bad_token:
        token = "bad_one"
    client = gitlab_client.GitlabClient(chained_ci, {
        "{}.url".format(url): token,
        "gitlab.com": "gitlab_token"
    }, None)
    generate_requests_mocks()
    payload = {"payload": {"value": 12}}
    run = ChainedCiRun(client, chained_ci, 12, payload, topic)
    JOB.run = run
    JOB.inner_jobs = []
    JOB.inner_pipe = None
    JOB._status = None
    JOB.output = ""
    JOB.summary = None
    JOB.get_summary = mock.Mock()
    JOB.get_summary.side_effect = mocked_get_summary
    JOB.get_inner_pipe_information = mock.Mock()
    JOB.get_inner_pipe_information.side_effect = mocked_inner_info
    return JOB


@responses.activate
def test_parse_errored_job():
    job = init_objects("failed-output")
    job.get_information()
    inner_pipe = "https://gitlab.com/Orange-OpenSource/lfn/onap/xtesting-onap/pipelines/213503660"
    assert job.status == "failed"
    assert job.inner_pipe == inner_pipe
    job.get_inner_pipe_information.assert_called_once()
    assert job.output == EXPECTED_FAILED_OUTPUT
    assert job.summary == "nice_summary"


@responses.activate
def test_parse_success_job():
    job = init_objects("success-output")
    job.get_information()
    inner_pipe = "https://gitlab.com/Orange-OpenSource/lfn/onap/xtesting-onap/pipelines/213503661"
    assert job.status == "success"
    assert job.inner_pipe == inner_pipe
    job.get_inner_pipe_information.assert_called_once()
    assert job.output == ''
    assert job.summary == "nice_summary"


@responses.activate
def test_parse_chained_ci_error():
    job = init_objects("chained-ci-error-output")
    job.get_information()
    assert job.status == "failed"
    assert job.inner_pipe is None
    job.get_inner_pipe_information.assert_not_called()
    assert job.output == EXPECTED_CHAINED_CI_FAILURE_OUTPUT
    assert job.summary is None


@responses.activate
def test_parse_request_error():
    job = init_objects("chained-ci-error-output", use_bad_token=True)
    job.get_information()
    assert job.status == "failed"
    assert job.inner_pipe is None
    job.get_inner_pipe_information.assert_not_called()
    assert job.output == EXPECTED_CHAINED_CI_ACCESS_FAILURE_OUTPUT
    assert job.summary is None


@responses.activate
def test_parse_request_connect_error():
    job = init_objects("chained-ci-error-connect")
    Http.default_max_retries = 2
    Http.default_backoff = 0
    job.get_information()
    assert job.status == "failed"
    assert job.inner_pipe is None
    job.get_inner_pipe_information.assert_not_called()
    assert job.output == EXPECTED_CHAINED_CI_CONNECT_FAILURE_OUTPUT
    assert job.summary is None
