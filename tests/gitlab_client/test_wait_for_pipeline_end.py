#!/usr/bin/env python3
# -*- coding: utf-8 -*-
# SPDX-License-Identifier: Apache-2.0
import mock
from mock import MagicMock, PropertyMock
from chained_ci_mqtt_trigger import chained_ci

from chained_ci_mqtt_trigger.gitlab_client import GitlabClient
from chained_ci_mqtt_trigger.notification_client import NotificationClient
from chained_ci_mqtt_trigger.chained_ci_run import ChainedCiRun


@mock.patch('chained_ci_mqtt_trigger.notification_client.NotificationClient')
@mock.patch('chained_ci_mqtt_trigger.chained_ci_run.ChainedCiRun')
def test_wait_pipeline_end_OK(chained_ci_run_mock_class,
                              notification_mock_class):
    notification_client = notification_mock_class.return_value
    chained_ci_run = chained_ci_run_mock_class.return_value
    finished_mock = PropertyMock(return_value=True)
    overall_message_mock = PropertyMock(return_value="overall")
    summary_message_mock = PropertyMock(return_value="summary")
    score_mock = PropertyMock(return_value=12)
    payload_mock = PropertyMock(return_value="mypayload")
    type(chained_ci_run).finished = finished_mock
    type(chained_ci_run).overall_message = overall_message_mock
    type(chained_ci_run).summary_message = summary_message_mock
    type(chained_ci_run).score = score_mock
    type(chained_ci_run).payload = payload_mock
    client = GitlabClient(None, {}, notification_client)
    client.wait_for_pipeline_end(chained_ci_run)
    finished_mock.assert_called_once()
    assert payload_mock.call_count == 2
    overall_message_mock.assert_called_once()
    summary_message_mock.assert_called_once()
    score_mock.assert_called_once()
    expected_message = "Chained-CI build too long. Aborting."
    expected_calls = [
        mock.call('overall', '"mypayload"'),
        mock.call('summary', '"mypayload"', score=12)
    ]
    assert notification_client.notify.mock_calls == expected_calls


@mock.patch('chained_ci_mqtt_trigger.notification_client.NotificationClient')
@mock.patch('chained_ci_mqtt_trigger.chained_ci_run.ChainedCiRun')
def test_wait_pipeline_end_NOK(chained_ci_run_mock_class,
                               notification_mock_class):
    notification_client = notification_mock_class.return_value
    chained_ci_run = chained_ci_run_mock_class.return_value
    finished_mock = PropertyMock(return_value=False)
    overall_message_mock = PropertyMock()
    summary_message_mock = PropertyMock()
    score_mock = PropertyMock()
    payload_mock = PropertyMock(return_value="mypayload")
    type(chained_ci_run).finished = finished_mock
    type(chained_ci_run).overall_message = overall_message_mock
    type(chained_ci_run).summary_message = summary_message_mock
    type(chained_ci_run).score = score_mock
    type(chained_ci_run).payload = payload_mock
    client = GitlabClient(None, {}, notification_client)
    client.boundaries['wait_time'] = 0
    client.boundaries['max_retries'] = 1
    client.wait_for_pipeline_end(chained_ci_run)
    finished_mock.assert_called_once()
    payload_mock.assert_called_once()
    overall_message_mock.assert_not_called()
    summary_message_mock.assert_not_called()
    score_mock.assert_not_called()
    expected_message = "Chained-CI build too long. Aborting."
    notification_client.notify.assert_called_once_with(expected_message,
                                                       '"mypayload"')


@mock.patch('chained_ci_mqtt_trigger.notification_client.NotificationClient')
@mock.patch('chained_ci_mqtt_trigger.chained_ci_run.ChainedCiRun')
def test_wait_pipeline_end_NOK_longer(chained_ci_run_mock_class,
                                      notification_mock_class):
    notification_client = notification_mock_class.return_value
    chained_ci_run = chained_ci_run_mock_class.return_value
    finished_mock = PropertyMock(return_value=False)
    overall_message_mock = PropertyMock()
    summary_message_mock = PropertyMock()
    score_mock = PropertyMock()
    payload_mock = PropertyMock(return_value="mypayload")
    type(chained_ci_run).finished = finished_mock
    type(chained_ci_run).overall_message = overall_message_mock
    type(chained_ci_run).summary_message = summary_message_mock
    type(chained_ci_run).score = score_mock
    type(chained_ci_run).payload = payload_mock

    client = GitlabClient(None, {}, notification_client)
    client.boundaries['wait_time'] = 0
    client.boundaries['max_retries'] = 3
    client.wait_for_pipeline_end(chained_ci_run)
    assert finished_mock.call_count == 3
    payload_mock.assert_called_once()
    overall_message_mock.assert_not_called()
    summary_message_mock.assert_not_called()
    score_mock.assert_not_called()
    expected_message = "Chained-CI build too long. Aborting."
    notification_client.notify.assert_called_once_with(expected_message,
                                                       '"mypayload"')
