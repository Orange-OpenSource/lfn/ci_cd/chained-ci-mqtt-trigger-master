#!/usr/bin/env python3
# -*- coding: utf-8 -*-
# SPDX-License-Identifier: Apache-2.0
import mock
import pytest

import requests
import json

from chained_ci_mqtt_trigger import gitlab_client
from chained_ci_mqtt_trigger.chained_ci import ChainedCi
from chained_ci_mqtt_trigger.chained_ci_run import ChainedCiRun
from chained_ci_mqtt_trigger.topic import Topic


def mocked_requests_post(*args, **kwargs):
    class MockResponse:
        def __init__(self, json_data, status_code):
            self.text = json_data
            self.status_code = status_code

        def json(self):
            if self.text == "no_json":
                raise (ValueError)
            return self.text

        def raise_for_status(self):
            if self.status_code > 299:
                raise (requests.RequestException)

    if args[0] == 'http://success.url/api/v4/projects/1234/trigger/pipeline':
        print("args: {}".format(args))
        print("kwargs: {}".format(kwargs))
        if kwargs['data'] == {
                'token': 'abc-def',
                'ref': 'master',
                'variables[MY_VAR]': '1664',
                'variables[GERRIT_REVIEW]': 22,
                'variables[GERRIT_PATCHSET]': 2
        }:
            return MockResponse({"id": 64}, 200)
        if kwargs['data'] == {
                'token': 'abc-def',
                'ref': 'master',
                'variables[MY_VAR]': '1664',
                'variables[GERRIT_REVIEW]': 22,
                'variables[GERRIT_PATCHSET]': None
        }:
            return MockResponse({"id": 33}, 200)
        else:
            return MockResponse("error", 500)
    elif args[0] == 'http://failure.url/api/v4/projects/1234/trigger/pipeline':
        return MockResponse({"status": "failed"}, 500)
    elif args[
            0] == 'http://bad_json.url/api/v4/projects/1234/trigger/pipeline':
        return MockResponse("no_json", 200)
    elif args[
            0] == 'http://exception.url/api/v4/projects/1234/trigger/pipeline':
        raise requests.ReadTimeout
    return MockResponse(None, 404)


def init_objects(url, mocker, use_bad_token=False):
    Topic.topics = []
    Topic.default_strict_mode = False
    notification_mock_class = mocker.patch(
        'chained_ci_mqtt_trigger.notification_client.NotificationClient')
    notification_client = notification_mock_class.return_value
    chained_ci = ChainedCi("http://{}.url".format(url), 'https://pages.io',
                           1234, 'abc-def', 'master')
    chained_ci.additional_variables = {'MY_VAR': "1664"}
    chained_ci.transformations = {
        'GERRIT_REVIEW': 'change.number',
        'GERRIT_PATCHSET': 'patchSet.number'
    }
    Topic('topic')
    client = gitlab_client.GitlabClient(chained_ci, {}, notification_client)
    client.wait_for_pipeline_end = mock.Mock()
    client.parse_request_error = mock.Mock()
    return (notification_client, client)


@mock.patch('requests.post', side_effect=mocked_requests_post)
def test_launch_success(mock_request, mocker):
    notification_client, client = init_objects("success", mocker)
    dict_payload = {'change': {'number': 22}, 'patchSet': {'number': 2}}
    payload = json.dumps(dict_payload)
    notification_client, client = init_objects("success", mocker)
    client.launch(payload, 'topic')
    calls = [
        mock.call(
            "Chained-CI build started (pipeline id: 64), max time: 24400 seconds",
            payload,
            score=0)
    ]
    assert notification_client.notify.call_count == 1
    notification_client.notify.assert_has_calls(calls)
    client.parse_request_error.assert_not_called()
    client.wait_for_pipeline_end.assert_called_once()
    assert isinstance(client.wait_for_pipeline_end.mock_calls[0][1][0],
                      ChainedCiRun)
    assert client.wait_for_pipeline_end.mock_calls[0][1][0].topic == Topic.get(
        'topic',
    )
    assert client.wait_for_pipeline_end.mock_calls[0][1][0].payload == dict_payload
    assert client.wait_for_pipeline_end.mock_calls[0][1][
        0].chained_ci == client._chained_ci
    assert client.wait_for_pipeline_end.mock_calls[0][1][
        0].gitlab_client == client
    assert client.available


@mock.patch('requests.post', side_effect=mocked_requests_post)
def test_launch_success_strict_mode(mock_request, mocker):
    notification_client, client = init_objects("success", mocker)
    dict_payload = {'change': {'number': 22}, 'patchSet': {'number': 2}}
    payload = json.dumps(dict_payload)
    notification_client, client = init_objects("success", mocker)
    Topic.get('topic').is_strict_mode = True
    client.launch(payload, 'topic')
    calls = [
        mock.call(
            "Chained-CI build started (pipeline id: 64), max time: 24400 seconds\n Putting a `-1` while waiting for the results.",
            payload,
            score=-1)
    ]
    assert notification_client.notify.call_count == 1
    notification_client.notify.assert_has_calls(calls)
    client.parse_request_error.assert_not_called()
    client.wait_for_pipeline_end.assert_called_once()
    assert isinstance(client.wait_for_pipeline_end.mock_calls[0][1][0],
                      ChainedCiRun)
    assert client.wait_for_pipeline_end.mock_calls[0][1][0].topic == Topic.get(
        'topic',
    )
    assert client.wait_for_pipeline_end.mock_calls[0][1][0].payload == dict_payload
    assert client.wait_for_pipeline_end.mock_calls[0][1][
        0].chained_ci == client._chained_ci
    assert client.wait_for_pipeline_end.mock_calls[0][1][
        0].gitlab_client == client
    assert client.available


@mock.patch('requests.post', side_effect=mocked_requests_post)
def test_launch_transformation_issue(mock_request, mocker):
    notification_client, client = init_objects("success", mocker)
    dict_payload = {'change': {'number': 22}, 'patchSet': {'other': 2}}
    payload = json.dumps(dict_payload)
    notification_client, client = init_objects("success", mocker)
    client.launch(payload, 'topic')
    calls = [
        mock.call(
            "Chained-CI build started (pipeline id: 33), max time: 24400 seconds",
            payload,
            score=0)
    ]
    assert notification_client.notify.call_count == 1
    notification_client.notify.assert_has_calls(calls)
    client.parse_request_error.assert_not_called()
    client.wait_for_pipeline_end.assert_called_once()
    assert isinstance(client.wait_for_pipeline_end.mock_calls[0][1][0],
                      ChainedCiRun)
    assert client.wait_for_pipeline_end.mock_calls[0][1][0].topic == Topic.get(
        'topic',
    )
    assert client.wait_for_pipeline_end.mock_calls[0][1][0].payload == dict_payload
    assert client.wait_for_pipeline_end.mock_calls[0][1][
        0].chained_ci == client._chained_ci
    assert client.wait_for_pipeline_end.mock_calls[0][1][
        0].gitlab_client == client
    assert client.available


@mock.patch('requests.post', side_effect=mocked_requests_post)
def test_launch_payload_not_json(mock_request, mocker):
    notification_client, client = init_objects("success", mocker)
    client.launch("payload", 'topic')
    expected_message = "Chained-CI failure, payload is not JSON"
    calls = [mock.call(expected_message, "payload")]
    assert notification_client.notify.call_count == 1
    notification_client.notify.assert_has_calls(calls)
    client.parse_request_error.assert_not_called()
    client.wait_for_pipeline_end.assert_not_called()
    assert client.available


@mock.patch('requests.post', side_effect=mocked_requests_post)
def test_launch_bad_request(mock_request, mocker):
    notification_client, client = init_objects("failure", mocker)
    payload = json.dumps({'change': {'number': 22}, 'patchSet': {'number': 2}})
    client.launch(payload, 'topic')
    notification_client.notify.assert_not_called()
    client.parse_request_error.assert_called_once_with(
        mock.ANY, payload, "Chained-CI build starting error.")
    client.wait_for_pipeline_end.assert_not_called()
    assert client.available


@mock.patch('requests.post', side_effect=mocked_requests_post)
def test_launch_bad_json(mock_request, mocker):
    notification_client, client = init_objects("bad_json", mocker)
    payload = json.dumps({'change': {'number': 22}, 'patchSet': {'number': 2}})
    client.launch(payload, 'topic')
    notification_client.notify.assert_not_called()
    client.parse_request_error.assert_called_once_with(
        mock.ANY, payload, "Chained-CI build starting error.")
    client.wait_for_pipeline_end.assert_not_called()
    assert client.available


@mock.patch('requests.post', side_effect=mocked_requests_post)
def test_launch_bad_connect_error(mock_request, mocker):
    notification_client, client = init_objects("exception", mocker)
    payload = json.dumps({'change': {'number': 22}, 'patchSet': {'number': 2}})
    client.launch(payload, 'topic')
    notification_client.notify.assert_not_called()
    client.parse_request_error.assert_called_once_with(
        mock.ANY, payload, "Chained-CI build starting error.")
    client.wait_for_pipeline_end.assert_not_called()
    assert client.available
