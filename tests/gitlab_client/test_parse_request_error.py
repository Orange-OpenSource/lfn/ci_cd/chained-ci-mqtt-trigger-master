#!/usr/bin/env python3
# -*- coding: utf-8 -*-
# SPDX-License-Identifier: Apache-2.0
import mock
import pytest

from chained_ci_mqtt_trigger import gitlab_client


class MockRequest:
    def __init__(self, json, status_code):
        self.text = json
        self.status_code = status_code

    def json(self):
        if self.text == "error":
            raise ValueError
        return self.text


def init_objects(mocker):
    notification_mock_class = mocker.patch(
        'chained_ci_mqtt_trigger.notification_client.NotificationClient')
    notification_client = notification_mock_class.return_value
    client = gitlab_client.GitlabClient(None, {}, notification_client)
    return notification_client, client


def test_parse_parse_request_error_404(mocker):
    notification_client, client = init_objects(mocker)
    request = MockRequest({"error": "the error"}, 404)
    client.parse_request_error(request, 'payload', "the message")
    expected_message = "the message Status code: 404, failure message: the error"
    notification_client.notify.assert_called_once_with(expected_message,
                                                       "payload")


def test_parse_parse_request_error_400(mocker):
    notification_client, client = init_objects(mocker)
    request = MockRequest({"message": {"base": "the error"}}, 400)
    client.parse_request_error(request, 'payload', "the message")
    expected_message = "the message Status code: 400, failure message: the error"
    notification_client.notify.assert_called_once_with(expected_message,
                                                       "payload")


def test_parse_parse_request_error_other(mocker):
    notification_client, client = init_objects(mocker)
    request = MockRequest({"message": "the error"}, 500)
    client.parse_request_error(request, 'payload', "the message")
    expected_message = "the message Status code: 500, failure message: the error"
    notification_client.notify.assert_called_once_with(expected_message,
                                                       "payload")


def test_parse_parse_request_error_bad_json(mocker):
    notification_client, client = init_objects(mocker)
    request = MockRequest("error", 504)
    client.parse_request_error(request, 'payload', "the message")
    expected_message = "the message Status code: 504, failure message: error"
    notification_client.notify.assert_called_once_with(expected_message,
                                                       "payload")
