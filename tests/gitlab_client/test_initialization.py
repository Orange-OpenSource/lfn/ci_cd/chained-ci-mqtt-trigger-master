#!/usr/bin/env python3
# -*- coding: utf-8 -*-
# SPDX-License-Identifier: Apache-2.0
import pytest

from chained_ci_mqtt_trigger import gitlab_client


def test_test_init_no_xtesting_client():
    client = gitlab_client.GitlabClient(None, {}, None)
    #assert client.xtesting_client is None


# def test_test_init_xtesting_client():
#     client = gitlab_client.GitlabClient(None, {}, None, xtesting_db="url")
#     assert client.xtesting_client.url == "url"