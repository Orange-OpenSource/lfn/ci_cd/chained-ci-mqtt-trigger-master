# -*- coding: utf-8 -*-
# SPDX-License-Identifier: Apache-2.0
import json
import os
import urllib.parse
from pathlib import Path

import mock
import responses
from loguru import logger

from chained_ci_mqtt_trigger.chained_ci import ChainedCi
from chained_ci_mqtt_trigger.gitlab_client import GitlabClient
from chained_ci_mqtt_trigger.http_adapter import Http
from chained_ci_mqtt_trigger.topic import Topic
from chained_ci_mqtt_trigger.xtesting import XtestingClient

dir_path = os.path.dirname(os.path.realpath(__file__))

PIPELINE_OK = json.loads(Path(
    '{0}/launch_pipeline_ok.json'.format(dir_path),
).read_text().replace('\n', ''))
CHECK_PIPELINE = json.loads(Path(
    '{0}/check_pipeline_early_failed.json'.format(dir_path),
).read_text().replace('\n', ''))
CHECK_JOBS = json.loads(Path(
    '{0}/check_pipeline_early_failed_jobs.json'.format(dir_path),
).read_text().replace('\n', ''))
TRACE_JOB_FAILED = Path(
    '{0}/trace_early_failed_job.txt'.format(dir_path),
).read_text()
INNER_PIPELINE = json.loads(Path(
    '{0}/inner_early_failed_pipeline.json'.format(dir_path),
).read_text().replace('\n', ''))
TRACE_INNER_JOB_FAILED = Path(
    '{0}/trace_early_inner_failed_job.txt'.format(dir_path),
).read_text()
FAILURE_MESSAGE = Path(
    '{0}/early_failure_message.txt'.format(dir_path),
).read_text()[0:-1]
# can't figure out why I've got one character more...
FAILURE_SUMMARY = Path(
    '{0}/early_failure_summary.txt'.format(dir_path),
).read_text()[0:-1]
TESTRESULTS = json.loads(Path(
    '{0}/testresults_early.json'.format(dir_path),
).read_text().replace('\n', ''))
BUILD_SO = Path(
    '{0}/build-so.html'.format(dir_path),
).read_text()


@responses.activate
def test_pipeline_failed_cases_NOK(mocker):

    def request_callback_post(request):
        payload_split = request.body.split('&')
        payload = {}
        for kv in payload_split:
            k = urllib.parse.unquote(kv.split('=')[0])
            v = urllib.parse.unquote(kv.split('=')[1])
            payload[k] = v
        expected_payload = {
            'token': 'abc-def',
            'ref': 'master',
            'variables[MY_VAR]': '1664',
            'variables[GERRIT_REVIEW]': '22',
            'variables[GERRIT_PATCHSET]': '2',
        }
        if payload == expected_payload:
            resp_body = PIPELINE_OK
            headers = {}
            return (200, headers, json.dumps(resp_body))
        return(404, {}, json.dumps({'not': 'found'}))

    notification_mock_class = mocker.patch(
        'chained_ci_mqtt_trigger.notification_client.NotificationClient',
    )
    notification_client = notification_mock_class.return_value
    chained_ci = ChainedCi(
        'http://failed.url',
        'https://pages.io',
        1234,
        'abc-def',
        'master',
    )
    chained_ci.additional_variables = {'MY_VAR': '1664'}
    chained_ci.transformations = {
        'GERRIT_REVIEW': 'change.number',
        'GERRIT_PATCHSET': 'patchSet.number',
    }
    chained_ci.result_page_template = '{{ change.number}}-{{ patchSet.number }}/'
    private_tokens = {'failed.url': 'private_token'}
    Http.default_max_retries = 1
    Http.default_backoff = 0
    XtestingClient.url = 'http://testresults.opnfv.org/onap'
    XtestingClient.build_tag_tpl = '{{ change.number}}-{{ patchSet.number }}'
    Topic.topics = []
    Topic(
        'onap/so',
        summary_console_jobs={
            'build_so:onap_oom_gating_azure_2': 'build',
            'infra_deploy:onap_oom_gating_azure_1': 'build',
            'onap_deploy:onap_oom_gating_azure_2': 'prepare',
            'onap_test:onap_oom_gating_azure_2': 'pages',
        },
        case_names={
            'onap-k8s': 'full',
            'core': 'Basic SO Health Check',
            'basic_vm': 'full',
        },
    )
    logger.debug(Topic.get('onap/so').case_names)
    client = GitlabClient(chained_ci, private_tokens, notification_client)
    client.boundaries = {
        'wait_time': 1,
        'max_retries': 2,
        'max_timeout_retries': 2,
    }
    responses.add_callback(
        responses.POST,
        'http://failed.url/api/v4/projects/1234/trigger/pipeline',
        callback=request_callback_post,
        content_type='application/json',
    )
    responses.add(
        responses.GET,
        'http://failed.url/api/v4/projects/1234/pipelines/215461543',
        json=CHECK_PIPELINE,
        status=200,
    )
    responses.add(
        responses.GET,
        'http://failed.url/api/v4/projects/1234/pipelines/215461543/jobs',
        json=CHECK_JOBS,
        status=200,
    )
    responses.add(
        responses.GET,
        'http://failed.url/api/v4/projects/1234/jobs/863259574/trace',
        body=TRACE_JOB_FAILED,
        status=200,
    )
    responses.add(
        responses.GET,
        'https://gitlab.com/Orange-OpenSource/lfn/onap/build-so/pipelines/219108874',
        json=INNER_PIPELINE,
        status=200,
    )
    responses.add(
        responses.GET,
        'https://gitlab.com/api/v4/projects/14144730/jobs/863263887/trace',
        body=TRACE_INNER_JOB_FAILED,
        status=200,
    )
    responses.add(
        responses.GET,
        'http://testresults.opnfv.org/onap/api/v1/results?build_tag=22-2&page=1',
        json=TESTRESULTS,
        status=200,
    )
    responses.add(
        responses.GET,
        'https://gitlab.com/Orange-OpenSource/lfn/onap/build-so',
        body=BUILD_SO,
        status=200,
    )
    payload = json.dumps({'change': {'number': 22}, 'patchSet': {'number': 2}})
    client.launch(payload, 'onap/so')
    first_call_msg_header = 'Chained-CI build started (pipeline id: 215461543)'
    first_call_msg = '{0}, max time: 2 seconds'.format(first_call_msg_header)
    call_payload = '{"change": {"number": 22}, "patchSet": {"number": 2}}'
    assert notification_client.notify.call_count == 3
    assert notification_client.notify.mock_calls[0][1][0] == first_call_msg
    assert notification_client.notify.mock_calls[0][1][1] == call_payload
    assert notification_client.notify.mock_calls[1][1][0] == FAILURE_MESSAGE
    assert notification_client.notify.mock_calls[1][1][1] == call_payload
    assert notification_client.notify.mock_calls[2][1][0] == FAILURE_SUMMARY
    assert notification_client.notify.mock_calls[2][1][1] == call_payload
    assert notification_client.notify.mock_calls[2][2] == {'score': -1}
