#!/usr/bin/env python3
# -*- coding: utf-8 -*-
# SPDX-License-Identifier: Apache-2.0
import mock

from threading import Event
import paho.mqtt.client as client

from chained_ci_mqtt_trigger.runner import Runner

@mock.patch.object(Event, 'wait')
def test_loop_forever_no_loop(mock_method):
    mqtt_client =  client.Client()
    notification_topics = "notification"
    nickname = "yolo"
    notification_tranformations = []
    trigger_config = {'magic_word': 'magic', 'on_comment': 'trigger'}
    runner = Runner(trigger_config, mqtt_client, notification_topics, nickname,
                    notification_tranformations)
    runner.loop = False
    runner.loop_forever()
    mock_method.assert_not_called()

@mock.patch.object(Event, 'wait')
@mock.patch('chained_ci_mqtt_trigger.runner.Runner.loop',
            new_callable=mock.PropertyMock)
def test_loop_forever(mock_loop, mock_wait):
    mqtt_client =  client.Client()
    notification_topics = "notification"
    nickname = "yolo"
    notification_tranformations = []
    trigger_config = {'magic_word': 'magic', 'on_comment': 'trigger'}
    runner = Runner(trigger_config, mqtt_client, notification_topics, nickname,
                    notification_tranformations)
    print("mock_loop: {}".format(mock_loop))
    print("mock_wait: {}".format(mock_wait))
    mock_loop.side_effect = [True, False]
    runner.loop_forever()
    mock_wait.assert_called_once_with(timeout=10)

def test_load_jobs_from_file():
    mqtt_client =  client.Client()
    notification_topics = "notification"
    nickname = "yolo"
    notification_tranformations = []
    trigger_config = {'magic_word': 'magic', 'on_comment': 'trigger'}
    runner = Runner(trigger_config, mqtt_client, notification_topics, nickname,
                    notification_tranformations)
    runner.load_jobs_from_file()
    assert True == True

def test_worker_message():
    mqtt_client =  client.Client()
    notification_topics = "notification"
    nickname = "yolo"
    notification_tranformations = []
    trigger_config = {'magic_word': 'magic', 'on_comment': 'trigger'}
    runner = Runner(trigger_config, mqtt_client, notification_topics, nickname,
                    notification_tranformations)
    runner.handle_worker_message('')
    assert True == True

def test_master_message():
    mqtt_client =  client.Client()
    notification_topics = "notification"
    nickname = "yolo"
    notification_tranformations = []
    trigger_config = {'magic_word': 'magic', 'on_comment': 'trigger'}
    runner = Runner(trigger_config, mqtt_client, notification_topics, nickname,
                    notification_tranformations)
    runner.handle_master_message('')
    assert True == True
