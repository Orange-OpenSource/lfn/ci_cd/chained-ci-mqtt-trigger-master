#!/usr/bin/env python3
# -*- coding: utf-8 -*-
# SPDX-License-Identifier: Apache-2.0
import json

import mock
import pytest
import responses

from chained_ci_mqtt_trigger.chained_ci import ChainedCi
from chained_ci_mqtt_trigger.chained_ci_run import ChainedCiRun
from chained_ci_mqtt_trigger.gitlab_client import GitlabClient
from chained_ci_mqtt_trigger.http_adapter import Http


def request_callback(request):
    if request.headers['PRIVATE-TOKEN'] != 'my_private_token':
        return (404, {}, None)
    if request.url == 'http://success.url/api/v4/projects/1234/pipelines/12':
        return (200, {}, json.dumps({"status": "success"}))
    if request.url == 'http://failed.url/api/v4/projects/1234/pipelines/12':
        return (200, {}, json.dumps({"status": "failed"}))
    if request.url == 'http://running.url/api/v4/projects/1234/pipelines/12':
        return (200, {}, json.dumps({"status": "running"}))
    if request.url == 'http://bad_json.url/api/v4/projects/1234/pipelines/12':
        return (200, {}, 'no_json')


def generate_requests_mocks():
    responses.add_callback(
        responses.GET,
        'http://success.url/api/v4/projects/1234/pipelines/12',
        callback=request_callback,
        content_type='application/json',
    )
    responses.add_callback(
        responses.GET,
        'http://failed.url/api/v4/projects/1234/pipelines/12',
        callback=request_callback,
        content_type='application/json',
    )
    responses.add_callback(
        responses.GET,
        'http://running.url/api/v4/projects/1234/pipelines/12',
        callback=request_callback,
        content_type='application/json',
    )
    responses.add_callback(
        responses.GET,
        'http://bad_json.url/api/v4/projects/1234/pipelines/12',
        callback=request_callback,
        content_type='application/json',
    )


def init_objects(url, mocker, use_bad_token=False):
    notification_mock_class = mocker.patch(
        'chained_ci_mqtt_trigger.notification_client.NotificationClient')
    chained_ci = ChainedCi("http://{}.url".format(url), 'https://pages.io',
                           1234, '', '')
    token = "my_private_token"
    if use_bad_token:
        token = "bad_one"
    generate_requests_mocks()
    client = GitlabClient(chained_ci, {"{}.url".format(url): token}, None)
    run = ChainedCiRun(client, chained_ci, 12, {}, 'topic')
    run.parse_pipeline = mock.Mock()
    run.parse_request_error = mock.Mock()
    return run


@responses.activate
def test_check_pipeline_status_success(mocker):
    run = init_objects("success", mocker)
    expected_message = "Chained-CI build finished with status OK"
    assert run.check_pipeline_status() == True
    run.parse_pipeline.assert_called_once_with(error_only=False)
    run.parse_request_error.assert_not_called()


@responses.activate
def test_check_pipeline_status_success_bad_tokens(mocker):
    run = init_objects("success", mocker, use_bad_token=True)
    expected_message = "Chained-CI status retrieving error."
    run.gitlab_client.boundaries['wait_time'] = 0
    run.gitlab_client.boundaries['max_timeout_retries'] = 0
    assert run.check_pipeline_status() == True
    run.parse_pipeline.assert_not_called()
    run.parse_request_error.assert_called_once_with(mock.ANY, expected_message)


@responses.activate
def test_check_pipeline_status_failed(mocker):
    run = init_objects("failed", mocker)
    assert run.check_pipeline_status() == True
    run.parse_pipeline.assert_called_once_with()
    run.parse_request_error.assert_not_called()


@responses.activate
def test_check_pipeline_status_bad_json(mocker):
    run = init_objects("bad_json", mocker)
    expected_message = "Chained-CI status retrieving error."
    Http.default_max_retries = 2
    Http.default_backoff = 0
    assert run.check_pipeline_status() == True
    run.parse_pipeline.assert_not_called()
    run.parse_request_error.assert_called_once_with(mock.ANY, expected_message)


@responses.activate
def test_check_pipeline_status_connect_error(mocker):
    run = init_objects("exception", mocker)
    expected_message = "Chained-CI status retrieving error."
    Http.default_max_retries = 2
    Http.default_backoff = 0
    assert run.check_pipeline_status() == True
    run.parse_pipeline.assert_not_called()
    run.parse_request_error.assert_called_once_with(mock.ANY, expected_message)


@responses.activate
def test_check_pipeline_status_running(mocker):
    run = init_objects("running", mocker)
    assert run.check_pipeline_status() == False
    run.parse_pipeline.assert_not_called()
    run.parse_request_error.assert_not_called()
