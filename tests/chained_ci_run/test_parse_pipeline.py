# -*- coding: utf-8 -*-
# SPDX-License-Identifier: Apache-2.0
import json

import mock
import responses

from chained_ci_mqtt_trigger import gitlab_client
from chained_ci_mqtt_trigger.chained_ci import ChainedCi
from chained_ci_mqtt_trigger.chained_ci_job import ChainedCiJob
from chained_ci_mqtt_trigger.chained_ci_run import ChainedCiRun
from chained_ci_mqtt_trigger.http_adapter import Http
from chained_ci_mqtt_trigger.topic import Topic

RUN = None

ONE_FAILED = [{
    'id': 1,
    'name': 'first',
    'status': 'success'
}, {
    'id': 2,
    'name': 'second',
    'status': 'success'
}, {
    'id': 3,
    'name': 'third',
    'status': 'failed'
}, {
    'id': 4,
    'name': 'fourth',
    'status': 'cancelled'
}]

SEVERAL_FAILED = [{
    'id': 1,
    'name': 'first',
    'status': 'success'
}, {
    'id': 2,
    'name': 'second',
    'status': 'success'
}, {
    'id': 3,
    'name': 'third',
    'status': 'failed'
}, {
    'id': 4,
    'name': 'fourth',
    'status': 'cancelled'
}, {
    'id': 5,
    'name': 'fifth',
    'status': 'failed'
}, {
    'id': 6,
    'name': 'sixth',
    'status': 'failed'
}, {
    'id': 7,
    'name': 'seventh',
    'status': 'failed'
}]

SUCCESS = [{
    'id': 1,
    'name': 'first',
    'status': 'success'
}, {
    'id': 2,
    'name': 'second',
    'status': 'success'
}, {
    'id': 3,
    'name': 'third',
    'status': 'success'
}, {
    'id': 4,
    'name': 'fourth',
    'status': 'success'
}, {
    'id': 5,
    'name': 'fifth',
    'status': 'success'
}, {
    'id': 6,
    'name': 'sixth',
    'status': 'success'
}, {
    'id': 7,
    'name': 'seventh',
    'status': 'success'
}]


def generate_job(run, name, id, status, output, summary):
    job = ChainedCiJob(run, id, name)
    job.status = status
    if output:
        job.output = output
    if summary:
        job.summary = summary
    job.get_information = mock.Mock()
    return job


def generate_jobs(run, success=False, several_errors=True):
    if success:
        run.jobs.append(generate_job(run, 'first', 1, "success", "", ""))
        run.jobs.append(
            generate_job(run, 'second', 2, "success", "", "success_summary"))
        run.jobs.append(generate_job(run, 'third', 3, "success", "", ""))
        run.jobs.append(generate_job(run, 'fourth', 4, "success", "", ""))
        run.jobs.append(
            generate_job(run, 'fifth', 5, "success", "", "nice_summary"))
        run.jobs.append(
            generate_job(run, 'sixth', 6, "success", "", "other_summary"))
        run.jobs.append(generate_job(run, 'seventh', 7, "success", "", ""))
    else:
        run.jobs.append(generate_job(run, 'third', 3, "failed", "yolo", ""))
        if several_errors:
            run.jobs.append(
                generate_job(run, 'fifth', 5, "failed", "yala",
                             "nice_summary"))
            run.jobs.append(
                generate_job(run, 'sixth', 6, "failed", "yili",
                             "other_summary"))
            run.jobs.append(generate_job(run, 'seventh', 7, "failed", "", ""))


def request_callback(request):
    if request.headers['PRIVATE-TOKEN'] != 'my_private_token':
        return (404, {}, None)
    if request.url == 'http://one-failed.url/api/v4/projects/1234/pipelines/12/jobs':
        return (200, {}, json.dumps(ONE_FAILED))
    if request.url == 'http://several-failed.url/api/v4/projects/1234/pipelines/12/jobs':
        return (200, {}, json.dumps(SEVERAL_FAILED))
    if request.url == 'http://success.url/api/v4/projects/1234/pipelines/12/jobs':
        return (200, {}, json.dumps(SUCCESS))
    return (404, {}, None)


def generate_requests_mocks():
    responses.add_callback(
        responses.GET,
        'http://one-failed.url/api/v4/projects/1234/pipelines/12/jobs',
        callback=request_callback,
        content_type='application/json',
    )
    responses.add_callback(
        responses.GET,
        'http://several-failed.url/api/v4/projects/1234/pipelines/12/jobs',
        callback=request_callback,
        content_type='application/json',
    )
    responses.add_callback(
        responses.GET,
        'http://success.url/api/v4/projects/1234/pipelines/12/jobs',
        callback=request_callback,
        content_type='application/json',
    )
    responses.add(
        method='GET',
        url='http://bad_json.url/api/v4/projects/1234/pipelines/12/jobs',
        body='no-json',
    )


def init_objects(url, mocker, use_bad_token=False):
    notification_mock_class = mocker.patch(
        'chained_ci_mqtt_trigger.notification_client.NotificationClient')
    notification_client = notification_mock_class.return_value
    chained_ci = ChainedCi("http://{}.url".format(url), 'https://pages.io',
                           1234, '', '')
    token = "my_private_token"
    if use_bad_token:
        token = "bad_one"
    topic = Topic('topic')
    client = gitlab_client.GitlabClient(chained_ci,
                                        {"{}.url".format(url): token},
                                        notification_client)

    payload = {"payload": {"value": 12}}
    generate_requests_mocks()
    run = ChainedCiRun(client, chained_ci, 12, payload, topic)
    run.parse_request_error = mock.Mock()
    run.find_jobs = mock.Mock()
    return run


@responses.activate
def test_parse_pipeline_OK_no_summary(mocker):
    run = init_objects("one-failed", mocker)
    run.chained_ci.result_page_template = "{{ payload.value }}"
    generate_jobs(run, several_errors=False)
    run.parse_pipeline()
    assert run.overall_message == "Chained CI build failure on some jobs.\nJobs with issue:\nyolo"
    assert run.summary_message == "\nFull results can be seen here: https://pages.io/12index.html"
    run.parse_request_error.assert_not_called()
    run.find_jobs.assert_called()


@responses.activate
def test_parse_pipeline_OK_no_summary_topics(mocker):
    run = init_objects("one-failed", mocker)
    run.chained_ci.result_page_template = "{{ payload.value }}"
    run.topic.case_names = {'test': 'test'}
    run.retrieve_status = mock.Mock()
    generate_jobs(run, several_errors=False)
    run.parse_pipeline()
    assert run.overall_message == "Chained CI build failure on some jobs.\nJobs with issue:\nyolo"
    assert run.summary_message == "\nFull results can be seen here: https://pages.io/12index.html"
    run.parse_request_error.assert_not_called()
    run.find_jobs.assert_called()
    run.retrieve_status.assert_called()


@responses.activate
def test_parse_pipeline_OK_success_no_summary(mocker):
    run = init_objects("success", mocker)
    run.chained_ci.result_page_template = "{{ payload.value }}"
    generate_jobs(run, success=True)
    run.parse_pipeline(error_only=False)
    assert run.overall_message == "Chained CI build failure on some jobs.\nJobs with issue:"
    assert run.summary_message == "\nFull results can be seen here: https://pages.io/12index.html\nsuccess_summary\nnice_summary\nother_summary"
    run.find_jobs.assert_called()
    run.parse_request_error.assert_not_called()


@responses.activate
def test_parse_pipeline_OK_summary(mocker):
    run = init_objects("several-failed", mocker)
    generate_jobs(run)
    run.parse_pipeline()
    assert run.overall_message == "Chained CI build failure on some jobs.\nJobs with issue:\nyolo\nyala\nyili\n  * name: seventh, no url"
    assert run.summary_message == "\nnice_summary\nother_summary"
    run.parse_request_error.assert_not_called()
    run.find_jobs.assert_called()


@responses.activate
def test_parse_pipeline_bad_json(mocker):
    run = init_objects("bad_json", mocker)
    expected_message = "Chained-CI status retrieving error."
    Http.default_max_retries = 2
    Http.default_backoff = 0
    run.payload = {"payload": {}}
    run.parse_pipeline()
    run.find_jobs.assert_not_called()
    run.parse_request_error.assert_called_once_with(mock.ANY, expected_message)


@responses.activate
def test_parse_pipeline_connect_error(mocker):
    run = init_objects("exception", mocker)
    expected_message = "Chained-CI status retrieving error."
    Http.default_max_retries = 2
    Http.default_backoff = 0
    run.payload = {"payload": {}}
    run.parse_pipeline()
    run.find_jobs.assert_not_called()
    run.parse_request_error.assert_called_once_with(mock.ANY, expected_message)


@responses.activate
def test_parse_pipeline_bad_request(mocker):
    run = init_objects("success", mocker, use_bad_token=True)
    expected_message = "Chained-CI status retrieving error."
    Http.default_max_retries = 2
    Http.default_backoff = 0
    run.payload = {"payload": {}}
    run.parse_pipeline()
    run.find_jobs.assert_not_called()
    run.parse_request_error.assert_called_once_with(mock.ANY, expected_message)
