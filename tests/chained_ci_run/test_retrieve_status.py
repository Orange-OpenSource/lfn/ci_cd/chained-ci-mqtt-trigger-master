# -*- coding: utf-8 -*-
# SPDX-License-Identifier: Apache-2.0

from mock import patch

from loguru import logger

from chained_ci_mqtt_trigger.chained_ci_run import ChainedCiRun
from chained_ci_mqtt_trigger.topic import Topic

from chained_ci_mqtt_trigger.xtesting import XtestingCase, XtestingClient


def mocked_xtesting_client(*args, **kwargs):
    logger.debug('args to use: {}', args)

    class MockClient:
        def __init__(self, payload):
            logger.debug('payload to use: {}', payload)
            if payload['test'] == 'criteria_failed':
                case = XtestingCase('case', 'failed', [])
                self.cases = [case]
            if payload['test'] == 'criteria_success':
                case = XtestingCase('case', 'success', [])
                self.cases = [case]

    return MockClient(args, *kwargs)


def mocked_post_init(*args, **kwargs):
    logger.debug('args to use: {}', args)


@patch('chained_ci_mqtt_trigger.chained_ci_run.XtestingClient')
def test_criteria_failed(mock_client_class):
    instance = mock_client_class.return_value
    case = XtestingCase('case', 'failed', [])
    instance.cases = [case]
    topic = Topic('topic', case_names={'case': 'full'})
    run = ChainedCiRun(None, None, 12, {'test': 'criteria_failed'}, topic)
    assert run.retrieve_status() == False
    assert run.score == -1


@patch('chained_ci_mqtt_trigger.chained_ci_run.XtestingClient')
def test_not_enough_tests_found(mock_client_class):
    instance = mock_client_class.return_value
    case = XtestingCase('case', 'success', [])
    instance.cases = [case]
    topic = Topic('topic', case_names={'other_case': 'full'})
    run = ChainedCiRun(None, None, 12, {'test': 'criteria_success'}, topic)
    assert run.retrieve_status() == False
    assert run.score == -1


@patch('chained_ci_mqtt_trigger.chained_ci_run.XtestingClient')
def test_all_criterias_success(mock_client_class):
    instance = mock_client_class.return_value
    case = XtestingCase('case', 'success', [])
    instance.cases = [case]
    topic = Topic('topic', case_names={'case': 'full'})
    run = ChainedCiRun(None, None, 12, {'test': 'criteria_success'}, topic)
    assert run.retrieve_status() == True
    assert run.score == 1
