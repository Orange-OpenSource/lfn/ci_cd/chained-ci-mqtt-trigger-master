#!/usr/bin/env python3
# -*- coding: utf-8 -*-
# SPDX-License-Identifier: Apache-2.0
import mock
import pytest

from chained_ci_mqtt_trigger.chained_ci_run import ChainedCiRun

RUN = ChainedCiRun(None, None, 12, {}, 'topic')


def pass_to_finish():
    RUN.finished = True


@mock.patch.object(ChainedCiRun, 'check_pipeline_status')
def test_not_finished(check_status):
    RUN.finished = False
    assert RUN.finished == False
    check_status.assert_called_once()


@mock.patch.object(ChainedCiRun, 'check_pipeline_status')
def test_already_finished(check_status):
    RUN.finished = True
    assert RUN.finished == True
    check_status.assert_not_called()


@mock.patch(
    'chained_ci_mqtt_trigger.chained_ci_run.ChainedCiRun.check_pipeline_status'
)
def test_finished(check_status):
    RUN.finished = False
    check_status.side_effect = pass_to_finish
    assert RUN.finished == True
    check_status.assert_called_once()