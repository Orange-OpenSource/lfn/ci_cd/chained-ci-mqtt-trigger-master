#!/usr/bin/env python3
# -*- coding: utf-8 -*-
# SPDX-License-Identifier: Apache-2.0
import pytest


@pytest.fixture()
def valid_gitlab_config():
    return {
        'hostname': 'https://gitlab.com',
        'result_url': 'https://pages.gitlab.io',
        'result_template': '',
        'nickname': 'Gitlab Public',
        'chained_ci_project_id': '1234',
        'project_token': '_trigger_token_',
        'private_tokens': {
            'gitlab.forge.orange-labs.fr': '_private_user_token_',
            'gitlab.com': '_private_user_token_gitlab_com_'
        },
        'git_reference': 'master',
        'additional_variables': {
            'POD': 'onap_oom_gating_pod4',
            'WORKAROUND': 'False'
        },
        'transformations': {
            'GERRIT_REVIEW': 'change.number',
            'GERRIT_PATCHSET': 'patchSet.number'
        },
        'notification_tranformations': {
            'gerrit_review': 'change.number',
            'gerrit_patchset': 'patchSet.number'
        },
        'summary_console_jobs': {
            'apps_test:onap_oom_gating_pod4': 'pages',
            'apps_deploy:onap_oom_gating_pod4': 'postconfigure'
        }
    }


@pytest.fixture()
def valid_trigger_config():
    return {'magic_word': 'magic', 'on_comment': 'second_one'}


@pytest.fixture()
def valid_topics_config():
    return {
        'main': 'topic/myproject',
        'master': 'topic/master',
        'worker': 'topic/worker',
        'notification': 'topic/review'
    }
