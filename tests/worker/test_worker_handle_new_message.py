#!/usr/bin/env python3
# -*- coding: utf-8 -*-
# SPDX-License-Identifier: Apache-2.0
import mock
import json

import paho.mqtt.client as mqtt

from chained_ci_mqtt_trigger.worker import Worker


def test_handle_new_message_no_magic_bad_json(valid_gitlab_config,
                                              valid_trigger_config,
                                              valid_topics_config, mocker):
    worker = Worker(valid_trigger_config, mqtt.Client(), valid_topics_config,
                    valid_gitlab_config)
    mocker.patch.object(mqtt.Client, 'publish')
    topic = "topic/myproject/second_one".encode('utf-8')
    msg = mqtt.MQTTMessage(topic=topic)
    msg.payload = "payload".encode('utf-8')
    worker.handle_new_message(msg)
    mqtt.Client.publish.assert_not_called()


def test_handle_new_message_no_magic(valid_gitlab_config, valid_trigger_config,
                                     valid_topics_config, mocker):
    mocker.patch.object(mqtt.Client, 'publish')
    worker = Worker(valid_trigger_config, mqtt.Client(), valid_topics_config,
                    valid_gitlab_config)
    topic = "topic/myproject/second_one".encode('utf-8')
    msg = mqtt.MQTTMessage(topic=topic)
    msg.payload = json.dumps({
        "comment": "Ce n'est pas magique"
    }).encode('utf-8')
    worker.handle_new_message(msg)
    mqtt.Client.publish.assert_not_called()


def test_handle_new_message_magic_gitlab_available(valid_gitlab_config,
                                                   valid_trigger_config,
                                                   valid_topics_config,
                                                   mocker):
    mocker.patch.object(mqtt.Client, 'publish')
    worker = Worker(valid_trigger_config, mqtt.Client(), valid_topics_config,
                    valid_gitlab_config)
    topic = "topic/myproject/second_one".encode('utf-8')
    msg = mqtt.MQTTMessage(topic=topic)
    payload = json.dumps({"comment": "it's a kind of magic"}).encode('utf-8')
    msg.payload = payload
    worker.handle_new_message(msg)
    mqtt.Client.publish.assert_called_once_with('topic/master',
                                                json.dumps(
                                                    {"worker": worker.name}),
                                                qos=0)


def test_handle_new_message_magic_gitlab_not_available(valid_gitlab_config,
                                                       valid_trigger_config,
                                                       valid_topics_config,
                                                       mocker):
    mocker.patch.object(mqtt.Client, 'publish')
    worker = Worker(valid_trigger_config, mqtt.Client(), valid_topics_config,
                    valid_gitlab_config)
    worker._gitlab_client.available = False
    topic = "topic/myproject/second_one".encode('utf-8')
    msg = mqtt.MQTTMessage(topic=topic)
    payload = json.dumps({"comment": "it's a kind of magic"}).encode('utf-8')
    msg.payload = payload
    worker.handle_new_message(msg)
    mqtt.Client.publish.assert_not_called()


def test_handle_new_message_not_trigger_topic_gitlab_available(
        valid_gitlab_config, valid_trigger_config, valid_topics_config,
        mocker):
    mocker.patch.object(mqtt.Client, 'publish')
    worker = Worker(valid_trigger_config, mqtt.Client(), valid_topics_config,
                    valid_gitlab_config)
    topic = "topic/myproject/third_one".encode('utf-8')
    msg = mqtt.MQTTMessage(topic=topic)
    payload = json.dumps({"we": "shouldn't care"}).encode('utf-8')
    msg.payload = payload
    worker.handle_new_message(msg)
    mqtt.Client.publish.assert_called_once_with('topic/master',
                                                json.dumps(
                                                    {"worker": worker.name}),
                                                qos=0)


def test_handle_new_message_not_trigger_topic_gitlab_not_available(
        valid_gitlab_config, valid_trigger_config, valid_topics_config,
        mocker):
    mocker.patch.object(mqtt.Client, 'publish')
    worker = Worker(valid_trigger_config, mqtt.Client(), valid_topics_config,
                    valid_gitlab_config)
    worker._gitlab_client.available = False
    topic = "topic/myproject/third_one".encode('utf-8')
    msg = mqtt.MQTTMessage(topic=topic)
    payload = json.dumps({"we": "shouldn't care"}).encode('utf-8')
    msg.payload = payload
    worker.handle_new_message(msg)
    mqtt.Client.publish.assert_not_called()
