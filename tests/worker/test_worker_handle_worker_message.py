#!/usr/bin/env python3
# -*- coding: utf-8 -*-
# SPDX-License-Identifier: Apache-2.0
import mock
import json

import paho.mqtt.client as mqtt

from chained_ci_mqtt_trigger.worker import Worker


def test_handle_worker_message_bad_json(valid_gitlab_config,
                                        valid_trigger_config,
                                        valid_topics_config, mocker):
    worker = Worker(valid_trigger_config, mqtt.Client(), valid_topics_config,
                    valid_gitlab_config)
    mocker.patch.object(mqtt.Client, 'publish')
    topic = "topic/myproject/second_one".encode('utf-8')
    msg = mqtt.MQTTMessage(topic=topic)
    msg.payload = "payload".encode('utf-8')
    worker.handle_worker_message(msg)
    mqtt.Client.publish.assert_not_called()
    assert worker.events.empty()


def test_handle_worker_message_bad_payload_no_worker(valid_gitlab_config,
                                                     valid_trigger_config,
                                                     valid_topics_config,
                                                     mocker):
    worker = Worker(valid_trigger_config, mqtt.Client(), valid_topics_config,
                    valid_gitlab_config)
    mocker.patch.object(mqtt.Client, 'publish')
    topic = "topic/myproject/second_one".encode('utf-8')
    payload = {'job': {'payload': 'yolo', 'topic': 'yala'}}
    msg = mqtt.MQTTMessage(topic=topic)
    msg.payload = json.dumps(payload).encode('utf-8')
    worker.handle_worker_message(msg)
    mqtt.Client.publish.assert_not_called()
    assert worker.events.empty()


def test_handle_worker_message_bad_payload_no_job(valid_gitlab_config,
                                                  valid_trigger_config,
                                                  valid_topics_config, mocker):
    worker = Worker(valid_trigger_config, mqtt.Client(), valid_topics_config,
                    valid_gitlab_config)
    mocker.patch.object(mqtt.Client, 'publish')
    topic = "topic/myproject/second_one".encode('utf-8')
    payload = {'worker': worker.name}
    msg = mqtt.MQTTMessage(topic=topic)
    msg.payload = json.dumps(payload).encode('utf-8')
    worker.handle_worker_message(msg)
    mqtt.Client.publish.assert_not_called()
    assert worker.events.empty()


def test_handle_worker_message_bad_payload_no_job_message(
        valid_gitlab_config, valid_trigger_config, valid_topics_config,
        mocker):
    worker = Worker(valid_trigger_config, mqtt.Client(), valid_topics_config,
                    valid_gitlab_config)
    mocker.patch.object(mqtt.Client, 'publish')
    topic = "topic/myproject/second_one".encode('utf-8')
    payload = {'worker': worker.name, 'job': {'topic': 'yala'}}
    msg = mqtt.MQTTMessage(topic=topic)
    msg.payload = json.dumps(payload).encode('utf-8')
    worker.handle_worker_message(msg)
    mqtt.Client.publish.assert_not_called()
    assert worker.events.empty()


def test_handle_worker_message_bad_payload_no_job_topic(
        valid_gitlab_config, valid_trigger_config, valid_topics_config,
        mocker):
    worker = Worker(valid_trigger_config, mqtt.Client(), valid_topics_config,
                    valid_gitlab_config)
    mocker.patch.object(mqtt.Client, 'publish')
    worker._gitlab_client.available = False
    topic = "topic/myproject/second_one".encode('utf-8')
    payload = {'worker': worker.name, 'job': {'payload': 'yolo'}}
    msg = mqtt.MQTTMessage(topic=topic)
    msg.payload = json.dumps(payload).encode('utf-8')
    worker.handle_worker_message(msg)
    mqtt.Client.publish.assert_not_called()
    assert worker.events.empty()


def test_handle_worker_message_gitlab_not_available(valid_gitlab_config,
                                                    valid_trigger_config,
                                                    valid_topics_config,
                                                    mocker):
    worker = Worker(valid_trigger_config, mqtt.Client(), valid_topics_config,
                    valid_gitlab_config)
    mocker.patch.object(mqtt.Client, 'publish')
    worker._gitlab_client.available = False
    topic = "topic/myproject/second_one".encode('utf-8')
    msg = mqtt.MQTTMessage(topic=topic)
    payload = {
        'worker': worker.name,
        'job': {
            'payload': 'yolo',
            'topic': 'yala'
        }
    }
    msg.payload = json.dumps(payload).encode('utf-8')
    worker.handle_worker_message(msg)
    mqtt.Client.publish.assert_called_once_with('yala', 'yolo', qos=0)
    assert worker.events.empty()


def test_handle_worker_message_not_right_worker(valid_gitlab_config,
                                                valid_trigger_config,
                                                valid_topics_config, mocker):
    worker = Worker(valid_trigger_config, mqtt.Client(), valid_topics_config,
                    valid_gitlab_config)
    mocker.patch.object(mqtt.Client, 'publish')
    topic = "topic/myproject/second_one".encode('utf-8')
    payload = {'worker': 'not_me'}
    msg = mqtt.MQTTMessage(topic=topic)
    msg.payload = json.dumps(payload).encode('utf-8')
    worker.handle_worker_message(msg)
    mqtt.Client.publish.assert_not_called()
    assert worker.events.empty()


def test_handle_worker_message_OK(valid_gitlab_config, valid_trigger_config,
                                  valid_topics_config, mocker):
    worker = Worker(valid_trigger_config, mqtt.Client(), valid_topics_config,
                    valid_gitlab_config)
    mocker.patch.object(mqtt.Client, 'publish')
    topic = "topic/myproject/second_one"
    encoded_topic = "topic/myproject/second_one".encode('utf-8')
    payload = {
        'worker': worker.name,
        'job': {
            'payload': 'yolo',
            'topic': topic,
        },
    }
    msg = mqtt.MQTTMessage(topic=encoded_topic)
    msg.payload = json.dumps(payload).encode('utf-8')
    worker.handle_worker_message(msg)
    mqtt.Client.publish.assert_not_called()
    assert worker.events.get_nowait() == {'payload': 'yolo', 'topic': topic}


def test_handle_worker_message_shutdown(valid_gitlab_config,
                                        valid_trigger_config,
                                        valid_topics_config, mocker):
    worker = Worker(valid_trigger_config, mqtt.Client(), valid_topics_config,
                    valid_gitlab_config)
    mocker.patch.object(mqtt.Client, 'publish')
    topic = "topic/myproject/second_one".encode('utf-8')
    payload = {'worker': worker.name, 'shutdown': 'please'}
    msg = mqtt.MQTTMessage(topic=topic)
    msg.payload = json.dumps(payload).encode('utf-8')
    worker.handle_worker_message(msg)
    mqtt.Client.publish.assert_not_called()
    assert worker.events.empty()
    assert worker.loop == False
