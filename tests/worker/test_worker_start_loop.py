#!/usr/bin/env python3
# -*- coding: utf-8 -*-
# SPDX-License-Identifier: Apache-2.0
import mock
import json

import paho.mqtt.client as mqtt

from chained_ci_mqtt_trigger.worker import Worker
from chained_ci_mqtt_trigger import gitlab_client


def test_handle_new_message_no_magic_bad_json(valid_gitlab_config,
                                              valid_trigger_config,
                                              valid_topics_config, mocker):
    mocker.patch.object(mqtt.Client, 'publish')
    worker = Worker(valid_trigger_config, mqtt.Client(), valid_topics_config,
                    valid_gitlab_config)
    worker.events.put({'payload': 'yolo', 'topic': 'topic/comment-added'})
    patched_launch = mocker.patch.object(gitlab_client.GitlabClient, 'launch')
    patched_launch.side_effect = KeyboardInterrupt()
    worker.loop_forever()
    mqtt.Client.publish.assert_called_once_with('topic/master',
                                                json.dumps(
                                                    {"worker": worker.name}),
                                                qos=0)
    gitlab_client.GitlabClient.launch.assert_called_once_with('yolo', 'topic')
