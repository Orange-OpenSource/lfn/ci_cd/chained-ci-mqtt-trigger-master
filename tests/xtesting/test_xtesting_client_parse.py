# -*- coding: utf-8 -*-
# SPDX-License-Identifier: Apache-2.0
import json
import os
from pathlib import Path

from mock import patch

from chained_ci_mqtt_trigger.xtesting import XtestingClient
from chained_ci_mqtt_trigger.xtesting import XtestingCase


dir_path = os.path.dirname(os.path.realpath(__file__))
RESULT = json.loads(Path(
    '{0}/result.json'.format(dir_path),
).read_text().replace('\n', ''))


@patch.object(XtestingClient, '__post_init__')
def test_parse_response_OK(mock_post_init):
    client = XtestingClient({})
    client.cases = []
    client.parse_xtesting_response(RESULT)
    assert len(client.cases) == 20
    assert client.cases[12].name == 'ves-collector'
    assert len(client.cases[12].tests) == 5
    assert client.cases[12].tests[2].name == 'Send 3GPP Heartbeat event to VES and check if is routed to proper topic'
    assert client.cases[12].tests[2].status == 'success'
    assert client.cases[2].name == 'nonssl_endpoints'
    assert client.cases[2].status == 'failed'


@patch.object(XtestingClient, '__post_init__')
def test_parse_response_case_present(mock_post_init):
    client = XtestingClient({})
    case = XtestingCase('jdpw_ports', 'failed', [])
    client.cases = [case]
    client.parse_xtesting_response(RESULT)
    assert len(client.cases) == 1
