# -*- coding: utf-8 -*-
# SPDX-License-Identifier: Apache-2.0

from chained_ci_mqtt_trigger import xtesting


def test_parse_tests():
    raw_tests = [
        {'name': 'test1', 'status': 'PASS'},
        {'name': 'test2'},
        {'status': 'PASS'},
        {'aname': 'test1', 'status': 'PASS'},
        {'name': 'test3', 'status': 'FAIL'},
    ]
    results = xtesting.parse_tests(raw_tests)
    assert len(results) == 2
    assert type(results[0]) == xtesting.XtestingTest
    assert results[0].name == "test1"
    assert results[0].status == "success"
    assert type(results[1]) == xtesting.XtestingTest
    assert results[1].name == "test3"
    assert results[1].status == "failed"
