# -*- coding: utf-8 -*-
# SPDX-License-Identifier: Apache-2.0

from mock import patch

from chained_ci_mqtt_trigger.xtesting import XtestingClient


@patch.object(XtestingClient, 'get_tests')
def test_init_OK(mock_get):
    XtestingClient.url = "http://get.url"
    XtestingClient.build_tag_tpl = "{{ some.place }}"
    client = XtestingClient({'some': {'place': 'here'}, 'other': 'there'})
    assert client.build_tag == 'here'
    assert len(client.cases) == 0
    mock_get.assert_called_once()


@patch.object(XtestingClient, 'get_tests')
def test_init_NOK(mock_get):
    XtestingClient.url = "http://get.url"
    XtestingClient.build_tag_tpl = "{{ some.place }}"
    client = XtestingClient({'no': {'place': 'here'}, 'other': 'there'})
    assert client.build_tag == ''
    assert len(client.cases) == 0
    mock_get.assert_called_once()
