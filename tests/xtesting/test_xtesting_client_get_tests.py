# -*- coding: utf-8 -*-
# SPDX-License-Identifier: Apache-2.0

import responses
from mock import patch

from chained_ci_mqtt_trigger.xtesting import XtestingClient


@responses.activate
@patch.object(XtestingClient, 'parse_xtesting_response')
@patch.object(XtestingClient, 'handle_pagination')
def test_get_tests_connect_error(mock_parse, mock_handle):
    responses.add(
        method='GET',
        url='http://other.url/api/v1/results?build_tag=here&page=1',
        json={'a': 'response'},
    )
    XtestingClient.url = "http://get.url"
    XtestingClient.build_tag_tpl = "{{ some.place }}"
    client = XtestingClient({'some': {'place': 'here'}, 'other': 'there'})
    assert len(client.cases) == 0
    mock_parse.assert_not_called()
    mock_handle.assert_not_called()


@responses.activate
@patch.object(XtestingClient, 'parse_xtesting_response')
@patch.object(XtestingClient, 'handle_pagination')
def test_get_tests_json_error(mock_parse, mock_handle):
    responses.add(
        method='GET',
        url='http://get.url/api/v1/results?build_tag=here&page=1',
        body="no_json",
    )
    XtestingClient.url = "http://get.url"
    XtestingClient.build_tag_tpl = "{{ some.place }}"
    client = XtestingClient({'some': {'place': 'here'}, 'other': 'there'})
    assert len(client.cases) == 0
    mock_parse.assert_not_called()
    mock_handle.assert_not_called()


@responses.activate
@patch.object(XtestingClient, 'parse_xtesting_response')
@patch.object(XtestingClient, 'handle_pagination')
def test_get_tests_OK(mock_parse, mock_handle):
    message = {'some': 'message'}
    responses.add(
        method='GET',
        url='http://get.url/api/v1/results?build_tag=here&page=1',
        json=message,
    )
    XtestingClient.url = "http://get.url"
    XtestingClient.build_tag_tpl = "{{ some.place }}"

    client = XtestingClient({'some': {'place': 'here'}, 'other': 'there'})
    assert len(client.cases) == 0
    mock_parse.assert_called_once_with(message)
    mock_handle.assert_called_once_with(message)
