# -*- coding: utf-8 -*-
# SPDX-License-Identifier: Apache-2.0

from mock import patch

from chained_ci_mqtt_trigger.xtesting import XtestingClient


@patch.object(XtestingClient, '__post_init__')
@patch.object(XtestingClient, 'get_tests')
def test_handle_pagination_not_last_page(mock_get, mock_post_init):
    response = {'pagination': {'current_page': 1, 'total_pages': 2}}
    client = XtestingClient({})
    client.handle_pagination(response)
    client.get_tests.assert_called_once_with(page=2)


@patch.object(XtestingClient, '__post_init__')
@patch.object(XtestingClient, 'get_tests')
def test_handle_pagination_last_page(mock_get, mock_post_init):
    response = {'pagination': {'current_page': 2, 'total_pages': 2}}
    client = XtestingClient({})
    client.handle_pagination(response)
    client.get_tests.assert_not_called()
